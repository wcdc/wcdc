<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/16/16
 * Time: 11:05 AM
 */
session_start();
if (!isset($_GET['id']) || $_GET['id'] == "")
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}
?>
<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $article->get_title(); ?></title>
        <link rel="stylesheet" href="../../css/app.css">
        <link rel="stylesheet" href="../../css/article.css">
        <link rel="stylesheet" href="../../css/login.css"/>
        <link rel="shortcut icon" href="../../img/Working_Logo.png">

        <script type="text/javascript">
            $(document).ready(function () {
                $('.likes').on('click', function (e) {
    
                    e.preventDefault();
                    var a = $(this);
                    var url = a.attr('href');
                    var tmp = url.substring(43);
                    var id = parseInt(tmp);
    
                    $.ajax(url, {
                        type : 'POST',
                        data: {likes:1, dislikes:0, id:id},
                        success: function (data) {
                            $('#likes').html(data);
    
                            $.ajax(url, {
                                type: 'POST',
                                data: {likes: 0, dislikes: -1, id: id},
                                success: function (data) {
                                    $('#dislikes').html(data);
                                }
                            });
                        }, error: function () {
                            $('html,body').animate({scrollTop: 0}, 'slow');
                            $('.err').text("You don't have permissions to like this article, please signin");
                            $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                            $('.err').show().slideDown(1000);
                            $('.err').find('.notif-btn').on('click', function () {
                                $('.err').hide().slideUp(1000);
                            });
                            setTimeout(function () {
                                $('.err').hide().slideUp();
                            }, 60000);
                        }
                    });
                });
    
                $('.dislikes').on('click', function (e) {
    
                    e.preventDefault();
                    var a = $(this);
                    var url = a.attr('href');
                    var tmp = url.substring(43);
                    var id = parseInt(tmp);
    
                    $.ajax(url, {
                        type : 'POST',
                        data: {likes:0, dislikes:1, id:id},
                        success: function (data) {
                            $('#dislikes').html(data);
    
                            $.ajax(url, {
                                type : 'POST',
                                data: {likes: -1, dislikes: 0, id:id},
                                success: function (data) {
                                    $('#likes').html(data);
                                }
                            });
                        }, error: function () {
                            $('html,body').animate({scrollTop: 0}, 'slow');
                            $('.err').text("You don't have permissions to dislike this article, please signin");
                            $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                            $('.err').show().slideDown(1000);
                            $('.err').find('.notif-btn').on('click', function () {
                                $('.err').hide().slideUp(1000);
                            });
                            setTimeout(function () {
                                $('.err').hide().slideUp();
                            }, 60000);
                        }
                    });
                });
            });
        </script>

    </head>
    <body>
    <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '520044271513632',
                    xfbml      : true,
                    version    : 'v2.6'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        
        <div class="corpus_article">
            <?php
                include_once ("../../Vue/notifications/notif.php");
                $article->article();
                $article_pseudo_author = $article->get_pseudo_author();
            ?>

            <hr/>
            <div id="comm" class="large-10 medium-10 small-10">
                
            <?php

                if (!$_GET['v'] && !$_GET['i']) {
                    $commentaires = $article->get_commentaires();

                    if (count($commentaires) > 0) {
                        foreach ($commentaires as $commentaire) {
                            $commentaire->commentaire();
                            
                            ?>
                            <script type="text/javascript">
                                <?php
                                    $user = new User($_SESSION['logged_on_user']);
                                if($user->get_pseudo() == $commentaire->get_pseudo_author())
                                { ?>
                                    var id = <?php echo $commentaire->get_id(); ?>;
                                    $('#hover-link-delete-' + id).show();
                                    $('#hover-link-edit-' + id).show();
                                <?php }
                                else if ($user->get_right_access() >= 3)
                                { ?>
                                    var id = <?php echo $commentaire->get_id(); ?>;
                                    $('#hover-link-delete-' + id).show();
                            <?php } ?>
                            </script>
                        <?php }
                    }
                }
            ?>


            </div>
            <div class="foot-bar" style="background-color: darkgray; height: 20px; position: fixed; bottom: 0px; right: 0px; left: 0px;"></div>
        </div>
    <br/>
    <br/>
    <br/>
    <br/>


    <?php include_once ("../../Vue/notifications/js.php"); ?>

        <script type="text/javascript">

            $('#add-comm').on('submit', function (e) {

                e.preventDefault();

                var $form = $('#add-comm');
                var url = '../../Modele/User_interface/add_comm.php';
                var formdata = (window.FormData) ? new FormData($form[0]) : null;
                var data = (formdata !== null) ? formdata : $form.serialize();

                $.ajax(url, {
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {
                        $('#input-add-comm').val('');
                        $('#comm').prepend(data);
                        var nb_comms = $('#nb_comms').text();
                        nb_comms = parseInt(nb_comms);
                        nb_comms += 1;
                        $('#nb_comms').text('');
                        $('#nb_comms').text(nb_comms);
                    },
                    error: function () {
                        $('html,body').animate({scrollTop: 0}, 'slow');
                        $('.err').text("You don't have permissions to edit this comment");
                        $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $('.err').show().slideDown(1000);
                        $('.err').find('.notif-btn').on('click', function () {
                            $('.err').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $('.err').hide().slideUp();
                        }, 60000);
                    }
                });
            });

        </script>


        <script type="text/javascript">

            $('.hover-link-edit').on('click', function (e) {

                e.preventDefault();
                var a = $(this);
                var url = a.attr('href');
                var tmp = url.substring(45);
                var id = parseInt(tmp);

                $.ajax(url, {
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        var id2 = "edit-" + id;
                        $('#' + id2).replaceWith(data);
                    },
                    error: function () {
                        $('.err').text("You don't have permissions to edit this comment");
                        $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $('.err').show().slideDown(1000);
                        $('.err').find('.notif-btn').on('click', function () {
                            $('.err').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $('.err').hide().slideUp();
                        }, 60000);
                    }
                });
            });


            $('.hover-link-delete').on('click', function (e) {

                e.preventDefault();
                var a = $(this);
                var url = a.attr('href');
                var tmp = url.substring(47);
                var id = parseInt(tmp);

                $.ajax(url, {
                    type: 'POST',
                    data: {id: id},
                    success: function () {
                        var id2 = "delete-" + id;
                        //alert(id2);
                        $('#' + id2).remove();
                        var nb_comms = $('#nb_comms').text();
                        nb_comms = parseInt(nb_comms);
                        nb_comms -= 1;
                        $('#nb_comms').text('');
                        $('#nb_comms').text(nb_comms);
                    },
                    error: function () {
                        $('.err').text("You don't have permissions to delete this comment");
                        $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $('.err').show().slideDown(1000);

                        $('.err').find('.notif-btn').on('click', function () {
                            $('.err').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $('.err').hide().slideUp();
                        }, 60000);
                    }
                });
            });

        </script>

    </body>
</html>
