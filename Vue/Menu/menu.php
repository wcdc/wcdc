<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/15/16
 * Time: 4:12 PM
 */
?>
<head>
    <?php
    if ($_SERVER['PHP_SELF'] == "/Controleur/Article/article.php")
    {
        $up_dir = "../../image_articles/tmp";
        if (!file_exists("../../image_articles"))
            mkdir("../../image_articles");
        if (!file_exists("../../image_articles/tmp"))
            mkdir($up_dir);
        $img1 = imagecreatefromstring(base64_decode($article->get_path_wc_img()));
        imagepng($img1, "$up_dir/img_tmp1.png");

        $img2 = imagecreatefromstring(base64_decode($article->get_path_dc_img()));
        imagepng($img2, "$up_dir/img_tmp2.png");

        $img3 = imagecreatetruecolor(660, 335);
        $img4 = imagecreatetruecolor(330, 335);
        $img5 = imagecreatetruecolor(330, 335);

        $TailleImg1 = getimagesize("$up_dir/img_tmp1.png");
        imagecopyresampled($img4, $img1, 0, 0, 0, 0, 330, 335, $TailleImg1[0], $TailleImg1[1]);

        $TailleImg2 = getimagesize("$up_dir/img_tmp2.png");
        imagecopyresampled($img5, $img2, 0, 0, 0, 0, 330, 335, $TailleImg2[0], $TailleImg2[1]);

        imagecopymerge($img3, $img4, 0, 0, 0, 0, 330, 335, 100);
        imagecopymerge($img3, $img5, 331, 0, 0, 0, 330, 335, 100);

        $img = "img_tmp" . time() . ".png";
        imagepng($img3, "$up_dir/$img");

        imagedestroy($img1);
        imagedestroy($img2);
        imagedestroy($img3);
        imagedestroy($img4);
        imagedestroy($img5);

        ?>
    <meta property="og:url"                content="http://workingconceptdyingconcept.com/Controleur/Article/article.php?id=<?php echo $article->get_id(); ?>" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="<?php echo $article->get_title(); ?>" />
    <meta property="og:description"        content="<?php echo $article->get_wc_city(); ?> - <?php echo $article->get_dc_city(); ?>"/>
    <meta property="og:site_name"          content="WCDC" />
    <meta property="og:image"              content="http://workingconceptdyingconcept.com/image_articles/tmp/<?php echo $img; ?>" />
    <meta property="og:image:width"        content="660">
    <meta property="og:image:height"       content="335">
    <meta property="fb:app_id"             content="520044271513632" />
<?php } ?>
    <link rel="stylesheet" href="../../css/nous.css"/>
    <link rel="stylesheet" href="../../css/app.css"/>
    <link rel="stylesheet" href="../../css/login.css"/>
    <link rel="shortcut icon" href="../../img/Favicon.png">
</head>
    <div id="topmenu" class="fixMenu">

        <?php
        if (isset($_SESSION['logged_on_user']))
        {
            ?>
            <div id="bar">

                <ul id="log" class="dropdown menu fixLog" data-dropdown-menu>
                    <li><a class="logout" style="color: white; padding: inherit; padding-top: 12px; padding-right: 5px;" href="../../Controleur/User_interface/user.php?pseudo=<?php echo $_SESSION['logged_on_user']; ?>"><?php $pseudo = $_SESSION['logged_on_user']; echo "My Account &nbsp;($pseudo)"; ?></a></li>
                    <li><a  class="logout" style="color: white; padding: inherit; padding-top: 12px; padding-right: 5px;" href="../../Modele/User_interface/logout.php">&nbsp;&nbsp; Logout</a></li>
                </ul>
            </div>
            <?php
        }
        else
        {
            ?>
            <div id="bar">
                
                <ul id="log" class="dropdown menu fixLog" data-dropdown-menu style="float: right;">
                    <li><a class="login" style="color: white; padding: inherit; padding-top: 12px; padding-right: 5px;" href="../../Controleur/Login_Register/login.php" style="text-decoration: none; font-size: 16px;">Login &nbsp;&nbsp;&nbsp; Register</a></li>
                </ul>
            </div>
            <?php
        }
        ?>

        <div id="mc" class="menu-centered" >
            <div><a href="../../Controleur/Accueil/accueil.php"><img id="logo" src="../../img/WORKING_DYING.png"/></a></div>
            <ul class="dropdown menu" data-dropdown-menu>
<!--                <li class="item"><a href="../../Controleur/Accueil/accueil.php">Home</a></li>-->
                <li class="item">
                    <a href="#">Category | City</a>
                    <ul id="fp" class="menu">
                        <form method="post" action="../../Controleur/Selection_Articles/select_articles.php">
                            <div id="button">
                                <input id="btn-select" type="submit" value="Submit">
                            </div>
                            <div class="row">
                                <div class="small-6 medium-6 large-6 columns cat" style="padding-left: 5px; padding-top: 5px; padding-right: 0px; margin-right: 0px;">

                                    <?php
                                    while ($cat = $categories->fetch()){
                                        echo '<div class="checkbox1"><input id="'.$cat['name'].'"type="checkbox" name="cat[]" value="'.$cat['name'].'"/><label for="'.$cat['name'].'">'.$cat['name'].'</label></div>';
                                    }
                                    ?>

                                </div>
                                
                                <div class="small-6 medium-6 large-6 columns villes" style="padding-left: 5px; padding-top: 5px;">

                                    <?php
                                    while ($ville = $villes->fetch()){
                                        echo '<div class="checkbox1"><input id="'.$ville['name'].'"type="checkbox" name="villes[]" value="'.$ville['name'].'"/><label for="'.$ville['name'].'">'.$ville['name'].'</label></div>';
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                        </form>
                    </ul>
                </li>
                <li class="item"><a href="../../Controleur/About/about.php">The Concept</a></li>
                <li class="item"><a href="../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php">Submit your Concept</a></li>
                <li class="item"><a href="../../Controleur/Contact/contact.php">Contact</a></li>
            </ul>
        </div>
        <div id="bar">
            <div class="search-bar">
                <form method="post" action="../../Controleur/Search/search.php">
                    <div>
                        <input id="search-bar1" name="search" type="search" placeholder="Search" class="fixSearch">
                        <input id="search-button1" type="submit" value="Search" class="log fixSearch">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="../../js/vendor/jquery.js"></script>
    <script src="../../js/vendor/what-input.js"></script>
    <script src="../../js/vendor/foundation.js"></script>
    <script src="../../js/app.js"></script>
    <script type="text/javascript">

        $('#fp').scroll(function () {
            if ($(this).scrollTop() != 0)
            {
                var top = $(this).scrollTop();
                $('#btn-select').css("position", "relative");
                $('#btn-select').css("top", top);
            }
            else
            {
                $('#btn-select').removeClass("fixButtonSelect");
            }
        });

    </script>
