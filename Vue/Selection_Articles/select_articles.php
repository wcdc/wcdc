<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/21/16
 * Time: 2:36 PM
 */
?>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" href="../../css/nous.css">
        <link rel="stylesheet" href="../../css/app.css">
        <link rel="stylesheet" href="../../css/article.css">
        <link rel="stylesheet" href="../../css/login.css"/>
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
    </head>
    <body>
        <div class="corpus" style="top: 120px;">
            <div class="menu-centered contact-title2" style="padding-top: 20px;">
                <?php
                    if (($nb_res = count($selection)) >= 0)
                    {
                        if ($nb_res > 1)
                            echo "$nb_res Concepts found";
                        else if ($nb_res == 1)
                            echo "$nb_res Concept found";
                        else
                            echo "No result";
                    }
                ?> </div>
            <div class="contact-bar"></div>
            <div class="rond"></div>

            <div class="row" style="margin-top: 20px;">
                <div class="menu-centered">
                    <div class="large-up-3 medium-up-3 small-up-3 container large-centered">
                        <?php
                            foreach ($selection as $art)
                            {
                                if ($art->get_authorized() == 1)
                                    $art->vignette(0);
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

