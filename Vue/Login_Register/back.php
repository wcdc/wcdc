<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/16/16
 * Time: 1:18 PM
 */
$date = getdate();
if ($date['mon'] < 10)
    $date['mon'] = "0" . $date['mon'];
$date_actuelle = $date['year'] . "-" . $date['mon'] . "-" . $date['mday'];
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" href="../../css/app.css">
        <link rel="stylesheet" href="../../css/nous.css">
        <link rel="stylesheet" href="../../css/login.css">
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
        <script type="text/javascript" src="../../js/vendor/jquery.js"></script>
        <script type="text/JavaScript" src="../../js/vendor/form.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body style="background: none;">

        <div id="" class="row corpus" style="position: relative; top: 200px;">

            <?php include_once ("../../Vue/notifications/notif.php"); ?>
            
            <div id="loggin" class="loggin small-6 large-2 columns">
                <div class="login-screen">

                    <div class="app-title">
                        <p>Login</p>
                    </div>

                    <form id="signin" action="../../Modele/Login_Register/back.php" method="POST">
                        <div class="login-form">
                            <div class="control-group">
                                <input type="text" class="" name="pseudo" value="" placeholder="Pseudo" id="login-pseudo" required>
                                <label class="login-field-icon fui-user" for="login-pseudo"></label>
                            </div>

                            <div class="control-group">
                                <input id="passwd" type="password" class="" name="passwd" value="" placeholder="Mot de passe"required>
                                <label class="login-field-icon fui-lock" for="login-pass"></label>
                            </div>

                            <input class="log" type="submit" name="submit" value="Login"/>
                            <a class="login-link" href="#">Lost your password ?</a>
                        </div>
                    </form>

                    <div class="menu-centered"><p>or</p><br/><div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="false"></div></div>

                </div>
            </div>
        </div>
    </body>
</html>