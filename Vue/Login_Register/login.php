<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/16/16
 * Time: 1:18 PM
 */
$date = getdate();
if ($date['mon'] < 10)
    $date['mon'] = "0" . $date['mon'];
$date_act = $date['year'] . "-" . $date['mon'] . "-" . $date['mday'];
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" href="../../css/nous.css">
        <link rel="stylesheet" href="../../css/app.css">
        <link rel="stylesheet" href="../../css/login.css">
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
        <script type="text/javascript" src="../../js/vendor/jquery.js"></script>
        <script type="text/JavaScript" src="../../js/vendor/form.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body style="background: none;">

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.6&appId=247144505668851";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

        <div id="" class="row corpus" style="position: relative; top: 200px;">

            <?php include_once ("../../Vue/notifications/notif.php"); ?>

            <div id="loggin" class="loggin small-6 large-2 columns">

                <div class="login-screen">
                    <div class="menu-centered">
                        <p style="font-size: 20px;;">Let's not compare apples to oranges</p>
                    </div>
                    <br/>
                    <br/>
                    <div class="app-title">
                        <p>Login</p>
                    </div>

                    <form id="signin" action="../../Controleur/User_interface/user.php" method="POST">
                        <div class="login-form">
                            <div class="control-group">
                                <input type="text" class="" name="pseudo" value="" placeholder="Pseudo*" id="login-pseudo" required>
                                <label class="login-field-icon fui-user" for="login-pseudo"></label>
                            </div>

                            <div class="control-group">
                                <input id="passwd" type="password" class="" name="passwd" value="" placeholder="Password*"required>
                                <label class="login-field-icon fui-lock" for="login-pass"></label>
                            </div>
                            <input class="log" type="submit" name="submit" value="Login"/>
                        </div>
<!--                        <div class="menu-centered"><br/><p>or</p><div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="false"></div></div>-->
                    </form>
                </div>
                <a class="login-link menu-centered" href="../../Controleur/User_interface/lost_password.php">Lost your password ?</a>
            </div>

            <div id="log" class="loggin small-6 columns" style="border-left: solid 1px black;">
                
                <div class="login-screen">
                    
                    <div class="app-title">
                        <p>Register</p>
                    </div>
                    
                    <form id="form" name="register" action="../../Modele/Login_Register/create.php" method="POST">
                        <div class="login-form">
                            <div class="row">
                                <div class="control-group small-6 columns">
                                    <input type="text" class="login-field" name="name" value="" placeholder="Name*" id="login-name" minlength="2" maxlength="30" required>
                                    <label class="login-field-icon fui-user" for="login-name"></label>
                                </div>

                                <div class="control-group small-6 columns">
                                    <input type="text" class="login-field" name="firstname" value="" placeholder="First name*" id="login-firstname" minlength="2" maxlength="30" required>
                                    <label class="login-field-icon fui-user" for="login-first_name"></label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="control-group small-6 column">
                                    <input type="date" class="login-field" name="date" value="" placeholder="Born date*" id="login-date" min="1970-01-01" max="<?php echo $date_act; ?>" required>
                                    <label class="login-field-icon fui-user" for="login-date"></label>
                                </div>

                                <div class="control-group small-3 column">
                                    <input type="radio" class="login-field" name="sexe" value="1" id="login-sexe" required>
                                    <label class="login-field-icon fui-user" for="login-sexe">Man</label>
                                </div>

                                <div class="control-group small-3 column">
                                    <input type="radio" class="login-field" name="sexe" value="0" id="login-sexe" required>
                                    <label class="login-field-icon fui-user" for="login-sexe">Woman</label>
                                </div>
                            </div>

                            <div class="control-group">
                                <input type="text" class="login-field" name="ville" value="" placeholder="City*" id="login-town" minlength="2" maxlength="30" required>
                                <label class="login-field-icon fui-user" for="login-town"></label>
                            </div>

                            <div class="control-group">
                                <input type="text" class="login-field" name="pseudo" value="" placeholder="Pseudo*" id="login-pseudo" minlength="2" maxlength="30" required>
                                <label class="login-field-icon fui-user" for="login-pseudo"></label>
                            </div>

                            <div class="control-group">
                                <input type="email" class="login-field" name="mail" value="" placeholder="Mail*" id="Email" required >
                                <label class="login-field-icon fui-user" for="login-mail"></label>
                            </div>

                            <div class="control-group">
                                <input id="passwd2" type="password" class="login-field" name="passwd2" value="" placeholder="Password* (8 characters minimum)" minlength="8" required>
                                <label class="login-field-icon fui-lock" for="login-pass"></label>
                            </div>

                            <div class="control-group">
                                <input id="confirm_passwd" form="Register" type="password" class="login-field" name="confirm_passwd" value="" placeholder="Confirm password*" minlength="8" required>
                                <label class="login-field-icon fui-lock" for="login-pass"></label>
                            </div>

                            <div class="control-group">
                                <input id="term_of_use" type="checkbox" class="login-field" name="term_of_use" value="1" required style="margin-top: 0px; position: relative;top: 0px;left: 0px;">
                                <label class="login-field-icon" for="term_of_use"><p>I have read and accepted the <a class="term" href="../../Controleur/Term_of_use/Term_of_use.php">Terms and Conditions</a> and <a class="term" href="../../Controleur/Term_of_use/Legal_notes.php">Legal notes</a></p></label>
                            </div>
                            <input class="log" type="submit"  name="submit" value="Register"/>


                        </div>
                        <div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="6Lcd5CETAAAAAEUBrKdlgyQyCvfqHk0U3k_xd_zz" style="margin-bottom: 30px;"></div>
                    </form>
                    <p id="champs_requis">* required fields</p>
                </div>
            </div>
        </div>
        
<!--        <script src="../../js/facebook.js"></script>-->
        <script type="text/javascript">
            $('.fb-login-button').on('click', function () {
                FB.login();
            });
        </script>
        <script src="../../js/vendor/sha512.js"></script>
        <script type="text/javascript">
            $('#login-date').on('change', function () {
               $('#login-date').attr("placeholder", "").placeholder();
            });
        </script>
        <?php include_once ("../../Vue/notifications/js.php"); ?>
    </body>
</html>