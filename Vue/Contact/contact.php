<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/16/16
 * Time: 5:06 PM
 */
?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body style="background: none;">
        <div class="corpus">
            <div class="menu-centered contact-title1">Contact</div>
            <hr size="1" align="center" width="80%">
            <div class="menu-centered contact-title2">How to contact us</div>
            <div class="contact-bar"></div>
            <div class="rond"></div>

            <div id="form" class="">

                <form id="my_form" action="../../Modele/Contact/mail.php" method="post">

                    <div class="row float-center">

                        <div class="large-6 columns">
                            <input class="thumbnail" type="text" name="Nom" placeholder="Name*" required>
                        </div>

                        <div class="large-6 columns">
                            <input class="thumbnail" type="text" name="Prenom" placeholder="Firstname *" required>
                        </div>

                    </div>

                    <div class="row float-center">

                        <div class="large-6 columns">
                            <input class="thumbnail" type="email" name="Mail" placeholder="Mail*" required minlength="6">
                        </div>

                        <div class="large-6 columns">
                            <input class="thumbnail" type="tel" name="Telephone" placeholder="Phone number" minlength="10" maxlength="10">
                        </div>

                    </div>

                    <div class="row float-center">

                        <div class="large-12 columns">
                            <textarea class="thumbnail" name="Message" rows="8" placeholder="Send here your request, we will respond as soon as possible *" required></textarea>
                        </div>

                    </div>

                    <div class="row float-left">
                        <div class="g-recaptcha" data-sitekey="6LduXCITAAAAAHa_DWmmSXCrZGbDyLahcgyrE_Kc" style="margin-bottom: 20px; margin-left: 25px;"></div>
                        <br/>
                        <p id="champs_requis">* required fields</p>

                    </div>

                    <div class="row">

                        <div id="div-captcha" class="large-3 large-centered column">
                            <input class="button log" type="submit" name="submit" value="Send" style="margin-bottom: 10px;">
                        </div>

                    </div>

                </form>

            </div>
        </div>

    </body>

</html>