<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/11/16
 * Time: 12:18 PM
 */
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" href="../../css/nous.css">
        <link rel="stylesheet" href="../../css/app.css">
        <link rel="stylesheet" href="../../css/article.css">
        <link rel="stylesheet" href="../../css/login.css"/>
    </head>
    <body>
        <div class="corpus" style="top: 140px;">
            <div class="menu-centered contact-title2" style="padding-top: 20px;"><?php if ($nb_res == 0) echo "No result"; else if ($nb_res == 1) echo "$nb_res Result found"; else echo "$nb_res Results found"?></div>
            <div class="contact-bar"></div>
            <div class="rond"></div>

            <?php
                if ($users)
                {?>

                    <div class="contact-title2 menu-centered">Users</div>
                    <hr/>
                    <div class="row" style="margin-top: 20px;">
                        <div class="menu-centered">
                            <div class="large-up-3 medium-up-3 small-up-3 container large-centered">
                                <?php
                                    foreach ($users as $user)
                                    {
                                        if ($user->get_authorized() == 1)
                                            $user->vignette(0);
                                    }
                                ?>
                            </div>
                        </div>
                    </div>

            <?php }
                if ($articles)
                {?>
                    <div class="contact-title2 menu-centered">Concepts</div>
                    <hr/>
                    <div class="row" style="margin-top: 20px;">
                        <div class="menu-centered">
                            <div class="large-up-3 medium-up-3 small-up-3 container large-centered">
                                <?php
                                foreach ($articles as $article)
                                {
                                    if ($article->get_authorized() == 1)
                                        $article->vignette(0);
                                }
                                ?>
                            </div>
                        </div>
                    </div>
            <?php } ?>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
    </body>
</html>
