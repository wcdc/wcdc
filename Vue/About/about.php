<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/19/16
 * Time: 1:41 PM
 */
?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
<!--        <link rel="stylesheet" type="text/css" href="../../css/app.css">-->
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
<!--        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>-->
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
    </head>

    <body>
        <div  class="corpus">

            <div class="menu-centered contact-title1">The Concept</div>
            <div class="contact-bar"></div>
            <div class="rond"></div>


            <div class="row" style="padding: 50px; border-bottom: solid 10px darkgray; background-color: lightgray; position: relative; top: -50px;">

<!--                Donnez votre opinion sur toutes choses qui vous concernent ou qui vous tiennent à cœur.-->
<!--                Vous aimez quelques choses, vous détestez quelques choses, faites le savoir.-->
<!--                “Working Concept Dying Concept” est la première plateforme permettant à chacun de devenir blogueur professionnel et avoir ses propres suiveurs/ suiveuses.-->
<!--                Grace à vous, “Working Concept Dying Concept”  est le site qui donne l’opportunité de faire mieux professionnellement et personnellement.-->
<!--                Quand vous critiquez un produit, un endroit, une situation, une personne… Vous avez aussi tôt l’opportunité de présenter ce que vous pensez mieux ou la voie à suivre. Faire évoluer les choses de façon constructive,-->
<!--                voilà l’essence de « Working Concept Dying Concept ».-->


                Give your opinion on everything you have a strong opinion about.
                Whether you like or hate something, let it be known !
                "Working Concept Dying Concept" is the first platform allowing anyone to become a professional blogger with his own followers.
                Thanks to you, "Working Concept Dying concept" is the site that gives opportunity to do better professionally and personally.
                When you review a product, a place, a scene, a person... You have the opportunity to present an alternative you think is better.
                Making things evolve through constructive criticism is the essence of "Working Concept Dying Concept".


<!--                <div id="right" class="small-6 medium-6 large-6 column perso">-->
<!--                    <div id="droite" class="rond2 about"><img src="../../img/PastedGraphic-2.png" class="aboutimg"></div>-->
<!--                    <p class="art description">Mr. Henri Hubert<br/>-->
<!--                        General Director<br/><br/></p>-->
<!--                    <p class="art">35 years experience in the fashion and-->
<!--                        communication industry worldwide.<br/>-->
<!--                        Expert in strategic and creative thinking in the-->
<!--                        luxury industry.<br/>-->
<!--                        Pioneer of new concept of branding with creative-->
<!--                        talent.<br/>-->
<!--                        13 years of creative direction experience in-->
<!--                        Vietnam. Pioneer of the Fashion industry vision-->
<!--                        in the country.<br/>-->
<!--                        Expert in fashion events and models coaching.</p>-->
<!--                </div>-->
<!--                <div id="left" class="small-6 medium-6 large-6 column perso">-->
<!--                    <div id="gauche" class="rond2 about"><img src="../../img/PastedGraphic-1.png" class="aboutimg"></div>-->
<!--                    <p class="art description">Aldo Belkouar<br/>-->
<!--                        Fashion Photographer<br/>-->
<!--                        Art Director<br/></p>-->
<!--                    <p class="art">Inspiring talent ; 360° avant-garde vision 20 years Experience in the luxury industry as-->
<!--                        artistic and creative director in Paris, Milan and New-York, Italy, Brazil, USA, France, Vietnam,-->
<!--                        Morocco. As a journalist and fashion photographer and artistic director, Aldo has spend over 20 years-->
<!--                        traveling around and have experienced in Fashion, Beauty and hospitality Business. (Hotels, Restaurants, Cafe, Luxury store...)-->
<!--                        located al over the world.<br/>-->
<!--                        Aldo is Fluent in 5 languages, including Arabic. Lives between NYC, Paris and Vietnam-->
<!--                    </p>-->
<!--                </div>-->
            </div>

<!--            <div class="contact-bar"></div>-->
<!--            <div class="rond"></div>-->
<!---->
<!--            <div class="row" style="border-bottom: solid 10px darkgray; background-color: lightgray; position: relative; top: -50px;">-->
<!--                <div id="right" class="small-6 medium-6 large-6 column perso">-->
<!--                    <div id="droite" class="rond2 about"><img src="../../img/aaverty.jpg" class="aboutimg"></div>-->
<!--                    <p class="art description">Alexis Averty<br/>-->
<!--                        Architect in digital technology<br/><br/></p>-->
<!--                    <p class="art">35 years experience in the fashion and-->
<!--                        communication industry worldwide.<br/>-->
<!--                        Expert in strategic and creative thinking in the-->
<!--                        luxury industry.<br/>-->
<!--                        Pioneer of new concept of branding with creative-->
<!--                        talent.<br/>-->
<!--                        13 years of creative direction experience in-->
<!--                        Vietnam. Pioneer of the Fashion industry vision-->
<!--                        in the country.<br/>-->
<!--                        Expert in fashion events and models coaching.</p>-->
<!--                </div>-->
<!--                <div id="left" class="small-6 medium-6 large-6 column perso">-->
<!--                    <div id="gauche" class="rond2 about"><img src="../../img/qmoinat.jpg" class="aboutimg"></div>-->
<!--                    <p class="art description">Quentin Moinat<br/>-->
<!--                        architect in digital technology<br/></p>-->
<!--                    <p class="art">Inspiring talent ; 360° avant-garde vision 20 years Experience in the luxury industry as-->
<!--                        artistic and creative director in Paris, Milan and New-York, Italy, Brazil, USA, France, Vietnam,-->
<!--                        Morocco. As a journalist and fashion photographer and artistic director, Aldo has spend over 20 years-->
<!--                        traveling around and have experienced in Fashion, Beauty and hospitality Business. (Hotels, Restaurants, Cafe, Luxury store...)-->
<!--                        located al over the world.<br/>-->
<!--                        Aldo is Fluent in 5 languages, including Arabic. Lives between NYC, Paris and Vietnam-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </body>
</html>