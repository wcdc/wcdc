<?php

?>

<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, minimum-scale=0.1">
        <title>WCDC</title>
        <link rel="stylesheet" href="../../css/app.css">
        <link rel="stylesheet" href="../../css/login.css">
        <link rel="stylesheet" href="../../css/article.css">
        <link rel="stylesheet" href="../../css/nous.css">
        <link rel="stylesheet" href="../../css/style.css" />
        <link rel="stylesheet" href="../../css/jquery.Jcrop.min.css">
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
        <script type="text/javascript" src="../../js/jquery.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.Jcrop.min.js"></script>

        <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>

    <body style="background: none;">


        <div class="row corpus">
            <?php include_once ("../../Vue/notifications/notif.php"); ?>
            <br/>
            <br/>
            <div class="menu-centered contact-title2" style="padding-right: 20px;padding-left: 20px;">Submit Your Working And Dying Concept</div>
            <br/>

            <br/>
            <hr/>
            <br/>
            <div class="menu-centered">
                <p style="font-size: 20px;;">Let's not compare apples to oranges</p>
            </div>
            <div class="">

                <div class="large-6 small-12 columns">

                    <div class="large-12 small-12 column large-centered small-centered">

                        <fieldset class="fieldset">

                            <legend class="contact-title2">Working</legend>

                            <div class="large-6 small-6 large-centered small-centered">

                                <div class="input-group">

                                    <form id="tt" enctype="multipart/form-data">
                                        <label for="wc_img" class="button">Upload image*</label>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="3145728" />
                                        <input type="hidden" id="wc_win_width" name="wc_win_width">
                                        <input type="file" accept="image/*" id="wc_img" name="wc_img" class="show-for-sr" required>
                                    </form>

                                </div>

                            </div>

                            <div class="input-group" style="margin-bottom: 0;">

                                <div id="image_preview">

                                    <div>

                                        <div class="thumbnail hidden">


                                        </div>

                                        <div id="hide" hidden>
                                            <form id="crop1" enctype="multipart/form-data">
                                                <input type="hidden" id="x" name="x" required/>
                                                <input type="hidden" id="y" name="y" />
                                                <input type="hidden" id="w" name="w" />
                                                <input type="hidden" id="h" name="h" />
                                                <input type="hidden" id="wc_image" name="wc_image" value="">
                                                <br/>
                                                <input id="wc_crop_btn" type="submit" value="Crop Image" class="button" style="margin: 20px auto 20px auto;" required/>
                                            </form>
                                            <br/>
                                            <br/>
                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="">

                                <div class="input-group">

                                    <textarea class="input-group-field" rows="8" cols="" id="contenu" name="wc_content" placeholder="Article content*" minlength="20" maxlength="500" required ></textarea>

                                    <br/>

                                    <input type="text" name="wc_name" placeholder="Working name*" minlength="4" maxlength="20" required>

                                    <input type="text" name="wc_address" placeholder="Working address" minlength="10" maxlength="200">

                                    <input type="text" name="wc_link" placeholder="Working link" minlength="10" maxlength="2000">

                                    <select class="select_submit large-12 small-12" name="wc_city" style="padding: 0px 0px 0px 10px;" required>
                                        <option value="" disabled selected>Select your working city*</option>
                                        <option class="opt" value="Global">Global</option>
                                        <?php

                                        while ($ville = $villes->fetch())
                                        { ?>

                                                <option class="opt" value="<?php echo $ville['name'];?>"><?php echo $ville['name']; ?></option>
                                <?php   }
                                        ?>

                                    </select>

                                </div>

                            </div>

                        </fieldset>

                    </div>

                </div>

            </div>



            <div class="">

                <div class="large-6 small-12 columns">

                    <div class="large-12 small-12 column large-centered small-centered">

                        <fieldset class="fieldset">

                            <legend class="contact-title2">Dying</legend>

                            <div class="large-6 small-6 large-centered small-centered">

                                <div class="input-group">

                                    <form id="tt2" enctype="multipart/form-data">

                                        <label for="dc_img" class="button">Upload image*</label>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="3145728" />
                                        <input type="hidden" id="dc_win_width" name="dc_win_width">
                                        <input type="file" accept="image/*" id="dc_img" name="dc_img" class="show-for-sr" required>
                                    </form>
                                </div>

                            </div>

                            <div class="input-group" style="margin-bottom: 0;">

                                <div id="image_preview2">

                                    <div>

                                        <div class="thumbnail hidden">


                                        </div>

                                        <div id="hide2" hidden>

                                            <form id="crop2" enctype="multipart/form-data">
                                                <input type="hidden" id="x1" name="x1" required/>
                                                <input type="hidden" id="y1" name="y1" />
                                                <input type="hidden" id="w1" name="w1" />
                                                <input type="hidden" id="h1" name="h1" />
                                                <input type="hidden" id="dc_image" name="dc_image" value="">
                                                <input type="submit" value="Crop Image" class="button" style="margin: 20px auto 20px auto;" required/>
                                            </form>

                                            <br/>
                                            <br/>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="">

                                <div class="input-group">

                                    <textarea class="input-group-field" rows="8" cols="" id="contenu" name="dc_content" placeholder="Article content*" minlength="20" maxlength="500" required ></textarea>

                                    <br/>

                                    <input type="text" name="dc_name" placeholder="Dying name*" minlength="4" maxlength="20" required>

                                    <input type="text" name="dc_address" placeholder="Dying address" minlength="10" maxlength="200">

                                    <input type="text" name="dc_link" placeholder="Dying link" minlength="10" maxlength="2000">

                                    <select class="select_submit large-12 small-12" name="dc_city" style="padding: 0px 0px 0px 10px;" required>
                                        <option value="" disabled selected>Select your dying city*</option>
                                        <option class="opt" value="Global">Global</option>
                                        <?php
                                        while ($ville = $villes2->fetch())
                                        { ?>
                                                <option class="opt" value="<?php echo $ville['name'];?>"><?php echo $ville['name']; ?></option>
                                        <?php
                                        }
                                        ?>

                                    </select>

                                </div>

                            </div>

                        </fieldset>

                        <br/>
                        <br/>

                    </div>

                </div>

                <br/>
                <hr/>
                <p id="champs_requis" style="margin-left: 55px;">* required fields</p>
                <br/>

                <div class="large-6 small-6 large-centered menu-centered small-centered">

                    <div class="large-12 small-12 columns">


                        <select id="category" class="select_submit large-12 small-12" name="category" style="padding: 0px 0px 0px 10px;" required>
                            <option disabled selected>Select your category*</option>
                            <?php

                            while ($cat = $categories->fetch())
                            { ?>
                                    <option class="opt" value="<?php echo $cat['name'];?>"><?php echo $cat['name']; ?></option>
                            <?php
                            }
                            ?>

                        </select>
                    </div>

                    <div class="large-12 small-12 columns">


                        <select class="select_submit large-12 small-12" name="category2" style="padding: 0px 0px 0px 10px;">
                            <option value="" disabled selected>Select your second category</option>
                            <?php

                            while ($cat2 = $categories2->fetch())
                            { ?>
                                <option class="opt" value="<?php echo $cat2['name'];?>"><?php echo $cat2['name']; ?></option>
                                <?php
                            }
                            ?>

                        </select>
                    </div>
                    <div class="large-12 small-12 columns">


                        <select class="select_submit large-12 small-12" name="category3" style="padding: 0px 0px 0px 10px;">
                            <option value="" disabled selected>Select your third category</option>
                            <?php

                            while ($cat3 = $categories3->fetch())
                            { ?>
                                <option class="opt" value="<?php echo $cat3['name'];?>"><?php echo $cat3['name']; ?></option>
                                <?php
                            }
                            ?>

                        </select>
                    </div>

                    <div class="">

                        <div class="l">
                            <input type="hidden" id="wc_img_crop" name="wc_image_crop" value="">
                            <input type="hidden" id="dc_img_crop" name="dc_image_crop" value="">
                            <div class="g-recaptcha" data-sitekey="6Lfw5yETAAAAAOhDLewAGvUhNNDVlnMyBqH_pNW1" style="margin-left: 25px;margin-bottom: 100px;"></div>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <input id="submit" class="button" type="submit">
                        </div>
                        <br/>
                        <br/>


                    </div>

                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
        <br/>


        <script type="text/javascript">

            $('#submit').on('click', function (e)
            {
                $( ".corpus" ).wrapAll("<form id=\"ttt\" method=\"post\" action=\"../../Modele/Submit_Your_Concept/Submit_concept.php\" />");
                if ($('#w').val() && $('#w1').val() && $('#contenu').val() && $('#category').selectedOptions)
                {
                    $('#ttt').submit();
                }
            });



            $('#wc_img').on('change', function (e) {

                var $form = $('#tt');
                $form.find('#wc_win_width').val(window.innerWidth);
                var url = '../../Modele/User_interface/upload.php';
                var formdata = (window.FormData) ? new FormData($form[0]) : null;
                var data = (formdata !== null) ? formdata : $form.serialize();


                $.ajax(url, {
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {

                        $('#image_preview').find('.thumbnail').removeClass('hidden');
                        $('#image_preview').find('.thumbnail').html(data);
                        $('#hide').show();
                        var img = $('#thumbnail').attr('src');
                        $('#wc_image').val(img);
                        $('#crop1').show();
                        $('#wc_img').val('');
                    },
                    statusCode: {
                        404: function () {
                            $('.err').text("Error : unable to download file");
                        },
                        405 : function () {
                            $('.err').text("Error : File is too large (Size may not exceed 3 MB)");
                        },
                        406 : function () {
                            $('.err').text("Error : Wrong file format (Please use .png, .jpg, .jpeg or .gif)");
                        },
                        407 : function () {
                            $('.err').text("Some problemes has happened");
                        }
                    },
                    error: function () {
                        
                        $('html,body').animate({scrollTop: 0}, 'slow');
                        $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $('.err').show().slideDown(1000);
                        $('.err').find('.notif-btn').on('click', function () {
                            $('.err').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $('.err').hide().slideUp();
                        }, 60000);
                    }
                });
            });


        </script>


        <script type="text/javascript">

            $('#dc_img').on('change', function () {

                var $form = $('#tt2');
                $form.find('#dc_win_width').val(window.innerWidth);
                var url = '../../Modele/User_interface/upload.php';
                var formdata = (window.FormData) ? new FormData($form[0]) : null;
                var data = (formdata !== null) ? formdata : $form.serialize();


                $.ajax(url, {
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {

                        $('#image_preview2').find('.thumbnail').removeClass('hidden');
                        $('#image_preview2').find('.thumbnail').html(data).slideDown();
                        $('#hide2').show();
                        var img = $('#thumbnail2').attr('src');
                        $('#dc_image').val(img);
                        $('#crop2').show();
                        $('#dc_img').val('');
                    },
                    statusCode: {
                        404: function () {
                            $('.err').text("Error : unable to download file");
                        },
                        405 : function () {
                            $('.err').text("Error : File is too large (Size may not exceed 3 MB)");
                        },
                        406 : function () {
                            $('.err').text("Error : Wrong file format (Please use .png, .jpg, .jpeg or .gif)");
                        },
                        407 : function () {
                            $('.err').text("Some problemes has happened");
                        }
                    },
                    error: function () {
                        
                        $('html,body').animate({scrollTop: 0}, 'slow');
                        $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $('.err').show().slideDown(1000);
                        $('.err').find('.notif-btn').on('click', function () {
                            $('.err').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $('.err').hide().slideUp();
                        }, 60000);
                    }
                });
            });


        </script>

        <script type="text/javascript">

            $(function () {

                $('#thumbnail') . Jcrop({
                    aspectRatio: 1,
                    minSize: [20, 20],
                    maxSize: [600, 600],
                    setSelect: [0, 0, 50, 50],
                    onSelect: updateCoords
                });

            });

            function updateCoords(c) {
                $('#x') . val(c . x);
                $('#y') . val(c . y);
                $('#w') . val(c . w);
                $('#h') . val(c . h);
            }

            function checkCoords()
            {
                if (parseInt($('#w') . val())) return true;
                alert('Please select a crop region then press submit.');
                return false;
            }


        </script>


        <script type="text/javascript">

            $(function () {

                $('#thumbnail2') . Jcrop({
                    aspectRatio: 1,
                    minSize: [20, 20],
                    maxSize: [600, 600],
                    setSelect: [0, 0, 50, 50],
                    onSelect: updateCoords1
                });

            });

            function updateCoords1(c) {
                $('#x1') . val(c . x);
                $('#y1') . val(c . y);
                $('#w1') . val(c . w);
                $('#h1') . val(c . h);
            }

            function checkCoords1()
            {
                if (parseInt($('#w1') . val())) return true;
                alert('Please select a crop region then press submit.');
                return false;
            }

        </script>



        <script type="text/javascript">

            $('#crop1').on('submit', function (e) {
                e.preventDefault();
                var $form = $(this);
                var url = '../../Modele/User_interface/crop.php';
                var formdata = (window.FormData) ? new FormData($form[0]) : null;
                var data = (formdata !== null) ? formdata : $form.serialize();


                $.ajax(url, {
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {

                        //$('#image_preview').find('.thumbnail').removeClass('hidden');
                        $('#image_preview').find('.thumbnail').html('');
                        $('#image_preview').find('.thumbnail').html(data);
                        $('#wc_img_crop').val($('#thumbnail').attr('src'));
                        $('#crop1').hide();
                    }
                });
            });

        </script>


        <script type="text/javascript">

            $('#crop2').on('submit', function (e) {
                e.preventDefault();
                var $form = $(this);
                var url = '../../Modele/User_interface/crop.php';
                var formdata = (window.FormData) ? new FormData($form[0]) : null;
                var data = (formdata !== null) ? formdata : $form.serialize();


                $.ajax(url, {
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {

                        //$('#image_preview').find('.thumbnail').removeClass('hidden');
                        $('#image_preview2').find('.thumbnail').html('');
                        $('#image_preview2').find('.thumbnail').html(data);
                        $('#dc_img_crop').val($('#thumbnail2').attr('src'));
                        $('#crop2').hide();
                    }
                });
            });

        </script>

        <?php include_once ("../../Vue/notifications/js.php"); ?>
    </body>
</html>