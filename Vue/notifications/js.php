<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/17/16
 * Time: 10:40 AM
 */
?>

<script type="text/javascript">
    <?php
    if (isset($message))
    { ?>
        $(document).ready(function (e) {
        $('.message').show().slideDown(1000);
        $('.message').find('.notif-btn').on('click', function () {
        $('.message').hide().slideUp(1000);
        });
        setTimeout(function () {
        $('.message').hide().slideUp(1000);
        }, 60000);
        });
    <?php    }
    else if (isset($error))
    { ?>
        $(document).ready(function (e) {
        $('.err').show().slideDown(1000);
        $('.err').find('.notif-btn').on('click', function () {
        $('.err').hide().slideUp(1000);
        });
        setTimeout(function () {
        $('.err').hide().slideUp(1000);
        }, 60000);
        });
    <?php   }
    ?>
</script>