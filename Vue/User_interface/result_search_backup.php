<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/22/16
 * Time: 5:18 PM
 */

include_once ("../../Class/Article.php");
include_once ("../../install.php");

if (isset($_POST['search']))
{
    $search = "%" . filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);

    $req = $bdd->prepare('SELECT `id` FROM article WHERE `wc_name` LIKE :search OR `dc_name` LIKE :search ORDER BY `date_published` DESC');
    $req->bindParam(":search", $search, PDO::PARAM_STR, strlen($search));
    $req->execute();
    $art = $req->fetchAll();
    $req->closeCursor();

    $i = 0;
    $tab = array();
    while ($art[$i]) {
        $tmp = new Article($art[$i++]['id']);
        if ($tmp->get_authorized() == -3)
            array_push($tab, $tmp);
    }
    $articles = $tab;

    $nb_res = 0;

    if ($articles)
        $nb_res += count($articles);
}

if (isset($articles))
{
    foreach ($articles as $article) {
        $article->vignette(2);
    }
}
