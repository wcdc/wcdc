<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/30/16
 * Time: 6:00 PM
 */

?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" href="../../css/login.css"/>
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body style="background: none;">
        <div class="corpus" style="top: 140px;">
            <?php include_once ("../../Vue/notifications/notif.php"); ?>
            <div class="menu-centered contact-title2">Renitialize your password</div>
            <hr/>
            <div id="form" class="">

                <form id="my_form" action="../../Modele/User_interface/lost_password.php" method="post">

                    <div class="row float-center">

<!--                        <div class="large-6 columns">-->
<!--                            <input class="thumbnail" type="text" name="Pseudo" placeholder="Pseudo*" required>-->
<!--                        </div>-->

                        <div class="large-6 columns large-centered medium-centered small-centered">
                            <input class="thumbnail" type="email" name="Mail" placeholder="Mail*" required minlength="6">
                        </div>

                    </div>

                    <div class="row float-left">
                        <div class="g-recaptcha" data-sitekey="6Lei-yMTAAAAAMdyrx7OsGwirPNFDsK9lPyFpTlq" style="margin-bottom: 20px; margin-left: 25px;"></div>
                        <br/>
                        <p id="champs_requis">* required fields</p>

                    </div>

                    <div class="row">

                        <div id="div-captcha" class="large-3 large-centered column">
                            <input class="button log" type="submit" name="submit" value="Send" style="margin-bottom: 10px;">
                        </div>

                    </div>

                </form>

            </div>
        </div>
        <?php include_once ("../../Vue/notifications/js.php"); ?>
    </body>

</html>