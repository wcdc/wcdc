<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/13/16
 * Time: 10:52 AM
 */

?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
    </head>
    <body style="background: none;">
        <div class="corpus">
            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
            <div class="menu-centered" style="margin-bottom: 0px;">
                <ul class="dropdown menu" data-dropdown-menu>
                    <?php include_once ("../../Vue/User_interface/submenu.php"); ?>
                </ul>
            </div>
            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
            <div class="menu-centered contact-title2">Concept awaiting validation</div>
            <hr size="1" align="center">
            <div class="row" style="margin-top: 20px;">
                <div class="menu-centered">
                    <div class="large-up-3 medium-up-3 small-up-3 container large-centered">
                        <?php
        
                        foreach ($tab_articles as $article)
                        {
                            if ($article->get_authorized() == 0)
                                $article->vignette(2);
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="menu-centered contact-title2">Articles awaiting supression</div>
            <hr size="1" align="center">
            <div class="row" style="margin-top: 20px;">
                <div class="menu-centered">
                    <div class="large-up-3 medium-up-3 small-up-3 container large-centered">
                        <?php

                        foreach ($tab_articles as $article)
                        {
                            if ($article->get_authorized() == -2)
                                $article->vignette(3);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
