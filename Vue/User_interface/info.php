<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/13/16
 * Time: 3:35 PM
 */

session_start();
?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
        <link rel="shortcut icon" href="../../img/Working_Logo.png">

        <script type="text/javascript">
            $(document).ready(function () {
                $('#search-article').on('submit', function (e) {
                    e.preventDefault();
                    var search = $('#search').val();
                    $.ajax('../../Vue/User_interface/result_search_articles.php', {
                        type : 'POST',
                        data: {search: search},
                        success: function (data) {
                            $('#hide2').show();
                            $('#search').val('');
                            $('#result2').html(data).fadeIn();

                        }
                    });
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#search-backup').on('submit', function (e) {
                    e.preventDefault();
                    var search = $('#search-backup-input').val();
                    $.ajax('../../Vue/User_interface/result_search_backup.php', {
                        type : 'POST',
                        data: {search: search},
                        success: function (data) {
                            $('#hide-backup').show();
                            $('#search-backup-input').val('');
                            $('#result-backup').html(data).fadeIn();

                        }
                    });
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#search-user').on('submit', function (e) {
                    e.preventDefault();
                    var search = $('#search').val();
                    $.ajax('../../Vue/User_interface/result_search_user_by_brand.php', {
                        type : 'POST',
                        data: {search: search},
                        success: function (data) {
                            $('#hide').show();
                            $('#search').val('');
                            $('#result').html(data).fadeIn();
                        }
                    });
                });
            });
        </script>

    </head>

    <body style="background: none;">
        <div class="corpus">
                <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
                <div class="menu-centered" style="margin-bottom: 0px;">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <?php include_once ("../../Vue/User_interface/submenu.php"); ?>
                    </ul>
                </div>
                <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>

            <?php include_once ("../../Vue/notifications/notif.php"); ?>

            <div class="menu-centered contact-title2">Category</div>
            <hr size="1" align="center">

            <div class="row">

                <div class="large-6 medium-6 small-6 columns">

                    <form method="post" action="../../Modele/User_interface/info.php">

                        <div class="input-group">

                            <input type="text" class="input-group-field" name="add_categorie" placeholder="Enter new categorie" style="max-width: 200px; margin-left: 93px;" required>

                            <div class="input-group-button">

                                <input type="submit" class="button" value="Submit">

                            </div>

                        </div>

                    </form>

                </div>

                <div class="large-6 medium-6 small-6 columns">

                    <form method="post" action="../../Modele/User_interface/info.php">

                        <div class="input-group">

                            <select class="select_submit" name="del_categorie" style="padding: 0px 0px 0px 10px;">

                                <option value="" selected>Delete a category</option>

                                <?php

                                while ($cat = $category->fetch())
                                {
                                    ?>
                                    <option class="opt" value="<?php echo $cat['name'];?>"><?php echo $cat['name']; ?></option>
                                    <?php
                                }
                                ?>

                            </select>

                            <div class="input-group-button">

                                <input type="submit" class="button" value="Submit" style="position: relative; top: -8px; height: 39px; margin-right: 100px;">

                            </div>

                        </div>

                    </form>

                </div>

            </div>
            
            <div class="menu-centered contact-title2">City</div>
            <hr size="1" align="center">

            <div class="row">

                <div class="large-6 medium-6 small-6 columns">

                    <form method="post" action="../../Modele/User_interface/info.php">

                        <div class="input-group">

                            <input type="text" class="input-group-field" name="add_city" placeholder="Enter new city" style="max-width: 200px; margin-left: 93px;" required>

                            <div class="input-group-button">

                                <input type="submit" class="button" value="Submit">

                            </div>

                        </div>

                    </form>

                </div>

                <div class="large-6 medium-6 small-6 columns">

                    <form method="post" action="../../Modele/User_interface/info.php">

                        <div class="input-group">

                            <select class="select_submit" name="del_city" style="padding: 0px 0px 0px 10px;">

                                <option value="" selected>Delete a city</option>

                                <?php

                                while ($ville = $city->fetch())
                                {
                                    ?>
                                    <option class="opt" value="<?php echo $ville['name'];?>"><?php echo $ville['name']; ?></option>
                                    <?php
                                }
                                ?>

                            </select>

                            <div class="input-group-button">

                                <input type="submit" class="button" value="Submit" style="position: relative; top: -8px; height: 39px; margin-right: 100px;">

                            </div>

                        </div>

                    </form>

                </div>

            </div>


            <div class="menu-centered contact-title2">Brand</div>
            <hr size="1" align="center">

            <div class="row">

                <div class="large-6 medium-6 small-6 columns">

                    <form method="post" action="../../Modele/User_interface/info.php">

                        <div class="input-group">

                            <input type="text" class="input-group-field" name="add_brand" placeholder="Enter new brand" style="max-width: 200px; margin-left: 93px;" required>

                            <div class="input-group-button">

                                <input type="submit" class="button" value="Submit">

                            </div>

                        </div>

                    </form>

                </div>

                <div class="large-6 medium-6 small-6 columns">

                    <form method="post" action="../../Modele/User_interface/info.php">

                        <div class="input-group">

                            <select class="select_submit" name="del_brand" style="padding: 0px 0px 0px 10px;">

                                <option value="" selected>Delete a brand</option>

                                <?php

                                while ($marque = $brand->fetch())
                                {
                                    ?>
                                    <option class="opt" value="<?php echo $marque['name'];?>"><?php echo $marque['name']; ?></option>
                                    <?php
                                }
                                ?>

                            </select>

                            <div class="input-group-button">

                                <input type="submit" class="button" value="Submit" style="position: relative; top: -8px; height: 39px; margin-right: 100px;">

                            </div>

                        </div>

                    </form>

                </div>

            </div>

            <div class="contact-title2 menu-centered">Search brand</div>
            <hr/>
            <form id="search-user" method="post">
                <div class="row">
                    <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered input-group">
                        <select class="select_submit" name="del_brand" style="padding: 0px 0px 0px 10px; max-width: 400px; margin-left: 160px; margin-top: 15px;">

                            <option value="" selected>Search users by brand</option>

                            <?php

                            while ($marque = $brand2->fetch())
                            {
                                ?>
                                <option class="opt" value="<?php echo $marque['name'];?>"><?php echo $marque['name']; ?></option>
                                <?php
                            }
                            ?>

                        </select>                        <div class="input-group-button">
                            <input type="submit" class="button" value="Search" style="margin-right: 155px;">
                        </div>
                    </div>
                </div>
            </form>

            <div id="hide" hidden>
                <br/>
                <div class="contact-title2 menu-centered">Users</div>
                <hr style="width: 50%;"/>
                <div class="row" style="margin-top: 20px;">
                    <div class="menu-centered">
                        <div id="result" class="large-up-3 medium-up-3 small-up-3 container large-centered">

                        </div>
                    </div>
                </div>
            </div>

            <div class="menu-centered contact-title2">Concepts Site</div>

            <hr size="1" align="center">

            <div class="" style="margin-top: 20px;">

                <div class="menu-centered">

                    <div class="large-up-3 medium-up-3 small-up-3 container large-centered">

                        <form id="search-article" method="post">

                            <div class="row">

                                <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered input-group">

                                    <input type="text" id="search" class="input-group-field" name="search" placeholder="Enter an article working or dying name" style="max-width: 400px;margin-left: 160px;" >

                                    <div class="input-group-button">

                                        <input type="submit" class="button" value="Search" style="margin-right: 155px;">

                                    </div>

                                </div>

                            </div>

                        </form>

                        <div id="hide2" hidden>

                            <br/>
                            <div class="row">

                                <div id="result2" class="large-up-3 medium-up-3 small-up-3 large-centered" style="padding: inherit;">

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="menu-centered contact-title2">Backup Concepts</div>

            <hr size="1" align="center">

            <div class="" style="margin-top: 20px;">

                <div class="menu-centered">

                    <div class="large-up-3 medium-up-3 small-up-3 container large-centered">

                        <form id="search-backup" method="post">

                            <div class="row">

                                <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered input-group">

                                    <input type="text" id="search-backup-input" class="input-group-field" name="search" placeholder="Enter an article working or dying name" style="max-width: 400px;margin-left: 160px;" >

                                    <div class="input-group-button">

                                        <input type="submit" class="button" value="Search" style="margin-right: 155px;">

                                    </div>

                                </div>

                            </div>

                        </form>

                        <div id="hide-backup" hidden>

                            <br/>
                            <div class="row">

                                <div id="result-backup" class="large-up-3 medium-up-3 small-up-3 large-centered" style="padding: inherit;">

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <?php include_once ("../../Vue/notifications/js.php"); ?>

    </body>

</html>
