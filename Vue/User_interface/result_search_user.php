<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/14/16
 * Time: 2:47 PM
 */

include_once ("../../Class/User.php");
include_once ("../../install.php");

if (isset($_POST['search']))
{
    $search = "%" . filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);
    $search .= "%";
    $req = $bdd->prepare('SELECT `pseudo` FROM users WHERE `pseudo` LIKE :search ORDER BY `pseudo` ASC');
    $req->bindParam(":search", strtolower($search), PDO::PARAM_STR, strlen($search));
    $req->execute();
    $user = $req->fetchAll();
    $req->closeCursor();

    $i = 0;
    $tab = array();
    while ($user[$i]) {
        $tmp = new User($user[$i++]['pseudo']);
        array_push($tab, $tmp);
    }
    $users = $tab;
}

if (isset($users))
{
    foreach ($users as $user) {
        if ($user->get_right_access() <= 4)
            $user->vignette(1);
    }
}
