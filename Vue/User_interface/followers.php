<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 7/20/16
 * Time: 10:22 AM
 */

?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
    </head>
    <body style="background: none;">
        <div class="corpus">
            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
            <div class="menu-centered" style="margin-bottom: 0px;">
                <ul class="dropdown menu" data-dropdown-menu>
                    <?php include_once ("../../Vue/User_interface/submenu.php"); ?>
                </ul>
            </div>
            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
                
            <div class="menu-centered contact-title2">Your Followers</div>
            <hr size="1" align="center">
            <div class="row" style="margin-top: 20px;">
                <div class="menu-centered">
                    <div id="result" class="large-up-3 medium-up-3 small-up-3 container large-centered">
                        <?php
                            foreach ($followers as $follower)
                            {
                                $follower = new User($follower);
                                $follower->vignette(0);
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="menu-centered contact-title2">Currently Following</div>
            <hr size="1" align="center">
            <div class="row" style="margin-top: 20px;">
                <div class="menu-centered">
                    <div id="result" class="large-up-3 medium-up-3 small-up-3 container large-centered">
                        <?php
                        foreach ($follow as $followed)
                        {
                            $followed = new User($followed);
                            $followed->vignette(0);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>