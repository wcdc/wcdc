<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/11/16
 * Time: 7:17 PM
 */

?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
        <script type="text/javascript" src="../../js/vendor/jquery.js"></script>
        <script type="text/javascript">
            $(function(){
                $('#change_passwd').on('submit', function (e) {
                    var form = document.getElementById("change_passwd");
                    var i = document.createElement("input");
                    var i2 = document.createElement("input");
                    form.appendChild(i);
                    form.appendChild(i2);
                    var old_pass = document.getElementById("old_passwd");
                    var new_pass = document.getElementById("new_passwd");
                    var new_conf_pass = document.getElementById("new_confirm_passwd");
                    i.name = "i";
                    i.type = "hidden";
                    i2.name = "i2";
                    i2.type = "hidden";

                    var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
                    if (!re.test(new_pass.value))
                    {
                        alert("Your password must contain at least one lowercase letter, one uppercase letter, one number and have a minimum size of 8 characters");
                        e.preventDefault();
                    }
                    if (new_pass.value == new_conf_pass.value) {
                        i.value = sha512(old_pass.value);
                        i2.value = sha512(new_pass.value);
                        old_pass.value = "";
                        new_pass.value = "";
                        new_conf_pass.value = "";
                    }
                    else
                    {
                        alert("Password and Confirmation of Password are not same");
                        e.preventDefault();
                    }
                });
            });
        </script>
    </head>
    <body style="background: none;">
        <div class="corpus">

                <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
                    <div class="menu-centered" style="margin-bottom: 0px;">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <?php include_once ("../../Vue/User_interface/submenu.php"); ?>
                    </ul>
                </div>
                <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>


            <?php include_once ("../../Vue/notifications/notif.php"); ?>

            <br/>
            <br/>
            <br/>
            <br/>
            <br/>

            <div>
                <div class="menu-centered">
                    <div id="droite" class="rond2 about"><img src="data:image/;base64,<?php echo $user->get_path_profil_photo(); ?>" class="aboutimg"></div>
                    <br/>
                    <form method="post" action="../../Modele/User_interface/modif.php" enctype="multipart/form-data">
                        <label for="profil_photo" class="button">Upload photo</label>
                        <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                        <input type="file" accept="image/*" id="profil_photo" name="profil_photo" class="show-for-sr" accept="image/*" onchange="this.form.submit();" required>
                    </form>
                </div>
                <hr/>
                <br/>
                <form method="post" action="../../Modele/User_interface/modif.php">
                    <div class="row">
                        <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered input-group">
                            <input type="text" id="new_pseudo" class="input-group-field" name="new_pseudo" placeholder="Enter your new pseudo" style="max-width: 290px; position:relative; left: 210px;" required>
                            <div class="input-group-button">
                                <input type="submit" class="button" value="Submit" style="position: relative; right: 215px;">
                            </div>
                        </div>
                    </div>
                </form>

                <hr/>
                <br/>
                <form id="change_passwd" method="post" action="../../Modele/User_interface/modif.php">
                    <div class="row">
                        <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered">

                            <input type="password" id="old_passwd" name="old_passwd" placeholder="Enter your actual password" required>
                            <input type="password" id="new_passwd" name="new_passwd" placeholder="Enter your new password" required>
                            <input type="password" id="new_confirm_passwd" name="new_confirm_passwd" placeholder="Confirm your new password" required>
                            <input type="submit" class="button" value="Submit" style="margin-left: 150px;">
                        </div>
                    </div>
                </form>


                <hr/>
                <br/>
                <form method="post" action="../../Modele/User_interface/modif.php">
                    <div class="row">
                        <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered">
                            <input type="text" id="new_mail" class="input-group-field" name="new_mail" placeholder="Enter your new email" required>
                            <div class="input-group-button">
                                <input type="submit" class="button" value="Submit" style="position: relative;top: -40px; left: 145px;">
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <script src="../../js/vendor/sha512.js"></script>
        <?php include_once ("../../Vue/notifications/js.php"); ?>
    </body>
</html>