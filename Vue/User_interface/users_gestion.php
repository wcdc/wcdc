<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/14/16
 * Time: 2:12 PM
 */
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
        <script type="text/javascript">
            $(document).ready(function () {
                $('#search-user').on('submit', function (e) {
                    e.preventDefault();
                    var search = $('#search').val();
                    $.ajax('../../Vue/User_interface/result_search_user.php', {
                        type : 'POST',
                        data: {search: search},
                        success: function (data) {
                            $('#hide').show();
                            $('#search').val('');
                            $('#result').html(data).fadeIn();
                        }
                    });
                });
            });
        </script>
    </head>
    <body style="background: none;">
        <div class="corpus">
            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
            <div class="menu-centered" style="margin-bottom: 0px;">
                <ul class="dropdown menu" data-dropdown-menu>
                    <?php include_once ("../../Vue/User_interface/submenu.php"); ?>
                </ul>
            </div>
            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>

            <div class="contact-title2 menu-centered">Search an user</div>
            <hr/>
            <form id="search-user" method="post">
                <div class="row">
                    <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered input-group">
                        <input type="text" id="search" class="input-group-field" name="search" placeholder="Enter an user pseudo" style="max-width: 200px; margin-left: 266px;">
                        <div class="input-group-button">
                            <input type="submit" class="button" value="Search" style="margin-right: 250px;">
                        </div>
                    </div>
                </div>
            </form>

            <div id="hide" hidden>
                <br/>
                <div class="contact-title2 menu-centered">Users</div>
                    <hr/>
                    <div class="row" style="margin-top: 20px;">
                        <div class="menu-centered">
                            <div id="result" class="large-up-3 medium-up-3 small-up-3 container large-centered">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>