<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/14/16
 * Time: 2:47 PM
 */

include_once ("../../Class/User.php");
include_once ("../../Class/Brand.php");
include_once ("../../install.php");

if (isset($_POST['search']))
{
    $search = "%" . filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);
    $search .= "%";
    $req = $bdd->prepare('SELECT * FROM brand WHERE `name` LIKE :search');
    $req->bindParam(":search", strtolower($search), PDO::PARAM_STR, strlen($search));
    $req->execute();
    $brand = $req->fetchAll();
    $req->closeCursor();

    $brand = new Brand($brand[0]['name']);

    $tab = array();

    foreach ($brand->getTabPseudoAssoc() as $use)
    {
        $tmp = new User($use['pseudo']);
        array_push($tab, $tmp);
    }
    $users = $tab;
}

if (isset($users))
{
    foreach ($users as $user) {
        $user->vignette(1);
    }
}
