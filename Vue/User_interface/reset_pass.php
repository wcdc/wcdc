<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/30/16
 * Time: 6:41 PM
 */

session_start();
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
        <script type="text/javascript" src="../../js/vendor/jquery.js"></script>
        <script type="text/javascript">
            $(function(){
                $('#change_passwd').on('submit', function (e) {
                    var form = document.getElementById("change_passwd");
                    var i = document.createElement("input");
                    form.appendChild(i);
                    var new_pass = document.getElementById("new_passwd");
                    var new_conf_pass = document.getElementById("new_confirm_passwd");
                    i.name = "i";
                    i.type = "hidden";

                    var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
                    if (!re.test(new_pass.value))
                    {
                        alert("Your password must contain at least one lowercase letter, one uppercase letter, one number and have a minimum size of 8 characters");
                        e.preventDefault();
                    }
                    if (new_pass.value == new_conf_pass.value) {
                        i.value = sha512(new_pass.value);
                        new_pass.value = "";
                        new_conf_pass.value = "";
                    }
                    else
                    {
                        alert("Password and Confirmation of Password are not same");
                        e.preventDefault();
                    }
                });
            });
        </script>
    </head>
    <body style="background: none;">
        <div class="corpus" style="top: 160px;">

            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
            <div class="menu-centered" style="margin-bottom: 0px;">
                <ul class="dropdown menu" data-dropdown-menu>
                    <?php include_once ("../../Vue/User_interface/submenu.php"); ?>
                </ul>
            </div>
            <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>


            <?php include_once ("../../Vue/notifications/notif.php"); ?>

            <br/>

            <div>
                <form id="change_passwd" method="post" action="../../Modele/User_interface/reset_pass.php">
                    <div class="row">
                        <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered">

                            <input type="password" id="new_passwd" name="new_passwd" placeholder="Enter your new password" required>
                            <input type="password" id="new_confirm_passwd" name="new_confirm_passwd" placeholder="Confirm your new password" required>
                            <input type="submit" class="button" value="Submit" style="margin-left: 150px;">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="../../js/vendor/sha512.js"></script>
        <?php include_once ("../../Vue/notifications/js.php"); ?>
    </body>
</html>
