<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/13/16
 * Time: 10:49 AM
 */

$user = new User($_SESSION['logged_on_user']);

?>

<?php

if ($user->get_right_access() == 0)
{ ?>
    <li class="item">
        <a href="#"><?php echo $_SESSION['logged_on_user']; ?></a>
        <ul id="fp" class="menu">
            <li class="item"><a href="../../Controleur/User_interface/user.php?pseudo=<?php echo $_SESSION['logged_on_user']; ?>">View profil</a></li>
            <li class="item"><a href="../../Controleur/User_interface/modif.php">Edit profil</a></li>
            <li class="item"><a href="">Followers</a></li>
        </ul>
    </li>
    <li class="item"><a href="../../Controleur/User_interface/edit.php">Edit articles/Delete Concepts</a></li>
    <li class="item"><a href="../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php">Submit concept</a></li>
<?php }
else if ($user->get_right_access() == 1)
{ ?>
    <li class="item">
        <a href="#"><?php echo $_SESSION['logged_on_user']; ?></a>
        <ul id="fp" class="menu">
            <li class="item"><a href="../../Controleur/User_interface/user.php?pseudo=<?php echo $_SESSION['logged_on_user']; ?>">View profil</a></li>
            <li class="item"><a href="../../Controleur/User_interface/modif.php">Edit profil</a></li>
            <li class="item"><a href="">Followers</a></li>
        </ul>
    </li>
    <li class="item"><a href="../../Controleur/User_interface/edit.php">Edit articles/Delete Concepts</a></li>
    <li class="item"><a href="../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php">Submit concept</a></li>
    <li class="item"><a href="../../Controleur/User_interface/articles_gestion.php">Manage Concepts</a></li>
    <li class="item"><a href="../../Controleur/User_interface/users_gestion.php">Manage users</a></li>
<?php }
else if ($user->get_right_access() == 2 || $user->get_right_access() >= 3)
{ ?>
    <li class="item">
        <a href="#"><?php echo $_SESSION['logged_on_user']; ?></a>
        <ul id="fp" class="menu" style="width: auto;">
            <li class="item"><a href="../../Controleur/User_interface/user.php?pseudo=<?php echo $_SESSION['logged_on_user']; ?>">View profil</a></li>
            <li class="item"><a href="../../Controleur/User_interface/modif.php">Edit profil</a></li>
            <li class="item"><a href="../../Controleur/User_interface/followers.php">Followers</a></li>
        </ul>
    </li>
    <li class="item"><a href="../../Controleur/User_interface/edit.php">Edit/Delete Concepts</a></li>
    <li class="item"><a href="../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php">Submit concept</a></li>
    <li class="item"><a href="../../Controleur/User_interface/articles_gestion.php">Manage Concepts</a></li>
    <li class="item"><a href="../../Controleur/User_interface/users_gestion.php">Manage users</a></li>
    <li class="item"><a href="../../Controleur/User_interface/info.php">Manage site</a></li>
<?php } ?>

<script src="../../js/vendor/what-input.js"></script>
<script src="../../js/vendor/foundation.js"></script>
<script src="../../js/app.js"></script>
