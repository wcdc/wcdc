<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/21/16
 * Time: 8:22 AM
 */

session_start();

if (isset($_SESSION['logged_on_user']))
    $adm = new User($_SESSION['logged_on_user']);

?>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>WCDC</title>
        <link rel="stylesheet" type="text/css" href="../../css/article.css">
        <link rel="stylesheet" type="text/css" href="../../css/nous.css">
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css"/>
        <link rel="shortcut icon" href="../../img/Working_Logo.png">
    </head>
    <body style="background: none;">
        <div class="corpus">
            <?php
                $pseudo = $_GET['pseudo'];
                if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'] && urldecode($pseudo) === $_SESSION['logged_on_user'])
                { ?>
                    <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
                    <div class="menu-centered" style="margin-bottom: 0px;">
                        <ul class="dropdown menu" data-dropdown-menu>
                            <?php include_once ("../../Vue/User_interface/submenu.php"); ?>
                        </ul>
                    </div>
                    <div class="contact-bar" style="background-color: black; height: 10px; margin: 0px;"></div>
                    <?php include_once ("../../Vue/notifications/notif.php"); ?>
        <?php   }
            else if ($adm && $adm->get_right_access() >= 3 && $_GET['v'] == 'i')
            {
                $user = new User(urldecode($pseudo));
                ?>
                <div class="row menu-centered">
                    <div class="small-12 medium-12 large-12 columns" >
                        <div class="small-6 medium-6 large-6 columns">
                            <a href="../../Controleur/User_interface/users_gestion.php"><button class="button" style="float: left; margin-top: 20px;">Back</button></a>
                        </div>
                        <?php   if ($user->get_right_access() < 4)
                                { ?>
                                    <div style="clear: both"></div>
                                    <div class="small-6 medium-6 large-6 columns">
                                        <a href="../../Controleur/User_interface/users_gestion.php?pseudo=<?php echo $_GET['pseudo'];?>&val=1"><button class="button success" style="float: right; margin-top: 20px;">Authorize</button></a>
                                    </div>
                                    <div class="small-6 medium-6 large-6 columns">
                                        <a href="../../Controleur/User_interface/users_gestion.php?pseudo=<?php echo $_GET['pseudo'];?>&val=-1"><button class="button alert" style="float: left; margin-top: 20px;">Ban</button></a>
                                    </div>
                        <?php   } ?>
                    </div>
                </div>
       <?php }
            else
            {
                if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
                {
                    $suiveur = new User($_SESSION['logged_on_user']);
                    $suivi = new User(urldecode($pseudo));
                    //print_r($suiveur->get_follow_users());
                    if ($suiveur->get_follow_users() && (in_array($suivi->get_pseudo(), $suiveur->get_follow_users())))
                    { ?>
                        <div class="button" style="position: absolute; top: 20px; right: 20px"><button id="follow">Unfollow</button></div>
            <?php   }
                    else
                    { ?>
                        <div class="button" style="position: absolute; top: 20px; right: 20px"><button id="follow">Follow</button></div>
            <?php   } ?>
                    <input type="hidden" id="pseudo_suivi" value="<?php echo urlencode($_GET['pseudo']); ?>">
                    <input type="hidden" id="pseudo_suiveur" value="<?php echo urlencode($_SESSION['logged_on_user']);?>">
       <?php    }
            }
            $user = new User(urldecode($pseudo));
            $user->profil();
            $pseudo = $user->get_pseudo();
            ?>

            <div class="menu-centered contact-title2"><?php echo "$pseudo's "; ?>concepts</div>
            <hr size="1" align="center">
            <div class="row" style="margin-top: 20px;">
                <div class="menu-centered">
                    <div class="large-up-3 medium-up-3 small-up-3 container large-centered">
                        <?php
                        $tab_articles = $user->get_user_articles();
                        foreach ($tab_articles as $article)
                        {
                            if ($article->get_authorized() == 1)
                                $article->vignette(0);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <?php include_once ("../../Vue/notifications/js.php"); ?>
        
        <script type="text/javascript">
            $('#follow').on('click', function () {

                var url = "../../Modele/User_interface/follow.php";
                var pseudo_suivi = $('#pseudo_suivi').val();
                var pseudo_suiveur = $('#pseudo_suiveur').val();
                var action = $('#follow').text();

                if (action == "Follow")
                    action = 1;
                else if (action == "Unfollow")
                    action = -1;

                $.ajax(url, {
                    type: 'POST',
                    data: {pseudo_suiveur: pseudo_suiveur, pseudo_suivi: pseudo_suivi, action: action},
                    success: function (data) {
                        $('.button').replaceWith(data);
                        $('.message').text("Now you follow " + pseudo_suivi);
                        $('.message').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $('.message').show().slideDown(1000);
                        $('.message').find('.notif-btn').on('click', function () {
                            $('.message').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $('.err').hide().slideUp();
                        }, 60000);
                    },
                    error: function () {
                        $('html,body').animate({scrollTop: 0}, 'slow');
                        $('.err').text("Somes problems was happened, Sorry for desagrement");
                        $('.err').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $('.err').show().slideDown(1000);
                        $('.err').find('.notif-btn').on('click', function () {
                            $('.err').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $('.err').hide().slideUp();
                        }, 60000);
                    }
                });
            });
        </script>
    </body>
</html>