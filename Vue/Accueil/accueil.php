<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Working Concept Dying Concept”est la première plateforme permettant à chacun de devenir blogueur professionnel et avoir ses propres followers.Donner votre opinion !">
    <title>WCDC</title>
    <link rel="stylesheet" href="../../css/app.css">
    <link rel="stylesheet" href="../../css/nous.css">
    <link rel="stylesheet" href="../../css/login.css">
    <link rel="shortcut icon" href="../../img/Favicon.png">
</head>
<header>
    <div id="slidtop" class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container" style="height: 300px;">
            <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span style="color: blue;">&#9664;&#xFE0E;</button>
            <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
            <?php
                $articles = $slide_top->get_articles();
                foreach ($articles as $article)
                    $article->slide_top();
            ?>
        </ul>
    </div>
</header>
<body>


    <div id ="topmenu">

        <?php
        if (isset($_SESSION['logged_on_user']))
        {
            ?>
            <div id="bar">

                <ul id="log" class="dropdown menu logg" data-dropdown-menu data-closing-time>
                    <li><a class="logout" style="color: white; padding: inherit; padding-top: 12px; padding-right: 5px;" href="../../Controleur/User_interface/user.php?pseudo=<?php echo $_SESSION['logged_on_user']; ?>"><?php $pseudo = $_SESSION['logged_on_user']; echo "My Account &nbsp;($pseudo)"; ?></a></li>
                    <li><a  class="logout" style="color: white; padding: inherit; padding-top: 12px; padding-right: 5px;" href="../../Modele/User_interface/logout.php">&nbsp;&nbsp;Logout</a></li>
                </ul>
            </div>
            <?php
        }
        else
        {
            ?>
            <div id="bar">

                <ul id="log" class="dropdown menu logg" data-dropdown-menu style="float: right;">
                    <li><a class="login" style="color: white; padding: inherit; padding-top: 12px; padding-right: 5px;" href="../../Controleur/Login_Register/login.php" style="text-decoration: none; font-size: 16px;">Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Register &nbsp;</a></li>
                </ul>
            </div>
            <?php
        }
        ?>
        <div id="mc" class="menu-centered">
            <div><a href="../../Controleur/Accueil/accueil.php"><img id="logo" src="../../img/WORKING_DYING.png"/></a></div>
            <ul class="dropdown menu" data-dropdown-menu  style="">
                <li class="item">
                    <a href="#">Category | City</a href="#">
                    <ul id="fp" class="menu">
                        <form method="post" action="../../Controleur/Selection_Articles/select_articles.php">
                            <div id="button">
                                <input id="btn-select" type="submit" value="Submit">
                            </div>
                            <div class="row">
                                <div class="small-6 medium-6 large-6 columns cat" style="padding-left: 5px; padding-top: 5px;">
                                    
                                    <?php
                                        while ($cat = $categories->fetch()){
                                            echo '<div class="checkbox1"><input id="'.$cat['name'].'"type="checkbox" name="cat[]" value="'.$cat['name'].'"/><label for="'.$cat['name'].'">'.$cat['name'].'</label></div>';
                                        }
                                    ?>
                                    
                                </div>
                                <div class="small-6 medium-6 large-6 columns villes" style="padding-left: 5px; padding-top: 5px;">

                                    <?php
                                    while ($ville = $villes->fetch()){
                                        echo '<div class="checkbox1"><input id="'.$ville['name'].'"type="checkbox" name="villes[]" value="'.$ville['name'].'"/><label for="'.$ville['name'].'">'.$ville['name'].'</label></div>';
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                        </form>
                    </ul>
                </li>
                <li class="item"><a href="../../Controleur/About/about.php  ">The Concept</a></li>
                <li class="item"><a href="../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php">Submit your Concept</a></li>
                <li class="item"><a href="../../Controleur/Contact/contact.php">Contact</a></li>
            </ul>
        </div>
        <div id="bar" style="position: relative; top: -8px">
            <div class="search-bar">
                <form method="post" action="../../Controleur/Search/search.php">
                    <div>
                        <input id="search-bar1" name="search" type="search" placeholder="Search" class="search-bar1">
                        <input id="search-button1" type="submit" value="Search" class="log search-button1">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="corps">
        

        <?php include_once ("../../Vue/notifications/notif.php"); ?>

<!--        <div class="menu-centered contact-title2" style="padding-top: 20px;">Articles of the World</div>-->
<!--        <hr size="1" align="center" width="80%">-->
<!--        <div id="caroussel" class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit style="margin-top: 20px;">-->
<!--            <ul class="orbit-container">-->
<!--                <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>-->
<!--                <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>-->
<!--                --><?php
//                    $articles = $slide_down->get_articles();
//                    foreach ($articles as $article)
//                        $article->slide_down();
//                ?>
<!--            </ul>-->
<!--        </div>-->
<!---->
<!--        <div class="contact-bar" style="margin-top: 80px;"></div>-->
<!--        <div class="rond"></div>-->
        <div class="menu-centered contact-title2">Recent concepts</div>
        <hr size="1" align="center" width="80%">


        <div class="row" style="margin-top: 20px;">
            <div class="menu-centered">
                <div class="large-up-3 medium-up-3 small-up-3 container large-centered">
                    <?php
                        foreach ($recents_articles as $item)
                        {
                            if($item->get_authorized() == 1)
                                $item->vignette(0);
                        }
                    ?>
                </div>
            </div>

<!--            --><?php
//                $i = 0;
//                while($i < 50)
//                {
//                    echo "<br/>";
//                    $i++;
//                }
//            ?>

        </div>
    </div>
    <br/>
    <br/>
    <br/>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
        $(function(){
            $(window).scroll(
                    function () {
                        if ($(this).scrollTop() > $( '#slidtop' ).height()) {
                            $('#topmenu').addClass("fixMenu");
                            $('#corps').addClass("fixCar");
                            $('#log').addClass("fixLog");
                            $('#log').removeClass("logg")
                            $('#search-bar1').addClass("fixSearch");
                            $('#search-button1').addClass("fixSearch");
                            $('#search-bar2').addClass("fixSearch");
                            $('#search-button2').addClass("fixSearch");
                            $('#search-bar1').removeClass("search-bar1");
                            $('#search-button1').removeClass("search-button1");
                            $('#search-bar2').removeClass("search-bar2");
                            $('#search-button2').removeClass("search-button2");
                        } else {
                            $('#topmenu').removeClass("fixMenu");
                            $('#corps').removeClass("fixCar");
                            $('#log').removeClass("fixLog");
                            $('#log').addClass("logg");
                            $('#search-bar1').removeClass("fixSearch");
                            $('#search-button1').removeClass("fixSearch")
                            $('#search-bar2').removeClass("fixSearch");
                            $('#search-button2').removeClass("fixSearch");
                            $('#search-bar1').addClass("search-bar1");
                            $('#search-button1').addClass("search-button1");
                            $('#search-bar2').addClass("search-bar2");
                            $('#search-button2').addClass("search-button2");
                        }
                    }
            );
        });

        $('#fp').scroll(function () {
           if ($(this).scrollTop() != 0)
           {
               var top = $(this).scrollTop();
               $('#btn-select').css("position", "relative");
               $('#btn-select').css("top", top);
           }
           else
           {
               $('#btn-select').removeClass("fixButtonSelect");
           }
        });

    </script>
    <script src="../../js/vendor/jquery.js"></script>
    <script src="../../js/vendor/what-input.js"></script>
    <script src="../../js/vendor/foundation.js"></script>
    <script src="../../js/app.js"></script>
    <?php include_once ("../../Vue/notifications/js.php"); ?>
    </body>
</html>