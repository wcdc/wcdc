<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/18/16
 * Time: 7:23 PM
 */
session_start();

if (isset($_GET['t']))
    $token = $_GET['t'];

else if (isset($_GET['m']))
{
    switch ($_GET['m'])
    {
        case 1 :
            $message = "Your concept has been submitted successfully";
            break;
        case 2 :
            $message = "Your account has been created successfully, please check your email for confirmation, the email sent is maybe in your spam";
            break;
        case 3:
            $message = "Your email sent successfully !";
            break;
        case 4:
            $message = "Please check your email for reset your password, the email sent is maybe in your spam";
            break;
    }
}
else if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "Somes problemes has happened, sorry for the desagrement";
            break;
        case 2:
            $error = "Author od the article was not found";
            break;
    }
}
include_once ("../../Class/Article.php");
include_once ("../../Class/User.php");
include_once("../../Class/Slide_top.php");
include_once ("../../Class/Slide_down.php");
include_once ("../../Modele/Accueil/accueil.php");

if (isset($token))
{
    if ($user->get_token_conf() == $_GET['t'] && $user->get_mail_confirmed() == 0)
    {
        $user->set_mail_confirmed_in_bdd(1);
        $user->set_authorized_in_bdd(1);
        $_SESSION['logged_on_user'] = $user->get_pseudo();
        header('Location:../../Controleur/User_interface/user.php?pseudo=' . $_SESSION['logged_on_user'] . '&m=2');
    }
    else if ($user->get_mail_confirmed() == 1) {
        $error = "Your Email is already confirmed";
    }
}
$slide_top = new Slide_top();
$slide_down = new Slide_down();
include_once ("../../Vue/Accueil/accueil.php");
include_once ("../../Vue/Footer/footer.php");