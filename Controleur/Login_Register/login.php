<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/18/16
 * Time: 7:34 PM
 */
session_start();

if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "An account with this pseudo already exist";
            break;

        case 2:
            $error = "An account with this email already exist";
            break;

        case 3:
            $error = "Email syntax error";
            break;

        case 4:
            $error = "Born date is invalid";
            break;
        case 5:
            $error = "Password incorrect";
            break;
        case 6:
            $error = "Mail not confirmed, please check your mail";
            break;
        case 7:
            $error = "Somes problemes has happened, please retry your rester";
            break;
        case 8:
            $error = "Please use recaptcha to confirm your are not a robot";
            break;
        case 9:
            $error = "Wrong pseudo";
            break;
        case 10:
            $error = "Sorry, your account is banned ! Contact an admin for more infos";
            break;
    }
}


include_once ("../Menu/menu.php");
include_once ("../../Vue/Login_Register/login.php");
include_once ("../../Vue/Footer/footer.php");