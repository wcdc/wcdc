<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/14/16
 * Time: 2:17 PM
 */
session_start();


if (isset($_GET['m']))
{
    switch ($_GET['m'])
    {
        case 1 :
            $message = "";
            break;
    }
}
else if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "You don't have the permission to authorized or ban an user";
            break;
    }
}

if (isset($_SESSION['logged_on_user']))
{
    include_once("../../Class/User.php");
    include_once("../../Modele/User_interface/users_gestion.php");
    include_once("../Menu/menu.php");
    include_once("../../Vue/User_interface/users_gestion.php");
    include_once("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}