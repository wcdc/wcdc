<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/12/16
 * Time: 4:55 PM
 */
session_start();

if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "This Working name and Dying name already exist";
            break;
        case 2:
            $error = "Please use recaptcha to confirm your are not a robot";
            break;
        case 3:
            $error = "Width or Height of your image is too large.(maximum 850 pixels X 850 pixels)";
            break;
    }
}

include_once ("../../Class/User.php");
include_once ("../../Class/Article.php");

if (isset($_GET['id']))
    $article = new Article($_GET['id']);

if (isset($_SESSION['logged_on_user']))
    $author = new User($_SESSION['logged_on_user']);

if ($author && $article && $article->get_id_users_of_author() == $author->get_id())
{
    include_once("../Menu/menu.php");
    include_once("../../Modele/User_interface/edit_your_concept.php");
    include_once("../../Vue/User_interface/edit_your_concept.php");
    include_once("../../Vue/Footer/footer.php");
}
else if ($author)
{
    header('Location:../../Controleur/User_interface/user.php?pseudo=' . $_SESSION['logged_on_user'] . '&e=1');
    exit(0);
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}