<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/21/16
 * Time: 8:10 AM
 */


session_start();
include_once ("../../Class/User.php");


if (isset($_GET['m']))
{
    switch ($_GET['m'])
    {
        case 1 :
            $message = "Operation done successful";
            break;
        case 2:
            $message = "Your account and your email is confirmed";
            break;
    }
}
else if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "You don't have permissions to edit this article";
            break;
    }
}

if (isset($_SESSION['logged_on_user']))
{
    $user = new User($_SESSION['logged_on_user']);
    include_once ("../Menu/menu.php");
    include_once ("../../Vue/User_interface/user.php");
    include_once ("../../Vue/Footer/footer.php");
}
else if (isset($_GET['pseudo']))
{
    $pseudo = $_GET['pseudo'];
    $user = new User(urldecode($pseudo));
    include_once ("../Menu/menu.php");
    include_once ("../../Vue/User_interface/user.php");
    include_once ("../../Vue/Footer/footer.php");
}


include_once ("../../Modele/User_interface/user.php");

if (isset($_POST['i'])) 
{
    $_POST['i'] = hash("whirlpool", $_POST['i']);

    if ($user[0]['pseudo'] == strtolower(htmlspecialchars($_POST['pseudo'])) && $user[0]["password"] == $_POST['i'] && $user[0]['mail_confirmed'] == 1) {
        $_SESSION['logged_on_user'] = $user[0]['pseudo'];
        $user = new User($_SESSION['logged_on_user']);
        $pseudo = urlencode($_SESSION['logged_on_user']);
        header('Location:../../Controleur/User_interface/user.php?pseudo=' . $pseudo);
        exit(0);
    }
    else if ($user[0]['password'] !== $_POST['i'])
    {
        header('Location:../../Controleur/Login_Register/login.php?e=5');
        exit(0);
    }
    else if ($user[0]['mail_confirmed'] == 0)
    {
        header('Location:../../Controleur/Login_Register/login.php?e=6');
        exit(0);
    }
    else if ($user[0]['pseudo'] != strtolower(htmlspecialchars($_POST['pseudo'])))
    {
        header('Location:../../Controleur/Login_Register/login.php?e=9');
        exit(0);
    }
    else if ($user[0]['authorized'] != 1)
    {
        header('Location:../../Controleur/Login_Register/login.php?e=10');
        exit(0);
    }
    else
    {
        header('Location: ../../Controleur/Accueil/accueil.php?e=1');
        exit(0);
    }
}
