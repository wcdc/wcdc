<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/11/16
 * Time: 7:16 PM
 */
session_start();

if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "Width or Height of your image is too large.(maximum 1250 pixels X 1250 pixels)";
            break;
        case 2:
            $error = "Somes probleme has happend with this file please try again with other file";
            break;
        case 3:
            $error = "An account with this pseudo already exists";
            break;
        case 4:
            $error = "An account with this email already exists";
            break;
        case 5:
            $error = "Your actual password is incorrect";
            break;
            
    }
}
else if (isset($_GET['m']))
{
    switch ($_GET['m']) 
    {
        case 1:
            $message = "Please check your email for confirmation";
            break;
    }
    
}

if (isset($_SESSION['logged_on_user']))
{
    include_once("../Menu/menu.php");
    include_once("../../Class/User.php");
    $user = new User($_SESSION['logged_on_user']);
    include_once("../../Vue/User_interface/modif.php");
    include_once("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}