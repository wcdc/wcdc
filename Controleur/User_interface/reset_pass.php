<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/30/16
 * Time: 6:32 PM
 */
session_start();

include_once ("../../install.php");
include_once ("../../Class/User.php");

if (isset($_GET['t']))
{
    $token = $_GET['t'];
    
    $req = $bdd->prepare('SELECT * FROM users WHERE `token_conf`= :token');
    $req->bindParam(':token', $token, PDO::PARAM_STR, strlen($token));
    $req->execute();
    
    $tmp = $req->fetchAll();
    $user = new User($tmp[0]['pseudo']);
    $_SESSION['logged_on_user'] = $user->get_pseudo();
    include_once ("../../Controleur/Menu/menu.php");
    include_once ("../../Vue/User_interface/reset_pass.php");
    include_once ("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php?e=42');
}