<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 7/20/16
 * Time: 10:22 AM
 */

session_start();

include_once ("../../Class/User.php");

if (isset($_SESSION['logged_on_user']))
{
    $user = new User($_SESSION['logged_on_user']);
    include_once ("../Menu/menu.php");
    include_once ("../../Modele/User_interface/followers.php");
    include_once ("../../Vue/User_interface/followers.php");
    include_once ("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}