<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/12/16
 * Time: 4:21 PM
 */
session_start();

if (isset($_SESSION['logged_on_user'])) 
{
    include_once("../../Class/User.php");
    include_once("../../Class/Article.php");
    include_once("../Menu/menu.php");
    include_once("../../Vue/User_interface/edit.php");
    include_once("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}