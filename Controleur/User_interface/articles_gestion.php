<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/13/16
 * Time: 10:53 AM
 */
session_start();

if (isset($_SESSION['logged_on_user'])) 
{
    include_once("../../Class/User.php");
    include_once("../../Class/Article.php");
    include_once("../../Modele/User_interface/articles_gestion.php");
    include_once("../Menu/menu.php");
    include_once("../../Vue/User_interface/articles_gestion.php");
    include_once("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}