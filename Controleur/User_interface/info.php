<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/13/16
 * Time: 3:35 PM
 */
session_start();

if (isset($_GET['m']))
{
    switch ($_GET['m'])
    {

    }
}
else if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "This category already exists";
            break;
        case 2:
            $error = "This city already exists";
            break;
        case 3:
            $error = "This article is already in slide top";
            break;
        case 4:
            $error = "This article is already in slide down";
            break;
        case 5:
            $error = "Category or City is incorrect, please retry";
            break;
        case 6:
            $error = "There are already 15 articles in slide, please delete one before";
            break;
        case 7:
            $error = "Not delete a category there is only one category";
            break;
        case 8:
            $error = "Not delete a city there is only one city";
            break;
        case 9:
            $error = "Not delete a brand there is only one brand";
            break;
    }
}

if (isset($_SESSION['logged_on_user']))
{
    include_once("../../Class/Article.php");
    include_once("../../Class/User.php");
    include_once("../../Class/Slide_top.php");
    include_once("../../Class/Slide_down.php");
    include_once("../../Modele/User_interface/info.php");
    include_once("../Menu/menu.php");
    include_once("../../Vue/User_interface/info.php");
    include_once("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}