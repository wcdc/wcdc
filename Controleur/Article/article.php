<?php
/**
 * Created by PhpStorm.
 * User: qmoinat
 * Date: 5/20/16
 * Time: 1:59 PM
 */
session_start();

if (isset($_GET['m']))
{
    switch ($_GET['m'])
    {
        case 1 :
            $message = "Successful";
            break;
    }
}
else if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "You must be signed in to comment this concept";
            break;
    }
}

include_once ("../../Class/Article.php");
include_once ("../../Class/User.php");
$user = new User($_SESSION['logged_on_user']);
if(isset($_GET['id']))
    $article = new Article($_GET['id']);
include_once ("../Menu/menu.php");
include_once ("../../Vue/Article/article.php");
include_once ("../../Vue/Footer/footer.php");