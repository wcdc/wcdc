<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/18/16
 * Time: 7:43 PM
 */

session_start();

if (isset($_GET['e']))
{
    switch ($_GET['e'])
    {
        case 1:
            $error = "This Working name and Dying name already exist";
            break;
        case 2:
            $error = "Please use recaptcha to confirm your are not a robot";
            break;
        case 3:
            $error = "Please fill the formulaire completly (Image, Contents, City, Name, Category)";
            break;
        case 4:
            $error = "Please if you use global city you must use it for working city and dying city.";
            break;
    }
}

if (isset($_SESSION['logged_on_user']))
{
    include_once ("../Menu/menu.php");
    include_once ("../../Modele/Submit_Your_Concept/Submit_concept.php");
    include_once ("../../Vue/Submit_Your_Concept/Submit_Your_Concept.php");
    include_once ("../../Vue/Footer/footer.php");
}
else
{
    header('Location:../../Controleur/Login_Register/login.php');
    exit(0);
}
