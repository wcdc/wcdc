<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/18/16
 * Time: 7:24 PM
 */

include_once ("../../install.php");

$req = $bdd->prepare('SELECT name FROM categories ORDER BY name ASC');
$req->execute();
$categories = $req;

$req = $bdd->prepare('SELECT name FROM villes ORDER BY name ASC');
$req->execute();
$villes = $req;

$req = $bdd->prepare('SELECT `pseudo` FROM users WHERE `token_conf`= :token');
$req->bindParam(":token", $token, PDO::PARAM_STR, strlen($token));
$req->execute();
$use = $req->fetchAll();
$req->closeCursor();

if (isset($use[0]['pseudo']))
    $user = new User($use[0]['pseudo']);

$req = $bdd->prepare('SELECT `id` FROM `article` WHERE 1 ORDER BY `date_published` DESC LIMIT 24');
$req->execute();
$recents_articles = $req->fetchAll();
$i = 0;
$tab = array();
while ($recents_articles[$i]['id'])
{
    $tmp = new Article($recents_articles[$i++]['id']);
    array_push($tab, $tmp);
}
$recents_articles = $tab;

$req->closeCursor();