<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/18/16
 * Time: 6:12 PM
 */

session_start();
include_once ("../../Class/Commentaires.php");
include_once ("../../Class/User.php");

if (isset($_POST['id']))
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
if (isset($id))
{
    $commentaire = new Commentaires($id);
    $user = new User($_SESSION['logged_on_user']);
    if ($user->get_pseudo() === $commentaire->get_pseudo_author())
    {
        printf('

                <form class="eddit-comm-form" method="post" style="width: 400px; height: 50px; position: relative; top: -105px; left: 100px;">
                    <div class="row">
                        <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered input-group">
                            <input type="text" id="eddit-comm-input" class="input-group-field" name="new-comm" value="%s" style="max-width" required autofocus>
                            <input type="hidden" value="%d">
                        </div>
                    </div>
                </form>
                <script type="text/javascript">

            $(\'.eddit-comm-form\').on(\'submit\', function (e) {
                e.preventDefault();
                var form = $(this);
//                var formdata = (window.FormData) ? new FormData($form[0]) : null;
//                var data = (formdata !== null) ? formdata : $form.serialize();
                var new_comm = form.find(\'input[name=\"new-comm\"]\').val();
                var id = form.find(\'input[type=\"hidden\"]\').val();
                
                var url = "../../Modele/User_interface/submit-edit-comm.php";

                $.ajax(url, {
                    type: \'POST\',
                    data: {new_comm: new_comm, id: id},
                    success: function (data) {
                        form.replaceWith(data);
                    },
                    error: function () {
                        $(\'.err\').text("Somes problems was happened, Sorry for desagrement");
                        $(\'.err\').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                        $(\'.err\').show().slideDown(1000);
                        $(\'.err\').find(\'.notif-btn\').on(\'click\', function () {
                            $(\'.err\').hide().slideUp(1000);
                        });
                        setTimeout(function () {
                            $(\'.err\').hide().slideUp();
                        }, 60000);
                    }
                });
            });

        </script>', $commentaire->get_commentaire(), $commentaire->get_id(), $commentaire->get_id());
    }
    else
        header('HTTP/1.0 404');
}