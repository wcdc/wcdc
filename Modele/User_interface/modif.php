<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/11/16
 * Time: 7:17 PM
 */
session_start();

define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);

include_once ("../../Class/User.php");

$user = new User($_SESSION['logged_on_user']);

if ($_FILES['profil_photo']['error'])
{
    header('Location:../../Controleur/User_interface/modif.php?e=2');
    exit(0);
}

if (isset($_FILES['profil_photo']))
{
    $uploads_dir = '../../image_profil/';
    if (!file_exists($uploads_dir))
        mkdir($uploads_dir);
    $tmp = $_FILES['profil_photo']['tmp_name'];
    $name = $_FILES['profil_photo']['name'];


    $ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
    $ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg');

    $ExtensionPresumee = explode('.', $name);
    $ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
    if ($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png')
    {
        if ($_FILES['size'] < 3 * MB) {
            if (move_uploaded_file($tmp, "$uploads_dir/$name"))
            {
                $img = "$uploads_dir/$name";

                $TailleImageChoisie = getimagesize($img);

                $NouvelleLargeur = 600;

                $Reduction = ( ($NouvelleLargeur * 100)/$TailleImageChoisie[0] );

                $NouvelleHauteur = ( ($TailleImageChoisie[1] * $Reduction)/100 );

                if($ExtensionPresumee == "jpg" || $ExtensionPresumee == "jpeg")
                {
                    $imgChoose = imagecreatefromjpeg($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagejpeg($NouvelleImage , $new_name, 100);
                }
                else if ($ExtensionPresumee == "png")
                {
                    $imgChoose = imagecreatefrompng($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagepng($NouvelleImage , $new_name);
                }
                else if ($ExtensionPresumee == "gif")
                {
                    $imgChoose = imagecreatefromgif($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagegif($NouvelleImage , $new_name);
                }
                else
                    header('HTTP/1.0 205');

                $img = base64_encode(file_get_contents($new_name));
                $user->set_path_profil_photo_in_bdd($img);
                header('Location:../../Controleur/User_interface/user.php?pseudo=' . $_SESSION['logged_on_user']);
                exit(0);
            }
        } else {
            header('Location:../../Contorleur/User_interface/modif.php?e=1');
            exit(0);
        }
    }
}

if (isset($_POST['new_pseudo']))
    $user->set_pseudo_in_bdd(htmlspecialchars(filter_input(INPUT_POST, 'new_pseudo', FILTER_SANITIZE_STRING)));

if (isset($_POST['i'], $_POST['i2']))
    $user->set_password_in_bdd($_POST['i'], $_POST['i2']);

if (isset($_POST['new_mail']))
    $user->set_mail_in_bdd($_POST['new_mail']);
