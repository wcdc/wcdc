<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/21/16
 * Time: 8:19 AM
 */

session_start();

include_once ("../../install.php");


if (isset($_POST['pseudo'], $_POST['i']))
{
    $pass = hash("whirlpool", $_POST['i']);
    $prep = $bdd->prepare('SELECT * FROM users WHERE `pseudo`= :pseudo AND `password`= :passwd');
    $prep->bindParam(":pseudo", htmlspecialchars($_POST['pseudo']), PDO::PARAM_STR, strlen($_POST['pseudo']));
    $prep->bindParam(":passwd", $pass, PDO::PARAM_STR, strlen($pass));
    $prep->execute();
    $user = $prep->fetchAll();
    $prep->closeCursor();
}