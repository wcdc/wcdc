<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 7/20/16
 * Time: 10:31 AM
 */

session_start();

include_once ("../../Class/User.php");

$user = new User($_SESSION['logged_on_user']);

$followers = $user->get_followers();
$follow = $user->get_follow_users();