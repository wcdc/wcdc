<?php
/**
 * Created by PhpStorm.
 * User: alexisaverty
 * Date: 19/06/2016
 * Time: 11:46
 */
//print_r($_POST);

include_once ("../../install.php");
include_once ("../../Class/Commentaires.php");

if (isset($_POST['id'],$_POST['new_comm']))
{
    $commentary = filter_input(INPUT_POST, 'new_comm', FILTER_SANITIZE_STRING);
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
    date_default_timezone_set("EUROPE/PARIS");
    $last_modified = date("Y-m-d H:i:s");
    $req = $bdd->prepare('UPDATE `commentaires` SET `commentaire`= :commentaire, `last_modified`= :last_modified WHERE `id`= :id');
    $req->bindParam(":commentaire", $commentary, PDO::PARAM_STR, strlen($commentary));
    $req->bindParam(":last_modified", $last_modified, PDO::PARAM_STR, strlen($last_modified));
    $req->bindParam(":id", $id, PDO::PARAM_INT);
    $req->execute();

    $commentaire = new Commentaires($id);
    printf('
                <p id="edit-%d" class="commentary">%s</p>', $commentaire->get_id(), $commentaire->get_commentaire());
}