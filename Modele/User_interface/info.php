<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/13/16
 * Time: 3:35 PM
 */

include_once ("../../install.php");
include_once ("../../Class/Article.php");
include_once ("../../Class/User.php");

$req = $bdd->prepare('SELECT `id` FROM article WHERE `authorized`= 1');
$req->execute();
$articles = $req->fetchAll();

$i = 0;
$tab = array();
while ($articles[$i]['id'])
{
    $tmp = new Article($articles[$i++]['id']);
    array_push($tab, $tmp);
}
$tab_articles = $tab;

if (isset($_GET['st']) || isset($_GET['sd']))
{
    $article = new Article($_GET['id']);
    $slide_top = new Slide_top();
    $slide_down = new Slide_down();
    if ($_GET['st'] == -1)
        $slide_top->delete_article_in_bdd($article->get_id());
    else if ($_GET['st'] == 1)
        $slide_top->add_article_in_bdd($article->get_id());
    if ($_GET['sd'] == -1)
        $slide_down->delete_article_in_bdd($article->get_id());
    else if ($_GET['sd'] == 1)
        $slide_down->add_article_in_bdd($article->get_id());

//    header("Location:../../Controleur/User_interface/info.php");
//    exit(0);
}

function add_categorie ($categorie)
{
    $connect = new User($_SESSION['logged_on_user']);
    $bdd = $connect->connect();
    $req = $bdd->prepare('SELECT `name` FROM `categories` WHERE `name`= :categorie');
    $req->bindParam(":categorie", $categorie, PDO::PARAM_STR, strlen($categorie));
    $req->execute();
    if ($req->rowCount() > 0)
    {
        header("Location:../../Controleur/User_interface/info.php?e=1");
        exit(0);
    }
    $req->closeCursor();

    $req = $bdd->prepare('INSERT INTO `categories` (`name`) VALUES (:categorie)');
    $req->bindParam(":categorie", $categorie, PDO::PARAM_STR, strlen($categorie));
    $req->execute();
    $req->closeCursor();
    header("Location:../../Controleur/User_interface/info.php");
    exit(0);
}

function del_categorie ($categorie)
{
    $connect = new User($_SESSION['logged_on_user']);
    $bdd = $connect->connect();
    $req = $bdd->prepare('SELECT * FROM `categories` WHERE 1');
    $req->execute();
    $nb_category = $req->rowCount();
    $req->closeCursor();

    if ($nb_category > 1)
    {
        $req = $bdd->prepare('DELETE FROM `categories` WHERE `name`= (:categorie)');
        $req->bindParam(":categorie", $categorie, PDO::PARAM_STR, strlen($categorie));
        $req->execute();
        $req->closeCursor();
        header("Location:../../Controleur/User_interface/info.php");
        exit(0);
    }
    else
    {
        header("Location:../../Controleur/User_interface/info.php?e=7");
        exit(0);
    }
}

function add_city ($city)
{

    $art = new Article($_GET['id']);
    $bdd = $art->connect();
    $req = $bdd->prepare('SELECT `name` FROM `villes` WHERE `name`= :city');
    $req->bindParam(":city", $city, PDO::PARAM_STR, strlen($city));
    $req->execute();
    if ($req->rowCount() > 0)
    {
        header("Location:../../Controleur/User_interface/info.php?e=2");
        exit(0);
    }
    $req->closeCursor();

    $req =$bdd->prepare('INSERT INTO `villes` (`name`) VALUES (:city)');
    $req->bindParam(":city", $city, PDO::PARAM_STR, strlen($city));
    $req->execute();
    $req->closeCursor();
    header("Location:../../Controleur/User_interface/info.php");
    exit(0);}

function del_city($city)
{
    $connect = new User($_SESSION['logged_on_user']);
    $bdd = $connect->connect();
    $req = $bdd->prepare('SELECT * FROM `villes` WHERE 1');
    $req->execute();
    $nb_city = $req->rowCount();
    $req->closeCursor();

    if ($nb_city > 1)
    {
        $req = $bdd->prepare('DELETE FROM `villes` WHERE `name`= (:city)');
        $req->bindParam(":city", $city, PDO::PARAM_STR, strlen($city));
        $req->execute();
        $req->closeCursor();
        header("Location:../../Controleur/User_interface/info.php");
        exit(0);
    }
    else
    {
        header("Location:../../Controleur/User_interface/info.php?e=8");
        exit(0);
    }
}

function add_brand ($brand)
{

    $connect = new User($_SESSION['logged_on_user']);
    $bdd = $connect->connect();
    $req = $bdd->prepare('SELECT `name` FROM `brand` WHERE `name`= :brand');
    $req->bindParam(":brand", $brand, PDO::PARAM_STR, strlen($brand));
    $req->execute();
    if ($req->rowCount() > 0)
    {
        header("Location:../../Controleur/User_interface/info.php?e=2");
        exit(0);
    }
    $req->closeCursor();

    $req =$bdd->prepare('INSERT INTO `brand` (`name`) VALUES (:brand)');
    $req->bindParam(":brand", $brand, PDO::PARAM_STR, strlen($brand));
    $req->execute();
    $req->closeCursor();
    header("Location:../../Controleur/User_interface/info.php");
    exit(0);}

function del_brand($brand)
{
    $connect = new User($_SESSION['logged_on_user']);
    $bdd = $connect->connect();
    $req = $bdd->prepare('SELECT * FROM `brand` WHERE 1');
    $req->execute();
    $nb_brand = $req->rowCount();
    $req->closeCursor();

    if ($nb_brand > 1)
    {
        $req = $bdd->prepare('DELETE FROM `brand` WHERE `name`= (:brand)');
        $req->bindParam(":brand", $brand, PDO::PARAM_STR, strlen($brand));
        $req->execute();
        $req->closeCursor();
        header("Location:../../Controleur/User_interface/info.php");
        exit(0);
    }
    else
    {
        header("Location:../../Controleur/User_interface/info.php?e=9");
        exit(0);
    }
}


if(isset($_POST['add_categorie']) && $_POST['add_categorie'] != "")
    add_categorie(filter_input(INPUT_POST, 'add_categorie', FILTER_SANITIZE_STRING));
else if ($_POST['del_categorie'])
    del_categorie(filter_input(INPUT_POST, 'del_categorie', FILTER_SANITIZE_STRING));

if(isset($_POST['add_city']) && $_POST['add_city'] != "")
    add_city(filter_input(INPUT_POST, 'add_city', FILTER_SANITIZE_STRING));
else if ($_POST['del_city'])
    del_city(filter_input(INPUT_POST, 'del_city', FILTER_SANITIZE_STRING));

if(isset($_POST['add_brand']) && $_POST['add_brand'] != "")
    add_brand(filter_input(INPUT_POST, 'add_brand', FILTER_SANITIZE_STRING));
else if ($_POST['del_brand'])
    del_brand(filter_input(INPUT_POST, 'del_brand', FILTER_SANITIZE_STRING));

if (isset($_POST['add_categorie']) && $_POST['add_categorie'] == "" || isset($_POST['add_city']) && $_POST['add_city'] == "" || isset($_POST['add_brand']) && $_POST['add_brand'] == "")
{
    header('Location:../../Controleur/User_interface/info.php?e=5');
    exit(0);
}


$category = $bdd->query('SELECT `name` FROM categories ORDER BY name ASC');
$city = $bdd->query('SELECT `name` FROM villes ORDER BY name ASC');
$brand = $bdd->query('SELECT `name` FROM brand ORDER BY name ASC');
$brand2 = $bdd->query('SELECT `name` FROM brand ORDER BY name ASC');
