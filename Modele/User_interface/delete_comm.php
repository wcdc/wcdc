<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/18/16
 * Time: 6:12 PM
 */

session_start();
include_once ("../../install.php");
include_once ("../../Class/Commentaires.php");

if (isset($_POST['id']))
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

if (isset($id))
{
    $commentaire = new Commentaires($id);
    $user = new User($_SESSION['logged_on_user']);
    if($commentaire->get_pseudo_author() === $user->get_pseudo() || $user->get_right_access() >= 3)
    {
        $req = $bdd->prepare('DELETE FROM `commentaires` WHERE `id`= :id');
        $req->bindParam(":id", $id, PDO::PARAM_INT);
        $req->execute();
    }
    else
        header('HTTP/1.0 404');
}