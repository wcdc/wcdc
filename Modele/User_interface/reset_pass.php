<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/30/16
 * Time: 7:10 PM
 */

session_start();

include_once ("../../Class/User.php");

$user = new User($_SESSION['logged_on_user']);

if (isset($_POST['i']))
    $user->reset_password_in_bdd($_POST['i']);
else
{
    header('Location:../../Controleur/User_interface/user.php');
    exit(0);
}