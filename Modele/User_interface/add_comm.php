<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/17/16
 * Time: 7:41 PM
 */
session_start();

include_once ("../../install.php");
include_once ("../../Class/User.php");

//print_r($_POST);

if(isset($_POST['id_article'], $_POST['commentaire']) && $_SESSION['logged_on_user'])
{
    $commentaire = filter_input(INPUT_POST, 'commentaire', FILTER_SANITIZE_STRING);
    date_default_timezone_set("EUROPE/PARIS");
    $date_published = date("Y-m-d H:i:s");
    $last_modified = $date_published;
    $user = new User($_SESSION['logged_on_user']);
    $id_author = $user->get_id();
    $pseudo_author = $user->get_pseudo();
    $id_article = $_POST['id_article'];

    $req = $bdd->prepare('INSERT INTO `commentaires` (`id_article`, `id_author`, `pseudo_author`, `commentaire`, 
                                                      `date_published`, `last_modified`) 
                                                      VALUES (:id_article, :id_author, :pseudo_author, :commentaire, 
                                                      :date_published, :last_modified)');
    $req->bindParam(":id_article", $id_article, PDO::PARAM_INT);
    $req->bindParam(":id_author", $id_author, PDO::PARAM_INT);
    $req->bindParam(":pseudo_author", $pseudo_author, PDO::PARAM_STR, strlen($pseudo_author));
    $req->bindParam(":commentaire", $commentaire, PDO::PARAM_STR, strlen($commentaire));
    $req->bindParam(":date_published", $date_published, PDO::PARAM_STR, strlen($date_published));
    $req->bindParam(":last_modified", $last_modified, PDO::PARAM_STR, strlen($last_modified));
    $req->execute();
    $req->closeCursor();

    $req = $bdd->prepare('SELECT `id` FROM commentaires WHERE `id_article`= :id_article AND `id_author`= :id_author AND `date_published`= :date_published');
    $req->bindParam(":id_article", $id_article, PDO::PARAM_INT);
    $req->bindParam(":id_author", $id_author, PDO::PARAM_INT);
    $req->bindParam(":date_published", $date_published, PDO::PARAM_STR, strlen($date_published));
    $req->execute();

    if($req->rowCount() > 0 && $req->rowCount() < 2)
    {
        $id = $req->fetch();

        printf('

                        <div id="delete-%d"> 
                            <br/>
                            <div class="commentary_box">
                                <div class="rond3 author_tof">
                                    <img src="data:image/;base64,%s" style="height: 110px; width: 110px;">
                                </div>
                                <p id="edit-%d" class="commentary">%s</p>
                                <div style="margin-top: 20px;">
                                    <p class="author" style="margin-top: 10px;">%s - %s
                                        <a class="hover-link-edit" href="../../Modele/User_interface/edit_comm.php?id=%d" style="padding-left: 20px;">Edit</a>
                                        <a class="hover-link-delete" href="../../Modele/User_interface/delete_comm.php?id=%d" style="padding-left: 20px;">Delete</a>
                                    </p>
                                </div>
                            </div>
                            <br/>
                        </div>
                        
                        
                        <script type="text/javascript">

                            $(\'.hover-link-edit\').on(\'click\', function (e) {
                
                                e.preventDefault();
                                var a = $(this);
                                var url = a.attr(\'href\');
                                var tmp = url.substring(45);
                                var id = parseInt(tmp);
                
                                $.ajax(url, {
                                    type: \'POST\',
                                    data: {id: id},
                                    success: function (data) {
                                        var id2 = "edit-" + id;
                                        $(\'#\' + id2).replaceWith(data);
                                    },
                                    error: function () {
                                        $(\'.err\').text("You don\'t have permissions to edit this comment");
                                        $(\'.err\').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                                        $(\'.err\').show().slideDown(1000);
                                        $(\'.err\').find(\'.notif-btn\').on(\'click\', function () {
                                            $(\'.err\').hide().slideUp(1000);
                                        });
                                        setTimeout(function () {
                                            $(\'.err\').hide().slideUp();
                                        }, 60000);
                                    }
                                });
                            });
                
                
                            $(\'.hover-link-delete\').on(\'click\', function (e) {
                
                                e.preventDefault();
                                var a = $(this);
                                var url = a.attr(\'href\');
                                var tmp = url.substring(47);
                                var id = parseInt(tmp);
                
                                $.ajax(url, {
                                    type: \'POST\',
                                    data: {id: id},
                                    success: function () {
                                        var id2 = "delete-" + id;
                                        //alert(id2);
                                        $(\'#\' + id2).remove();
                                        var nb_comms = $(\'#nb_comms\').text();
                                        nb_comms = parseInt(nb_comms);
                                        nb_comms -= 1;
                                        $(\'#nb_comms\').text(\'\');
                                        $(\'#nb_comms\').text(nb_comms);
                                    },
                                    error: function () {
                                        $(\'.err\').text("You don\'t have permissions to delete this comment");
                                        $(\'.err\').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                                        $(\'.err\').show().slideDown(1000);
                                        $(\'.err\').find(\'.notif-btn\').on(\'click\', function () {
                                            $(\'.err\').hide().slideUp(1000);
                                        });
                                        setTimeout(function () {
                                            $(\'.err\').hide().slideUp();
                                        }, 60000);
                                    }
                                });
                            });
                
                        </script>
                        
                        
                        ', $id['id'], $user->get_path_profil_photo(), $id['id'], $commentaire, $pseudo_author, $last_modified, $id['id'], $id['id']);
    }
}
else
    header('404');