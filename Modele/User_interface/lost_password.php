<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/30/16
 * Time: 5:59 PM
 */

include_once ("../../install.php");
include_once ("../../Class/User.php");


if (isset($_POST['Mail']) && $_POST['g-recaptcha-response'] != "")
{
    $mail = filter_input(INPUT_POST, 'Mail', FILTER_SANITIZE_STRING);
    $mail = filter_var($mail, FILTER_VALIDATE_EMAIL);


    $req = $bdd->prepare('SELECT * FROM users WHERE `mail`= :mail');
    $req->bindParam(':mail', $mail, PDO::PARAM_STR, strlen($mail));
    $req->execute();


    if ($req->rowCount() > 0)
    {
        $tmp = $req->fetchAll();
        $pseudo = (String) $tmp[0]['pseudo'];
        $user = new User($pseudo);
        $user->send_reset_password_mail();
        header('Location:../../Controleur/Accueil/accueil.php?m=4');
        exit(0);
    }
    else
    {
        header('Location:../../Controleur/User_interface/lost_password.php?e=1');
        exit(0);
    }
}
else
{
    header('Location:../../Controleur/Accueil/accueil.php');
    exit(0);
}