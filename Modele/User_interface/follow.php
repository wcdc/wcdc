<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/21/16
 * Time: 1:41 PM
 */

include_once ("../../Class/User.php");


function follow_OR_unfollow_mail($action, $suivi, $suiveur)
{
    if ($action == "follow")
    {
        $user = new User($suiveur);
        $mail= $user->get_mail();
        $objet = "Now you follow " . $suivi;

        // message
        $message = '
         <html>
         <head>
               <title>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</title>
          </head>
          <body>
          <h1>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</h1>
          <p>You followed '. $suivi .', an email will be sent to you to notify his activities.</p>
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: ' . $mail . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($mail, $objet, $message, $headers);
    }
    else if ($action == "unfollow")
    {
        $user = new User($suiveur);
        $mail= $user->get_mail();
        $objet = "Now you unfollow " . $suivi;

        // message
        $message = '
         <html>
         <head>
               <title>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</title>
          </head>
          <body>
          <h1>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</h1>
          <p>You unfollowed '. $suivi .', no email will be sent anymore.</p>
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: ' . $mail . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($mail, $objet, $message, $headers);
    }
}




if (isset($_POST['action']))
{
    if ($_POST['action'] == 1)
    {
        $suivi = new User(urldecode($_POST['pseudo_suivi']));
        $suiveur = new User(urldecode($_POST['pseudo_suiveur']));

        $bdd = $suiveur->connect();
        
        $req = $bdd->prepare('INSERT INTO `follow` (`id_suivi`,`pseudo_suivi`, `id_suiveur`,`pseudo_suiveur`) VALUES (:id_suivi, :pseudo_suivi, :id_suiveur, :pseudo_suiveur)');
        $req->bindParam(":id_suivi", $suivi->get_id(), PDO::PARAM_INT);
        $req->bindParam(":pseudo_suivi", $suivi->get_pseudo(), PDO::PARAM_STR, strlen($suivi->get_pseudo()));
        $req->bindParam(":id_suiveur", $suiveur->get_id(), PDO::PARAM_INT);
        $req->bindParam(":pseudo_suiveur", $suiveur->get_pseudo(), PDO::PARAM_STR, strlen($suiveur->get_pseudo()));
        $req->execute();

        follow_OR_unfollow_mail("follow", $suivi->get_pseudo(), $suiveur->get_pseudo());

        echo '<div class="button" style="position: absolute; top: 20px; right: 20px"><button id="follow">Unfollow</button></div>
                <script type="text/javascript">
                    $(\'#follow\').on(\'click\', function () {
        
                        var url = "../../Modele/User_interface/follow.php";
                        var pseudo_suivi = $(\'#pseudo_suivi\').val();
                        var pseudo_suiveur = $(\'#pseudo_suiveur\').val();
                        var action = $(\'#follow\').text();
        
                        if (action == "Follow")
                            action = 1;
                        else if (action == "Unfollow")
                            action = -1;
        
                        $.ajax(url, {
                            type: \'POST\',
                            data: {pseudo_suiveur: pseudo_suiveur, pseudo_suivi: pseudo_suivi, action: action},
                            success: function (data) {
                                $(\'.button\').replaceWith(data);
                                $(\'.message\').text("Now you follow " + pseudo_suivi);
                                $(\'.message\').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                                $(\'.message\').show().slideDown(1000);
                                $(\'.message\').find(\'.notif-btn\').on(\'click\', function () {
                                    $(\'.message\').hide().slideUp(1000);
                                });
                                setTimeout(function () {
                                    $(\'.err\').hide().slideUp();
                                }, 60000);
                            },
                            error: function () {
                                $(\'.err\').text("Somes problems was happened, Sorry for desagrement");
                                $(\'.err\').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                                $(\'.err\').show().slideDown(1000);
                                $(\'.err\').find(\'.notif-btn\').on(\'click\', function () {
                                    $(\'.err\').hide().slideUp(1000);
                                });
                                setTimeout(function () {
                                    $(\'.err\').hide().slideUp();
                                }, 60000);
                            }
                        });
                    });
                </script>';
    }
    else if ($_POST['action'] == -1)
    {
        $suivi = new User($_POST['pseudo_suivi']);
        $suiveur = new User($_POST['pseudo_suiveur']);

        $bdd = $suiveur->connect();

        $req = $bdd->prepare('DELETE FROM `follow` WHERE `pseudo_suivi`= :pseudo_suivi AND `pseudo_suiveur`= :pseudo_suiveur');
        $req->bindParam(":pseudo_suivi", $suivi->get_pseudo(), PDO::PARAM_STR, strlen($suivi->get_pseudo()));
        $req->bindParam(":pseudo_suiveur", $suiveur->get_pseudo(), PDO::PARAM_STR, strlen($suiveur->get_pseudo()));
        $req->execute();

        follow_OR_unfollow_mail("unfollow", $suivi->get_pseudo(), $suiveur->get_pseudo());

        echo '<div class="button" style="position: absolute; top: 20px; right: 20px"><button id="follow">Follow</button></div>
                    <script type="text/javascript">
                        $(\'#follow\').on(\'click\', function () {
            
                            var url = "../../Modele/User_interface/follow.php";
                            var pseudo_suivi = $(\'#pseudo_suivi\').val();
                            var pseudo_suiveur = $(\'#pseudo_suiveur\').val();
                            var action = $(\'#follow\').text();
            
                            if (action == "Follow")
                                action = 1;
                            else if (action == "Unfollow")
                                action = -1;
            
                            $.ajax(url, {
                                type: \'POST\',
                                data: {pseudo_suiveur: pseudo_suiveur, pseudo_suivi: pseudo_suivi, action: action},
                                success: function (data) {
                                    $(\'.button\').replaceWith(data);
                                    $(\'.message\').text("Now you follow " + pseudo_suivi);
                                    $(\'.message\').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                                    $(\'.message\').show().slideDown(1000);
                                    $(\'.message\').find(\'.notif-btn\').on(\'click\', function () {
                                        $(\'.message\').hide().slideUp(1000);
                                    });
                                    setTimeout(function () {
                                        $(\'.err\').hide().slideUp();
                                    }, 60000);
                                },
                                error: function () {
                                    $(\'.err\').text("Somes problems was happened, Sorry for desagrement");
                                    $(\'.err\').append("<a href=\"#\"><span class=\"notif-btn\">X</span></a>");
                                    $(\'.err\').show().slideDown(1000);
                                    $(\'.err\').find(\'.notif-btn\').on(\'click\', function () {
                                        $(\'.err\').hide().slideUp(1000);
                                    });
                                    setTimeout(function () {
                                        $(\'.err\').hide().slideUp();
                                    }, 60000);
                                }
                            });
                        });
                    </script>';
    }
}