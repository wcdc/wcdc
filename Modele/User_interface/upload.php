<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/15/16
 * Time: 2:40 PM
 */

session_start();

//print_r($_FILES);
//echo "<br/>";
//echo "<br/>";
//print_r($_POST);

define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);

if (isset($_FILES['wc_img']))
{
    $uploads_dir = '../../image_articles/tmp';
    if (!file_exists('../../image_articles'))
        mkdir('../../image_articles');
    if (!file_exists($uploads_dir))
        mkdir($uploads_dir);


    $tmp = $_FILES['wc_img']['tmp_name'];
    $name = $_FILES['wc_img']['name'];

    $ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
    $ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg');

    $ExtensionPresumee = explode('.', $name);
    $ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
    if ($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png')
    {
        if ($_FILES['size'] < 3 * MB)
        {
            if (move_uploaded_file($tmp, "$uploads_dir/$name"))
            {
                $img = "$uploads_dir/$name";

                $TailleImageChoisie = getimagesize($img);
                $win_width = $_POST['wc_win_width'];
                $NouvelleLargeur = (int)$win_width / 4;

                $Reduction = ( ($NouvelleLargeur * 100)/$TailleImageChoisie[0] );

                $NouvelleHauteur = ( ($TailleImageChoisie[1] * $Reduction)/100 );

                if($ExtensionPresumee == "jpg" || $ExtensionPresumee == "jpeg")
                {
                    $imgChoose = imagecreatefromjpeg($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagejpeg($NouvelleImage , $new_name, 100);
                }
                else if ($ExtensionPresumee == "png")
                {
                    $imgChoose = imagecreatefrompng($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagepng($NouvelleImage , $new_name);
                }
                else if ($ExtensionPresumee == "gif")
                {
                    $imgChoose = imagecreatefromgif($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagegif($NouvelleImage , $new_name);
                }
                else
                {
                    header('HTTP/1.0 403');
                    exit(0);
                }


                echo "<script type=\"text/javascript\">
    
                            $(function () {
                    
                                $('#thumbnail') . Jcrop({
                                    aspectRatio: 1,
                                    minSize: [20, 20],
                                    maxSize: [600, 600],
                                    setSelect: [0, 0, 50, 50],
                                    onSelect: updateCoords
                                });
                    
                            });
                    
                            function updateCoords(c) {
                                $('#x') . val(c . x);
                                $('#y') . val(c . y);
                                $('#w') . val(c . w);
                                $('#h') . val(c . h);
                            }
                    
                            function checkCoords()
                            {
                                if (parseInt($('#w') . val())) return true;
                                alert('Please select a crop region then press submit.');
                                return false;
                            }
                            
                    
                        </script>
                        <img id=\"thumbnail\" src=\"$new_name\">";
            }
            else
            {
                header('HTTP/1.0 404');
                exit(0);
            }
        }
        else
        {
            header('HTTP/1.0 405');
            exit(0);
        }
    }
    else
    {
        header('HTTP/1.0 406');
        exit(0);
    }

}
else if (isset($_FILES['dc_img']))
{
    $uploads_dir = '../../image_articles/tmp';
    if (!file_exists($uploads_dir))
        mkdir($uploads_dir);
    $tmp = $_FILES['dc_img']['tmp_name'];
    $name = $_FILES['dc_img']['name'];

    $ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
    $ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg');

    $ExtensionPresumee = explode('.', $name);
    $ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
    if ($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png')
    {

        if ($_FILES['size'] < 3 * MB)
        {
            if (move_uploaded_file($tmp, "$uploads_dir/$name"))
            {

                $img = "$uploads_dir/$name";

                $TailleImageChoisie = getimagesize($img);
                $win_width = $_POST['dc_win_width'];
                $NouvelleLargeur = (int)$win_width / 4;

                $Reduction = ( ($NouvelleLargeur * 100)/$TailleImageChoisie[0] );

                $NouvelleHauteur = ( ($TailleImageChoisie[1] * $Reduction)/100 );

                if($ExtensionPresumee == "jpg" || $ExtensionPresumee == "jpeg")
                {
                    $imgChoose = imagecreatefromjpeg($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagejpeg($NouvelleImage , $new_name, 100);
//                    $new_img = base64_encode(file_get_contents($new_name));
                }
                else if ($ExtensionPresumee == "png")
                {
                    $imgChoose = imagecreatefrompng($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagepng($NouvelleImage , $new_name);
                }
                else if ($ExtensionPresumee == "gif")
                {
                    $imgChoose = imagecreatefromgif($img);
                    $NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
                    imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
                    imagedestroy($imgChoose);
                    $NomImageChoisie = explode('.', $name);
                    $NomImageExploitable = time();
                    $new_name = $uploads_dir.$NomImageExploitable.'.'.$ExtensionPresumee;
                    imagegif($NouvelleImage , $new_name);
                }
                else
                {
                    header('HTTP/1.0 403');
                    exit(0);
                }

                echo "<script type=\"text/javascript\">
    
                            $(function () {
                    
                                $('#thumbnail2') . Jcrop({
                                    aspectRatio: 1,
                                    minSize: [20, 20],
                                    maxSize: [600, 600],
                                    setSelect: [0, 0, 50, 50],
                                    onSelect: updateCoords1
                                });
                    
                            });
                    
                            function updateCoords1(c) {
                                $('#x1') . val(c . x);
                                $('#y1') . val(c . y);
                                $('#w1') . val(c . w);
                                $('#h1') . val(c . h);
                            }
                    
                            function checkCoords1()
                            {
                                if (parseInt($('#w1') . val())) return true;
                                alert('Please select a crop region then press submit.');
                                return false;
                            }
    
                    </script>
                    <img id=\"thumbnail2\" src=\"$new_name\">";
            }
            else
            {
                header('HTTP/1.0 404');
                exit(0);
            }
        }
        else
        {
            header('HTTP/1.0 404');
            exit(0);
        }
    }
    else
    {
        header('HTTP/1.0 404');
        exit(0);
    }
}
else
{
    header('HTTP/1.0 407');
    exit(0);
}