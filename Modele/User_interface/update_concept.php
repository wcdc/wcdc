<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/16/16
 * Time: 1:19 PM
 */

include_once ("../../install.php");
include_once ("../../Class/Article.php");
include_once ("../../Class/User.php");
include_once ("../../Class/Slide_top.php");

//print_r($_POST);

function whoami()
{
    if ($_SESSION['logged_on_user'] != "")
        return  $_SESSION['logged_on_user'];
    else
        return False;
}

if (isset($_POST['id'], $_POST['wc_content'], $_POST['dc_content'], $_POST['category'], $_POST['wc_city'], $_POST['dc_city'], $_POST['wc_name'], $_POST['dc_name']))
{

    $article = new Article($_POST['id']);

    if ($_POST['g-recaptcha-response'] == "")
    {
        header('Location:../../Controleur/User_interface/edit_your_concept.php?id=' . $article->get_id() . '&e=2');
        exit(0);
    }

    if (file_exists($_POST['wc_image_crop'])){
        $wc_img = base64_encode(file_get_contents($_POST['wc_image_crop']));
        $t = preg_split('/\./', $_POST['wc_image_crop']);
        $wc_type = $t[5];
        //echo "<br/><br/>" . $wc_img . "<br/><br/>";
    }
    else
    {
	    $wc_img = $article->get_path_wc_img();
    }
    
    if (file_exists($_POST['dc_image_crop'])) 
    {
        $dc_img = base64_encode(file_get_contents($_POST['dc_image_crop']));
        $t = preg_split('/\./', $_POST['dc_image_crop']);
        $dc_type = $t[5];
        //echo $dc_img;
    }
    else
    {
	    $dc_img = $article->get_path_dc_img();
    }

    $wc_content = filter_input(INPUT_POST, 'wc_content', FILTER_SANITIZE_STRING);
    $dc_content = filter_input(INPUT_POST, 'dc_content', FILTER_SANITIZE_STRING);

	if (isset($_POST['wc_address']) && $_POST['dc_address'])
    {
        $WC_address = filter_input(INPUT_POST, 'wc_address', FILTER_SANITIZE_STRING);
        $DC_address = filter_input(INPUT_POST, 'dc_address', FILTER_SANITIZE_STRING);
    }
    else
    {
        $WC_address = "";
        $DC_address = "";
    }

    if(isset($_POST['wc_link']))
    {
        $wc_link = $_POST['wc_link'];
    }
    else
    {
        $wc_link ="";
    }

    if (isset($_POST['dc_link']))
    {
        $dc_link = $_POST['dc_link'];
    }
    else
    {
        $dc_link ="";
    }

    $categorie = $_POST['category'];

    if (isset($_POST['category2']))
    {
	    $categorie2 = $_POST['category2'];
    }
    else
    {
	    $categorie2 = "";
    }

    if (isset($_POST['category3']))
    {
	    $categorie3 = $_POST['category3'];
    }
    else
    {
        $categorie3 = "";
    }

    $wc_name = filter_input(INPUT_POST, 'wc_name', FILTER_SANITIZE_STRING);
    $dc_name = filter_input(INPUT_POST, 'dc_name', FILTER_SANITIZE_STRING);
    $title = $wc_name . " / " . $dc_name;
    $wc_city = $_POST['wc_city'];
    $dc_city = $_POST['dc_city'];

    if ($wc_city == "Global")
    {
        if ($dc_city !== "Global")
        {
            header('Location:../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php?e=4');
            exit(0);
        }
    }
    else if ($dc_city == "Global")
    {
        if ($wc_city !== "Global")
        {
            header('Location:../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php?e=4');
            exit(0);
        }
    }

    date_default_timezone_set("EUROPE/PARIS");
    $last_modified = date("Y-m-d H:i:s");
    $id = $_POST['id'];

    $art = new Article($id);

    $art->set_authorized_in_bdd(0);
    $slide = new Slide_top();
    $slide->delete_article_in_bdd($id);


    $req = $bdd->prepare('UPDATE  `article` SET `title`= :title, `working_img`= :wc_img, `dying_img`= :dc_img,
                                                `wc_name`= :wc_name, `dc_name`= :dc_name, `wc_content`= :wc_content,
                                                `dc_content`= :dc_content, `wc_address`= :wc_address,
                                                `dc_address`= :dc_address, `wc_link`= :wc_link, `dc_link`= :dc_link,
                                                `wc_city`= :wc_city, `dc_city`= :dc_city, `categorie`= :categorie,
                                                `categorie2`= :categorie2,`categorie3`= :categorie3,
                                                `last_modified`= :last_modified
                                                WHERE `id`= :id');

    $req->bindParam(":title", $title, PDO::PARAM_STR, strlen($title));
    $req->bindParam(":wc_img", $wc_img, PDO::PARAM_LOB);
    $req->bindParam(":dc_img", $dc_img, PDO::PARAM_LOB);
    $req->bindParam(":wc_name", $wc_name, PDO::PARAM_STR, strlen($wc_name));
    $req->bindParam(":dc_name", $dc_name, PDO::PARAM_STR, strlen($dc_name));
    $req->bindParam(":wc_content", $wc_content, PDO::PARAM_STR, strlen($wc_content));
    $req->bindParam(":dc_content", $dc_content, PDO::PARAM_STR, strlen($dc_content));
    $req->bindParam(":wc_address", $WC_address, PDO::PARAM_STR, strlen($WC_address));
    $req->bindParam(":dc_address", $DC_address, PDO::PARAM_STR, strlen($DC_address));
	$req->bindParam(":wc_link", $wc_link, PDO::PARAM_STR, strlen($wc_link));
	$req->bindParam(":dc_link", $dc_link, PDO::PARAM_STR, strlen($dc_link));
    $req->bindParam(":wc_city", $wc_city, PDO::PARAM_STR, strlen($wc_city));
    $req->bindParam(":dc_city", $dc_city, PDO::PARAM_STR, strlen($dc_city));
    $req->bindParam(":categorie", $categorie, PDO::PARAM_STR, strlen($categorie));
    $req->bindParam(":categorie2", $categorie2, PDO::PARAM_STR, strlen($categorie2));
    $req->bindParam(":categorie3", $categorie3, PDO::PARAM_STR, strlen($categorie3));
    $req->bindParam(":last_modified", $last_modified, PDO::PARAM_STR, strlen($last_modified));
    $req->bindParam(":id", $id, PDO::PARAM_INT);
    $req->execute();

    header('Location:../../Controleur/Accueil/accueil.php?m=1');
    exit(0);
}
else
{
	header('Location:../../Controleur/User_interface/edit_your_concept');
	exit(0);
}