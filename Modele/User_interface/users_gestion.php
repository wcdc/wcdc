<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/14/16
 * Time: 5:07 PM
 */

session_start();

if ($_GET['pseudo'] && $_GET['val'])
{
    $adm = new User($_SESSION['logged_on_user']);
    $user = new User($_GET['pseudo']);
    if ($adm->get_right_access() >= 3)
    {
        if ($_GET['val'] == 1 && $user->get_authorized() != 1)
        {
            $user->set_authorized_in_bdd(1);
            header('Location:../../Controleur/User_interface/users_gestion.php');
            exit(0);
        }
        else if ($_GET['val'] == -1 && $user->get_authorized() != -1)
        {
            $user->set_authorized_in_bdd(-1);
            header('Location:../../Controleur/User_interface/users_gestion.php');
            exit(0);
        }
    }
    else
    {
        header('Location:../../Controleur/User_interface/users_gestion.php?e=1');
        exit(0);
    }
}