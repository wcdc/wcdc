<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/15/16
 * Time: 6:08 PM
 */


if (isset($_POST['x'], $_POST['y'], $_POST['w'], $_POST['h'], $_POST['wc_image']))
{
    $name = $_POST['wc_image'];
    $t = preg_split('/\./', $name);
    $t[5] = strtolower($t[5]);
    if (file_exists("$name"))
    {
        if ($t[5] == "png")
            $source = imagecreatefrompng("$name");
        else if ($t[5] == "jpg" || $t[5] == "jpeg")
            $source = imagecreatefromjpeg("$name");
        else if ($t[5] == "gif")
            $source = imagecreatefromgif("$name");
        else
            $error = "wrong pictures format";

        $w = $_POST['w'];
        $h = $_POST['h'];
        $x = $_POST['x'];
        $y = $_POST['y'];

        list($width, $height) = getimagesize("$name");
        $arr = array('x' => $x, 'y' => $y, 'width' => $w, 'height' => $h);
        if (isset($source))
        {
            $dst = imagecrop($source, $arr);
            $time = time();
            $tab = preg_split('/\//', $t[4]);
            $names = $tab[3] . '_' . $time . '.' . $t[5];
            if ($t[5] == "png")
            {
                imagepng($dst, "../../image_articles/$names", null);
                $img_crop = "../../image_articles/$names";
            }
            else if ($t[5] == "jpg" || $t[5] == "jpeg")
            {
                imagejpeg($dst, "../../image_articles/$names", 90);
                $img_crop = "../../image_articles/$names";
            }
            else if ($t[5] == "gif")
            {
                imagegif($dst, "../../image_articles/$names", null);
                $img_crop = "../../image_articles/$names";
            }
            if ($img_crop)
            {
                unlink($name);
                echo "<img id=\"thumbnail\" src=\"$img_crop\">";
            }
        }
    }
}
else if (isset($_POST['x1'], $_POST['y1'], $_POST['w1'], $_POST['h1'], $_POST['dc_image']))
{
    $name = $_POST['dc_image'];
    $t = preg_split('/\./', $name);
    if (file_exists("$name"))
    {
        if ($t[5] == "png")
            $source = imagecreatefrompng("$name");
        else if ($t[5] == "jpg" || $t[5] == "jpeg")
            $source = imagecreatefromjpeg("$name");
        else if ($t[5] == "gif")
            $source = imagecreatefromgif("$name");
        else
            $error = "wrong pictures format";

        $w = $_POST['w1'];
        $h = $_POST['h1'];
        $x = $_POST['x1'];
        $y = $_POST['y1'];
        list($width, $height) = getimagesize("$name");
        $arr = array('x' => $x, 'y' => $y, 'width' => $w, 'height' => $h);
        if (isset($source))
        {
            $dst = imagecrop($source, $arr);
            $time = time();
            $tab = preg_split('/\//', $t[4]);
            $names = $tab[3] . '_' . $time . '.' . $t[5];
            if ($t[5] == "png")
            {
                imagepng($dst, "../../image_articles/$names", null);
                $img_crop2 = "../../image_articles/$names";
            }
            else if ($t[5] == "jpg" || $t[5] == "jpeg")
            {
                imagejpeg($dst, "../../image_articles/$names", 90);
                $img_crop2 = "../../image_articles/$names";
            }
            else if ($t[5] == "gif")
            {
                imagegif($dst, "../../image_articles/$names", null);
                $img_crop2 = "../../image_articles/$names";
            }
            if ($img_crop2)
            {
                unlink($name);
                echo "<img id=\"thumbnail2\" src=\"$img_crop2\">";
            }
        }
    }
}