<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/13/16
 * Time: 10:57 AM
 */

include_once ("../../install.php");
require_once ("../../Class/Article.php");
require_once ("../../Class/User.php");

$req = $bdd->prepare('SELECT `id` FROM article WHERE `authorized`<= 0 ORDER BY `date_published` ASC');
$req->execute();
$articles = $req->fetchAll();

$i = 0;
$tab = array();
while ($articles[$i]['id'])
{
    $tmp = new Article($articles[$i++]['id']);
    array_push($tab, $tmp);
}
$tab_articles = $tab;

if (isset($_GET['id'], $_GET['val']))
{
    $article = new Article($_GET['id']);
    if ($_GET['val'] == 1) 
    {
        $article->set_authorized_in_bdd(1);
        $user = new User($article->get_pseudo_author());
        $user->send_validate_concept_mail($user);
    }
    else if ($_GET['val'] == -1) 
    {
        $article->set_authorized_in_bdd(-1);
        $user = new User($article->get_pseudo_author());
        $user->send_ban_concept_mail($user);
    }
    header('Location:../../Controleur/User_interface/articles_gestion.php');
    exit(0);
}