<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 7/6/16
 * Time: 3:49 PM
 */

session_start();

include_once("../../Class/Article.php");
include_once ("../../Class/User.php");

if (isset($_GET['id']))
{
    $article = new Article($_GET['id']);
    if (isset($_SESSION['logged_on_user']))
        $user = new User($_SESSION['logged_on_user']);
    if(isset($_GET['backup']))
    {
        if ($user->get_right_access() >= 3)
        {
            $article->set_authorized_in_bdd(-3);
            header('Location:../../Controleur/User_interface/info.php');
            exit(0);
        }
    }
    else if ($user->get_id() == $article->get_id_users_of_author() || $user->get_right_access() >= 3)
    {
        $article->set_authorized_in_bdd(-2);
        header('Location:../../Controleur/User_interface/edit.php');
        exit(0);
    }
}