<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/11/16
 * Time: 12:18 PM
 */

if (isset($_POST['search']))
{
    $search = "%" . filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);
    $search .= "%";
    $req = $bdd->prepare('SELECT `pseudo` FROM users WHERE `pseudo` LIKE :search ORDER BY `pseudo` ASC');
    $req->bindParam(":search", strtolower($search), PDO::PARAM_STR, strlen($search));
    $req->execute();
    $user = $req->fetchAll();
    $req->closeCursor();
    
    $i = 0;
    $tab = array();
    while ($user[$i])
    {
        $tmp = new User($user[$i++]['pseudo']);
        if ($tmp->get_authorized())
            array_push($tab, $tmp);
    }
    $users = $tab;

    $req = $bdd->prepare('SELECT `id` FROM article 
                          WHERE `wc_name` LIKE :search 
                          OR `dc_name` LIKE :search 
                          OR `date_published` LIKE :search 
                          OR `wc_content` LIKE :search 
                          OR `dc_content` LIKE :search 
                          OR `wc_city` LIKE :search 
                          OR `wc_city` LIKE "Global" 
                          OR `dc_city` LIKE :search 
                          OR `dc_city` Like "Global" 
                          ORDER BY `date_published` DESC');

    $req->bindParam(":search", $search, PDO::PARAM_STR, strlen($search));
    $req->execute();
    $art = $req->fetchAll();
    $req->closeCursor();

    $i = 0;
    $tab = array();
    while ($art[$i])
    {
        $tmp = new Article($art[$i++]['id']);
        if ($tmp->get_authorized() == 1)
            array_push($tab, $tmp);
    }
    $articles = $tab;
    
    $nb_res = 0;
    
    if ($articles)
        $nb_res += count($articles);
    if ($users)
        $nb_res += count($users);
    
}