<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/10/16
 * Time: 2:51 PM
 */

include_once ("../../install.php");
if (isset($_POST['cat']))
{
    $i = 0;
    while ($_POST['cat'][$i])
    {
        if ($i == 0)
            $query1 = "`categorie`= '" . $_POST['cat'][$i] . "' OR `categorie2`= '" . $_POST['cat'][$i] . "' OR `categorie3`= '" . $_POST['cat'][$i++] . "'";
        else
            $query1 .= " OR `categorie`= '" . $_POST['cat'][$i] . "' OR `categorie2`= '" . $_POST['cat'][$i] . "' OR `categorie3`= '" . $_POST['cat'][$i++] . "'";
    }
}
if (isset($_POST['villes']))
{
    $i = 0;
    array_push($_POST['villes'], "Global");
    while ($_POST['villes'][$i])
    {
        if ($i == 0)
            $query2 = "`wc_city`= '" . $_POST['villes'][$i] . "' OR `dc_city`= '" . $_POST['villes'][$i++] . "'";
        else
            $query2 .= " OR `wc_city`= '" . $_POST['villes'][$i] . "' OR `dc_city`= '" . $_POST['villes'][$i++] . "'";
    }
}
if (isset($query1, $query2))
    $query = $query1 . " OR " . $query2;
else if(!$query1 && $query2)
    $query = $query2;
else
    $query = $query1;

if (isset($query) && $query != "")
{
    $req = $bdd->prepare('SELECT `id` FROM `article` WHERE ' . $query . 'ORDER BY `date_published` DESC');
    $req->execute();
    $select = $req->fetchAll();
}

$i = 0;
$tab = array();
while ($select[$i]['id'])
{
    $tmp = new Article($select[$i++]['id']);
    if ($tmp->get_authorized() == 1)
        array_push($tab, $tmp);
}
$selection = $tab;
