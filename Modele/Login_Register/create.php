<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/17/16
 * Time: 6:11 PM
 */

include_once ("../../install.php");

if ($_POST['g-recaptcha-response'] == "")
    header('Location:../../Controleur/Login_Register/login.php?e=8');

if (isset($_POST['name'],$_POST['firstname'],$_POST['date'],$_POST['sexe'],$_POST['ville'],$_POST['pseudo'],$_POST['mail'], $_POST['i'])) {

    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $first_name = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);

    if (strlen($_POST['date']) <= 10) {
        $dat = date_parse($_POST['date']);
        $dates = checkdate($dat['month'], $dat['day'], $dat['year']);
        if ($dates)
            $date = $dat['year'] . "/" . $dat['month'] . "/" . $dat['day'];
    }
    else
    {
        header('Location:../../Controleur/Login_Register/login.php?e=4');
        exit(0);
    }

    $sexe = $_POST['sexe'];

    $ville = filter_input(INPUT_POST, 'ville',FILTER_SANITIZE_STRING);

    if ($pseudo = filter_input(INPUT_POST, 'pseudo', FILTER_SANITIZE_STRING))
    {
        $pseudo = strtolower($pseudo);
        $prep = $bdd->prepare('SELECT `pseudo` FROM users WHERE `pseudo`= :pseudo');
        $prep->bindParam(":pseudo", $pseudo, PDO::PARAM_STR, strlen($pseudo));
        $prep->execute();

        if ($prep->rowCount() > 0)
        {
            $prep->closeCursor();
            header('Location:../../Controleur/Login_Register/login.php?e=1');
            exit(0);
        }
        $prep->closeCursor();
    }



    if (($mail = filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL)) && ($mail = filter_var($mail, FILTER_VALIDATE_EMAIL)))
    {
        $mail = (String)$mail;
        $prep = $bdd->prepare('SELECT `mail` FROM users WHERE `mail`= :mail');
        $prep->bindParam(":mail", $mail, PDO::PARAM_STR, strlen($mail));
        $prep->execute();

        if ($prep->rowCount() > 0)
        {
            $prep->closeCursor();
            header('Location:../../Controleur/Login_Register/login.php?e=2');
            exit(0);
        }
        $prep->closeCursor();
    }
    else
    {
        header('Location:../../Controleur/Login_Register/login.php?e=3');
        exit(0);
    }

    if (isset($date, $name, $first_name, $pseudo, $_POST['i'], $mail, $ville, $sexe))
    {
        $passwd = hash("whirlpool", $_POST['i']);


        $token = hash("whirlpool", $pseudo);
        $token .= hash("whirlpool", $passwd);
        $credibility = 0;

        $profil_photo = base64_encode(file_get_contents("../../img/user.png"));

        $prep = $bdd->prepare('INSERT INTO `users`(`nom`, `prenom`, `pseudo`, `password`, `mail`, `ville`, `date_de_naissance`, `sexe`, `token_conf`, `profil_photo`)
                                VALUES (:nom,:prenom,:pseudo,:password,:mail,:ville,:date_de_naissance,:sexe, :token, :profil_photo);');
        $prep->bindParam(":nom", $name, PDO::PARAM_STR, strlen($name));
        $prep->bindParam(":prenom", $first_name, PDO::PARAM_STR, strlen($first_name));
        $prep->bindParam(":pseudo", $pseudo, PDO::PARAM_STR, strlen($pseudo));
        $prep->bindParam(":password", $passwd, PDO::PARAM_STR, strlen($passwd));
        $prep->bindParam(":mail", $mail, PDO::PARAM_STR, strlen($mail));
        $prep->bindParam(":ville", $ville, PDO::PARAM_STR, strlen($ville));
        $prep->bindParam(":date_de_naissance", $date, PDO::PARAM_STR, strlen($date));
        $prep->bindParam(":sexe", $sexe, PDO::PARAM_BOOL);
        $prep->bindParam(":token", $token, PDO::PARAM_STR, strlen($token));
        $prep->bindParam(":profil_photo", $profil_photo, PDO::PARAM_LOB);
        $prep->execute();


        $to = $mail;
        $objet = "Confirmation d'inscription";

        // message
        $message = '
         <html>
          <head>
           <title>Hello ' . $prenom . ' ' . $nom . '</title>
          </head>
          <body>
          <p>Please use the following link to confirm your subscription to workingconceptdyingconcept.com</p>
          <!-- http://wcdc.mooo.com/Controleur/Accueil/accueil.php?t=' . $token . ' -->
          <p style="word-wrap: break-word;">http://workingconceptdyingconcept.com/WCDC/Controleur/Accueil/accueil.php?t=' . $token . '</p>  
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: ' . $mail . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($mail, $objet, $message, $headers);

        header('Location:../../Controleur/Accueil/accueil.php?m=2');
        exit(0);
    }
    else {
        header('Location:../../Controleur/Accueil/accueil.php?e=7');
        exit(0);
    }
}

