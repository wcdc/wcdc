<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/11/16
 * Time: 4:19 PM
 */

if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] != "")
{
    if (isset($_POST['Nom'], $_POST['Prenom'], $_POST['Mail'], $_POST['Message']))
    {
        $nom = filter_input(INPUT_POST, 'Nom', FILTER_SANITIZE_STRING);
        $prenom = filter_input(INPUT_POST, 'Prenom', FILTER_SANITIZE_STRING);
        $mail = filter_input(INPUT_POST, 'Mail', FILTER_SANITIZE_STRING);
        $mail = filter_var($mail, FILTER_VALIDATE_EMAIL);
        $mess = filter_input(INPUT_POST, 'Message', FILTER_SANITIZE_STRING);
        if (isset($_POST['Telephone']))
            $telephone = filter_input(INPUT_POST, 'Telephone', FILTER_SANITIZE_NUMBER_INT);

        if ($nom && $prenom && $mail)
        {
            $TO = "aldo@tatchme.com, moinat.quentin57@hotmail.fr, avertalexis@free.fr";
            //$TO = "averalexis@free.fr";

            $objet = "Renseignements";

            // message
            if ($telephone) {
                $message = '
              <html>
              <body>
              <h2>Bonjour,</h2><br/>
              <p>' . $mess . '</p><br/>
              </body>
              <footer>
               Coridalement ' . $prenom . ' ' . $nom . '<br/>
               Tel : ' . $telephone . '
              </footer>
              </html>
             ';
            }
            else{
                $message = '
              <html>
              <body>
              <h2>Bonjour,</h2><br/>
              <p>' . $mess . '</p><br/>
              </body>
              <footer>
               Coridalement ' . $prenom . ' ' . $nom . '<br/>
              </footer>
              </html>
              ';
            }

            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'To: ' . $TO . "\r\n";
            $headers .= 'From:' . $mail . "\r\n";

            if (mail($mail, $objet, $message, $headers))
            {
                header('Location:../../Controleur/Accueil/accueil.php?m=3');
                exit(0);
            }
            else
            {
                $error = "Une erreur c'est produite votre mail ne c'est pas envoyé veuillez réessayer.";
                header('Location:../../Controleur/Contact/contact.php?e=1');
                exit(0);
            }
        }
        else
        {
            $error = "Une erreur c'est produite votre mail ne c'est pas envoyé veuillez réessayer.";
            header('Location:../../Controleur/Contact/contact.php?e=1');
            exit(0);
        }

    }

}

else
{
    header('Location:../../Controleur/Contact/contact.php?e=1');
    exit(0);
}