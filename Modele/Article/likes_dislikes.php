<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/14/16
 * Time: 6:53 PM
 */

session_start();

//print_r($_POST);

include_once ("../../Class/Article.php");
include_once ("../../Class/User.php");


function likes ($id_article)
{
    if(isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'] != "")
        $user = new User($_SESSION['logged_on_user']);
    else
    {
        header('HTTP/1.0 404');
        exit(0);
    }
    $article = new Article($id_article);
    if (in_array($id_article, $user->get_user_dislikes()))
    {
        $user->del_dislikes_article_in_bdd($id_article);
        $user->add_likes_article_in_bdd($id_article);
        $article->del_dislikes_in_bdd();
        $article->add_likes_in_bdd();
    }
    else
    {
        $user->add_likes_article_in_bdd($id_article);
        $article->add_likes_in_bdd();
    }

    $author = new User($article->get_pseudo_author());
    $author_articles = $author->get_user_articles();

    foreach ($author_articles as $art)
    {
        if ($art->get_authorized() == 1 && $art->get_likes() > 0 || $art->get_dislikes() > 0)
            $resultats += ($art->get_likes() / ($art->get_dislikes() + $art->get_likes()));
    }

    $resultat = $resultats / count($author_articles);

    $resultat = $resultat * (0.025 * count($author_articles));

    $credibility_author = $resultat * 100;

    $author->set_credibility_in_bdd($credibility_author);
    
    return $article->get_likes() + 1;
}

function dislikes ($id_article)
{
    if(isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'] != "")
        $user = new User($_SESSION['logged_on_user']);
    else
    {
        header('HTTP/1.0 404');
        exit(0);
    }
    $article = new Article($id_article);
    if (in_array($id_article, $user->get_user_likes()))
    {
        $user->del_likes_article_in_bdd($id_article);
        $user->add_dislikes_article_in_bdd($id_article);
        $article->del_likes_in_bdd();
        $article->add_dislikes_in_bdd();
    }
    else
    {
        $user->add_dislikes_article_in_bdd($id_article);
        $article->add_dislikes_in_bdd();
    }

    $author = new User($article->get_pseudo_author());
    $author_articles = $author->get_user_articles();

    foreach ($author_articles as $art)
    {
        if ($art->get_authorized() == 1 && $art->get_likes() > 0 || $art->get_dislikes() > 0)
            $resultats += ($art->get_likes() / ($art->get_dislikes() + $art->get_likes()));
    }

    $resultat = $resultats / count($author_articles);

    $resultat = $resultat * (0.025 * count($author_articles));

    $credibility_author = $resultat * 100;

    $author->set_credibility_in_bdd($credibility_author);

    
    return $article->get_dislikes() + 1;
}




if(isset($_SESSION['logged_on_user']))
    $user = new User($_SESSION['logged_on_user']);
else
{
   // header("Location:../../Controleur/Login_Register/login.php");
    header('HTTP/1.0 404');
    $art = new Article($_POST['id']);
    if ($_POST['likes'] == 1 || $_POST['likes'] == -1)
        echo $art->get_likes();
    else if ($_POST['dislikes'] == 1 || $_POST['dislikes'] == -1)
        echo $art->get_dislikes();
    exit(0);
}

//print_r($user->get_user_likes());

if (isset($_POST['likes']) && $_POST['likes'] == 1 && (in_array($_POST['id'], $user->get_user_likes())) == FALSE)
{
    $likes = likes($_POST['id']);
    echo $likes;
}
else if (in_array($_POST['id'], $user->get_user_likes()) && $_POST['likes'] == 1)
{
    $art = new Article($_POST['id']);
    $art->del_likes_in_bdd();
    $art->del_likes();
    $user = new User($_SESSION['logged_on_user']);
    $user->del_likes_article_in_bdd($_POST['id']);
    $user->del_user_likes($_POST['id']);
    echo $art->get_likes();
}

if (isset($_POST['dislikes']) && $_POST['dislikes'] == 1 && (in_array($_POST['id'], $user->get_user_dislikes())) == FALSE)
{
    $dislikes = dislikes($_POST['id']);
    echo $dislikes;
}
else if (in_array($_POST['id'], $user->get_user_dislikes()) && $_POST['dislikes'] == 1)
{
    $art = new Article($_POST['id']);
    $art->del_dislikes_in_bdd();
    $art->del_dislikes();
    $user = new User($_SESSION['logged_on_user']);
    $user->del_dislikes_article_in_bdd($_POST['id']);
    $user->del_user_dislikes($_POST['id']);
    echo $art->get_dislikes();
}

if ($_POST['likes'] == -1)
{
    $art = new Article($_POST['id']);
    echo $art->get_likes();
}
else if ($_POST['dislikes'] == -1)
{
    $art = new Article($_POST['id']);
    echo $art->get_dislikes();
}