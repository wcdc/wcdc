<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 5/16/16
 * Time: 7:51 PM
 */

session_start();

//print_r($_POST);
//echo "<br/>";
//print_r($_FILES);
//echo "<br/>";
//print_r($_SESSION);

include_once ("../../install.php");
include_once ("../../Class/Article.php");
include_once ("../../Class/User.php");
include_once ("../../Class/Brand.php");

function whoami()
{
    if ($_SESSION['logged_on_user'] != "")
        return  $_SESSION['logged_on_user'];
    else
        return False;
}


if (isset($_POST['wc_content'], $_POST['dc_content'], $_POST['category'], $_POST['wc_city'], $_POST['dc_city'], $_POST['wc_name'], $_POST['dc_name']) && $_POST['wc_image_crop'] && $_POST['dc_image_crop'])
{
    if ($_POST['g-recaptcha-response'] == "")
    {
        header('Location:../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php?e=2');
        exit(0);
    }
    
    if (file_exists($_POST['wc_image_crop']))
    {
        $wc_img = base64_encode(file_get_contents($_POST['wc_image_crop']));
        unlink($_POST['wc_image_crop']);
    }
    if (file_exists($_POST['dc_image_crop']))
    {
        $dc_img = base64_encode(file_get_contents($_POST['dc_image_crop']));
        unlink($_POST['dc_image_crop']);
    }

    $pseudo = whoami();

    $req = $bdd->prepare('SELECT `id` FROM users WHERE `pseudo`= :pseudo ');
    $req->bindParam(":pseudo", $pseudo, PDO::PARAM_STR, strlen($pseudo));
    $req->execute();
    if ($req->rowCount() > 0)
    {
        $user = $req->fetch();
        $id_users_of_author = $user['id'];


        $wc_content = filter_input(INPUT_POST, 'wc_content', FILTER_SANITIZE_STRING);
        $dc_content = filter_input(INPUT_POST, 'dc_content', FILTER_SANITIZE_STRING);
        if (isset($_POST['wc_address']) && $_POST['dc_address'])
        {
            $WC_address = filter_input(INPUT_POST, 'wc_address', FILTER_SANITIZE_STRING);
            $DC_address = filter_input(INPUT_POST, 'dc_address', FILTER_SANITIZE_STRING);
        }
        else
        {
            $WC_address = "";
            $DC_address = "";
        }

	    if(isset($_POST['wc_link']))
	    {
		    $wc_link = $_POST['wc_link'];
	    }
	    else
	    {
		    $wc_link ="";
	    }

	    if (isset($_POST['dc_link']))
	    {
		    $dc_link = $_POST['dc_link'];
	    }
	    else
	    {
		    $dc_link ="";
	    }

        $categorie = $_POST['category'];

        if (isset($_POST['category2']))
            $categorie2 = $_POST['category2'];
        else
            $categorie2 = "";

        if (isset($_POST['category3']))
            $categorie3 = $_POST['category3'];
        else
            $categorie3 = "";
        
        $wc_name = htmlspecialchars(filter_input(INPUT_POST, 'wc_name', FILTER_SANITIZE_STRING));
        $dc_name = htmlspecialchars(filter_input(INPUT_POST, 'dc_name', FILTER_SANITIZE_STRING));
        $wc_name = filter_input(INPUT_POST, 'wc_name', FILTER_SANITIZE_STRING);
        $dc_name = filter_input(INPUT_POST, 'dc_name', FILTER_SANITIZE_STRING);
        $title = $wc_name . " / " . $dc_name;
        $wc_city = $_POST['wc_city'];
        $dc_city = $_POST['dc_city'];

        if ($wc_city == "Global")
        {
            if ($dc_city !== "Global")
            {
                header('Location:../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php?e=4');
                exit(0);
            }
        }
        else if ($dc_city == "Global")
        {
            if ($wc_city !== "Global")
            {
                header('Location:../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php?e=4');
                exit(0);
            }
        }

        date_default_timezone_set("EUROPE/PARIS");
        $date_published = date("Y-m-d H:i:s");


        $brand = $bdd->query('SELECT `name` FROM brand');
        $marques = $brand->fetchAll();

        $i = 0;

        while ($marques[0][$i])
        {
            $marque = strtolower($marques[0][$i]);
            if (preg_match('/'.$marque.'/', strtolower($wc_name)))
            {
                $brand = new Brand($marque);
                $brand->addPseudoToTabPseusoAssocInBdd($id_users_of_author, $pseudo);
            }
            $i++;
        }

        $req = $bdd->prepare('INSERT INTO article (`title`, `working_img`, `dying_img`, `wc_name`, `dc_name`, `wc_content`, `dc_content`, `wc_address`, `dc_address`, `wc_link`, `dc_link`, `wc_city`, `dc_city`, `categorie`, `categorie2`,  `categorie3`, `date_published`, `last_modified`, `id_users_of_author`, `pseudo_author`) 
                                          VALUES (:title, :wc_img, :dc_img, :wc_name, :dc_name, :wc_content, :dc_content, :wc_address, :dc_address, :wc_link, :dc_link, :wc_city, :dc_city, :categorie, :categorie2, :categorie3, :date_published, :last_modified, :id_users_of_author, :pseudo_author)');
        $req->bindParam(":title", $title, PDO::PARAM_STR, strlen($title));
        $req->bindParam(":wc_img", $wc_img, MYSQLI_TYPE_LONG_BLOB, strlen($wc_img));
        $req->bindParam(":dc_img", $dc_img, MYSQLI_TYPE_LONG_BLOB, strlen($dc_img));
        $req->bindParam(":wc_name", $wc_name, PDO::PARAM_STR, strlen($wc_name));
        $req->bindParam(":dc_name", $dc_name, PDO::PARAM_STR, strlen($dc_name));
        $req->bindParam(":wc_content", $wc_content, PDO::PARAM_STR, strlen($wc_content));
        $req->bindParam(":dc_content", $dc_content, PDO::PARAM_STR, strlen($dc_content));
        $req->bindParam(":wc_address", $WC_address, PDO::PARAM_STR, strlen($WC_address));
        $req->bindParam(":dc_address", $DC_address, PDO::PARAM_STR, strlen($DC_address));
	    $req->bindParam(":wc_link", $wc_link, PDO::PARAM_STR, strlen($wc_link));
	    $req->bindParam(":dc_link", $dc_link, PDO::PARAM_STR, strlen($dc_link));
        $req->bindParam(":wc_city", $wc_city, PDO::PARAM_STR, strlen($wc_city));
        $req->bindParam(":dc_city", $dc_city, PDO::PARAM_STR, strlen($dc_city));
        $req->bindParam(":categorie", $categorie, PDO::PARAM_STR, strlen($categorie));
        $req->bindParam(":categorie2", $categorie2, PDO::PARAM_STR, strlen($categorie2));
        $req->bindParam(":categorie3", $categorie3, PDO::PARAM_STR, strlen($categorie3));
        $req->bindParam(":date_published", $date_published, PDO::PARAM_STR, strlen($date_published));
        $req->bindParam(":last_modified", $date_published, PDO::PARAM_STR, strlen($date_published));
        $req->bindParam(":id_users_of_author", $id_users_of_author, PDO::PARAM_INT);
        $req->bindParam(":pseudo_author", $pseudo, PDO::PARAM_STR, strlen($pseudo));
        $req->execute();
        $req->closeCursor();
        
        $user = new User($pseudo);
        
        $user->send_confirmation_submit_concept_successful_mail($user);


        header('Location:../../Controleur/Accueil/accueil.php?m=1');
        exit(0);
    }
    else
    {
        header('Location:../../Controleur/Accueil/accueil.php?e=9');
        exit(0);
    }
}
else if (isset($_POST['wc_content']))
{
    header('Location:../../Controleur/Submit_Your_Concept/Submit_Your_Concept.php?e=3');
    exit(0);
}

$categories = $bdd->query('SELECT `name` FROM categories ORDER BY name ASC');
$categories2 = $bdd->query('SELECT `name` FROM categories ORDER BY name ASC');
$categories3 = $bdd->query('SELECT `name` FROM categories ORDER BY name ASC');
$villes = $bdd->query('SELECT `name` FROM villes ORDER BY name ASC');
$villes2 = $bdd->query('SELECT `name` FROM villes ORDER BY name ASC');

?>