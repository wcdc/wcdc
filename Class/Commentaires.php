<?php

/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/17/16
 * Time: 1:46 PM
 */

session_start();

require_once ("../../Class/User.php");

class Commentaires
{
    private $_id;
    private $_id_article;
    private $_id_author;
    private $_pseudo_author;
    private $_commentaire;
    private $_working_or_dying;
    private $_date_published;
    private $_last_modified;

    public function connect()
    {
        try
        {
	        $bdd = new PDO('mysql:host=ba677080-001.privatesql;dbname=workingcecwcdc;port=35319;charset=utf8', 'user', 'Userpassword1', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }
        return $bdd;
    }

    public function __construct($id)
    {
        $bdd = self::connect();
        $req =  $bdd->prepare('SELECT * FROM `commentaires` WHERE `id`= :id');
        $req->bindParam(":id", $id, PDO::PARAM_INT);
        $req->execute();
        $comment = $req->fetch();

        $this->_id = $comment['id'];
        $this->_id_article = $comment['id_article'];
        $this->_id_author = $comment['id_author'];
        $this->_pseudo_author = $comment['pseudo_author'];
        $this->_commentaire = $comment['commentaire'];
        $this->_working_or_dying = $comment['working_or_dying'];
        $this->_date_published = $comment['date_published'];
        $this->_last_modified = $comment['last_modified'];
    }

    public function __destruct()
    {
    }


    ##########################################################################
                                    //GETTERS
    ##########################################################################

    public function get_id() {
        return $this->_id;
    }

    public function get_id_article() {
        return $this->_id_article;
    }

    public function get_id_author() {
        return $this->_id_author;
    }

    public function get_pseudo_author() {
        return $this->_pseudo_author;
    }

    public function get_commentaire() {
        return $this->_commentaire;
    }

    public function get_working_or_dying() {
        return $this->_working_or_dying;
    }

    public function get_date_published() {
        return $this->_date_published;
    }

    public function get_last_modified() {
        return $this->_last_modified;
    }



    ##########################################################################
                                    //SETTERS
    ##########################################################################



    public function set_id_article_in_bdd($id_article) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE commentaires SET `id_article`= :id_article WHERE `id`= :id');
        $req->bindParam(":id_article", $id_article, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_id_author_in_bdd($id_author) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE commentaires SET `id_author`= :id_author WHERE `id`= :id');
        $req->bindParam(":id_author", $id_author, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_pseudo_author_in_bdd($pseudo_author) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE commentaires SET `pseudo_author`= :pseudo_author WHERE `id`= :id');
        $req->bindParam(":pseudo_author", $pseudo_author, PDO::PARAM_STR, strlen($pseudo_author));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_commentaire_in_bdd($commentaire) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE commentaires SET `commentaire`= :commentaire WHERE `id`= :id');
        $req->bindParam(":commentaire", $commentaire, PDO::PARAM_STR, strlen($commentaire));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_working_or_dying_in_bdd($working_or_dying) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE commentaires SET `working_or_dying`= :working_or_dying WHERE `id`= :id');
        $req->bindParam(":working_or_dying", $working_or_dying, PDO::PARAM_BOOL);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_date_published_in_bdd($date_published) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `date_published`= :date_published WHERE `id`= :id');
        $req->bindParam(":date_published", $date_published, PDO::PARAM_STR, strlen($date_published));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_last_modified_in_bdd($last_modified) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `last_modified`= :last_modified WHERE `id`= :id');
        $req->bindParam(":last_modified", $last_modified, PDO::PARAM_STR, strlen($last_modified));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }



    ##########################################################################
                                //AFFICHAGE
    ##########################################################################

    
    public function commentaire() {
        $author = new User($this->_pseudo_author);
        printf('
                        <div id="delete-%d"> 
                            <br/>
                            <div class="commentary_box">
                                <div class="rond3 author_tof">
                                    <img src="data:image/;base64,%s" style="height: 110px; width: 110px;">
                                </div>
                                <p id="edit-%d" class="commentary">%s</p>
                                <div style="margin-top: 20px;">
                                    <p class="author" style="margin-top: 10px;">%s - %s
                                        <a id="hover-link-edit-%d" class="hover-link-edit" href="../../Modele/User_interface/edit_comm.php?id=%d" style="padding-left: 20px;" hidden> Edit</a>
                                        <a id="hover-link-delete-%d" class="hover-link-delete" href="../../Modele/User_interface/delete_comm.php?id=%d" style="padding-left: 20px;" hidden>Delete</a>
                                    </p>
                                </div>
                            </div>
                            <br/>
                        </div>
                        ', $this->_id, $author->get_path_profil_photo(), $this->_id, $this->_commentaire, $this->_pseudo_author, $this->_last_modified, $this->_id, $this->_id, $this->_id, $this->_id);
    }




    public function __toString()
    {
        $str = sprintf("<br/><br/>
                                id:         %d<br/><br/>
                        id_article:         %d<br/><br/>
                        id_author:          %d<br/><br/>
                        commentaire:        %s<br/><br/>
                        date_published:     %s<br/><br/>
                        last_modified:      %s<br/><br/>",  $this->_id, $this->_id_article, $this->_id_author,
                                                            $this->_commentaire, $this->_date_published,
                                                            $this->_last_modified);
        return $str;
    }
}