<?php

/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 7/7/16
 * Time: 7:21 PM
 */

session_start();

require_once ("../../install.php");
require_once ("../../Class/User.php");
require_once ("../../Class/Article.php");

class Brand
{
    private $_id;
    private $_name;
    private $_tab_pseudo_assoc;


    public function connect()
    {
        try
        {
	        $bdd = new PDO('mysql:host=ba677080-001.privatesql;dbname=workingcecwcdc;port=35319;charset=utf8', 'user', 'Userpassword1', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }
        return $bdd;
    }

    public function __construct($brand)
    {
        $bdd = self::connect();

        $req = $bdd->prepare('SELECT * FROM brand WHERE `name`= :brand');
        $req->bindParam(":brand", $brand, PDO::PARAM_STR, strlen($brand));
        $req->execute();
        $brand = $req->fetchAll();
        $req->closeCursor();

        $this->_id = $brand[0]['id'];
        $this->_name = $brand[0]['name'];


        if ($brand[0]['tab_pseudo_assoc'] != null)
            $this->_tab_pseudo_assoc = json_decode($brand[0]['tab_pseudo_assoc'], true, 512);
    }

    public function __destruct()
    {
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getTabPseudoAssoc()
    {
        return $this->_tab_pseudo_assoc;
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function addPseudoToTabPseusoAssoc($id, $pseudo)
    {
        $user = array("id" => $id, "pseudo" => $pseudo);
        array_push($this->_tab_pseudo_assoc ,$user);
    }

    public function updateNameInBdd($newName)
    {
        $oldName = $this->_name;
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE brand SET `name`= :newName WHERE `name`= :oldName');
        $req->bindParam(':newName', $newName, PDO::PARAM_STR, strlen($newName));
        $req->bindParam(':oldName', $oldName, PDO::PARAM_STR, strlen($oldName));
        $req->execute();
    }

    public function addPseudoToTabPseusoAssocInBdd($id, $pseudo)
    {
        $user = array("id" => $id, "pseudo" => $pseudo);
        if (empty($this->_tab_pseudo_assoc) || !(in_array($user, $this->_tab_pseudo_assoc)))
        {
            if(empty($this->_tab_pseudo_assoc))
                $this->_tab_pseudo_assoc = array();
            $this->addPseudoToTabPseusoAssoc($id, $pseudo);
            $json = json_encode($this->_tab_pseudo_assoc);
            $bdd = self::connect();
            $req = $bdd->prepare('UPDATE brand SET `tab_pseudo_assoc`= :json WHERE `name`= :name');
            $req->bindParam(':json', $json, PDO::PARAM_STR, strlen($json));
            $req->bindParam(':name', $this->_name, PDO::PARAM_STR, strlen($this->_name));
            $req->execute();
        }
    }

    public function updatePseudoToTabPseusoAssocInBdd($id, $new_pseudo, $old_pseudo)
    {
        $user = array("id" => $id, "pseudo" => $old_pseudo);

        echo "<br/>";
        print_r($this->_tab_pseudo_assoc);
        echo "<br/>";
        echo "<br/>";
        if (in_array($user, $this->_tab_pseudo_assoc))
        {
            echo "<br/>";
            print_r($this->_tab_pseudo_assoc);
            echo "<br/>";
            echo "<br/>";
            $tab = array();
            foreach ($this->_tab_pseudo_assoc as $assoc)
            {
                if ($assoc['id'] == $id)
                {
                    $assoc['pseudo'] = $new_pseudo;
                }
                array_push($tab, $assoc);
            }
            echo "<br/>";

            $json = json_encode($tab);
            $bdd = self::connect();
            $req = $bdd->prepare('UPDATE brand SET `tab_pseudo_assoc`= :json WHERE `name`= :name');
            $req->bindParam(':json', $json, PDO::PARAM_STR, strlen($json));
            $req->bindParam(':name', $this->_name, PDO::PARAM_STR, strlen($this->_name));
            $req->execute();
        }
    }
}