<?php

/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/7/16
 * Time: 12:32 PM
 */

session_start();

require_once ("../../Class/Commentaires.php");

class Article
{
    private $_id;
    private $_title;
    private $_path_wc_img;
    private $_path_dc_img;
    private $_wc_name;
    private $_dc_name;
    private $_wc_content;
    private $_dc_content;
    private $_wc_address;
    private $_dc_address;
	private $_wc_link;
	private $_dc_link;
    private $_wc_city;
    private $_dc_city;
    private $_categorie;
    private $_categorie1;
    private $_categorie2;
    private $_categorie3;
    private $_date_published;
    private $_last_modified;
    private $_id_users_of_author;
    private $_pseudo_author;
    private $_id_authorized_mod;
    private $_authorized;
    private $_likes;
    private $_dislikes;
    private $_commentaires;
    private $_count;
    

    ##########################################################################
                                    //CONNECT
    ##########################################################################

    public function connect()
    {
        try
        {
	        $bdd = new PDO('mysql:host=ba677080-001.privatesql;dbname=workingcecwcdc;port=35319;charset=utf8', 'user', 'Userpassword1', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }
        return $bdd;
    }


    function follow_notif($suivi, $follower)
    {
        $mail= $follower->get_mail();
        $objet = "New article by " . $suivi->get_pseudo() . " !";

        // message
        $message = '
         <html>
          <body>
             <h1>Hello ' . $follower->get_prenom() . ' ' . $follower->get_nom() . '</h1>
             <br/>
            <p>' . $suivi->get_pseudo() . ' has published a new article on <a href="https://workingconceptdyingconcept.com/WCDC/Controleur/Article/article.php?id='. $this->_id . '">workingconceptdyingconcept.com</a></p>
          </body>
         </html>
         ';

        $headers  = 'Content-type: text/html; utf-8' . "\r\n";
        $headers .= 'To: ' . $mail . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($mail, $objet, $message, $headers);
    }


    ##########################################################################
                                    //CONSTRUCT
    ##########################################################################

    public function __construct($id)
    {
        $bdd = self::connect();

        $req = $bdd->prepare('SELECT * FROM article WHERE `id`= :id');
        $req->bindParam(":id", $id, PDO::PARAM_INT);
        $req->execute();
        $article = $req->fetch();
        $req->closeCursor();
        //print_r($article);

        $tab = preg_split("/ /", $article['date_published']);
        $str = str_replace('-', '/', $tab[0]);
        $tab = preg_split("/\//", $str);
        $date = $tab[1] . "/" . $tab[0];

        $tab = preg_split("/ /", $article['last_modified']);
        $str = str_replace('-', '/', $tab[0]);
        $tab = preg_split("/\//", $str);
        $last = $tab[1] . "/" . $tab[0];


        $this->_id = $article['id'];
        $this->_title = $article['title'];
        $this->_path_wc_img = $article['working_img'];
        $this->_path_dc_img = $article['dying_img'];
        $this->_wc_name = $article['wc_name'];
        $this->_dc_name = $article['dc_name'];
        $this->_wc_content = $article['wc_content'];
        $this->_dc_content = $article['dc_content'];
        $this->_wc_address = $article['wc_address'];
        $this->_dc_address = $article['dc_address'];
	    $this->_wc_link = $article['wc_link'];
	    $this->_dc_link = $article['dc_link'];
        $this->_wc_city = $article['wc_city'];
        $this->_dc_city = $article['dc_city'];
        $this->_categorie1 = $article['categorie'];
        $this->_categorie2 = $article['categorie2'];
        $this->_categorie3 = $article['categorie3'];

        if (isset($article['categorie2']))
        {
            if (strlen($article['categorie'] . " - " . $article['categorie2']) <= 18)
                $this->_categorie = $article['categorie'] . " - " . $article['categorie2'];
            else
            {
                $tmp = $article['categorie'] . " - " . $article['categorie2'];
                $this->_categorie = substr($tmp, 0, 15);
                $this->_categorie = $this->_categorie . "...";
            }
        }
        else
        {
            $this->_categorie = $article['categorie'];
        }

        if (strlen($this->_categorie > 18))
            $this->_categorie = $article['categorie'];

        $this->_date_published = $date;
        $this->_last_modified = $last;
        $this->_id_users_of_author = $article['id_users_of_author'];
        $this->_pseudo_author = $article['pseudo_author'];
        $this->_id_authorized_mod = $article['id_authorized_mod'];
        $this->_authorized = $article['authorized'];
        $this->_likes = $article['likes'];
        $this->_dislikes = $article['dislikes'];


        $req = $bdd->prepare('SELECT `id` FROM `commentaires` WHERE `id_article`= :id_article ORDER BY last_modified DESC');
        $req->bindParam(":id_article", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $comms = $req->fetchAll();
        $req->closeCursor();

        $tab = array();
        $i = 0;

        while ($comms[$i]['id'])
        {
            $tmp = new Commentaires($comms[$i++]['id']);
            array_push($tab, $tmp);
        }
        $this->_commentaires = $tab;
        $this->_count = count($this->_commentaires);
    }


    public function __destruct()
    {

    }




    ##########################################################################
                                    //GETTERS
    ##########################################################################

    public function get_id() {
        return $this->_id;
    }

    public function get_title() {
        return $this->_title;
    }

    public function get_path_wc_img() {
        return $this->_path_wc_img;
    }

    public function get_path_dc_img() {
        return $this->_path_dc_img;
    }

    public function get_wc_name() {
        return $this->_wc_name;
    }

    public function get_dc_name() {
        return $this->_dc_name;
    }

    public function get_wc_content() {
        return $this->_wc_content;
    }

    public function get_dc_content() {
        return $this->_dc_content;
    }

    public function get_wc_address() {
        return $this->_wc_address;
    }

    public function get_dc_address() {
        return $this->_dc_address;
    }

	public function get_wc_link()
	{
		return $this->_wc_link;
	}

	public function get_dc_link()
	{
		return $this->_dc_link;
	}

    public function get_wc_city() {
        return $this->_wc_city;
    }

    public function get_dc_city() {
        return $this->_dc_city;
    }

    public function get_categorie() {
        return $this->_categorie;
    }

    public function get_categorie1() {
        return $this->_categorie1;
    }

    public function get_categorie2() {
        return $this->_categorie2;
    }

    public function get_categorie3() {
        return $this->_categorie3;
    }

    public function get_date_published() {
        return $this->_date_published;
    }

    public function get_last_modified() {
        return $this->_last_modified;
    }

    public function get_id_users_of_author() {
        return $this->_id_users_of_author;
    }

    public function get_pseudo_author() {
        return $this->_pseudo_author;
    }

    public function get_id_authorized_mod() {
        return $this->_id_authorized_mod;
    }

    public function get_authorized() {
        return $this->_authorized;
    }

    public function get_likes() {
        return $this->_likes;
    }

    public function get_dislikes() {
        return $this->_dislikes;
    }

    public function get_commentaires() {
        return $this->_commentaires;
    }



    ##########################################################################
                                    //SETTERS
    ##########################################################################



    public function set_title_in_bdd($title) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `title`= :title WHERE `id`= :id');
        $req->bindParam(":title", $title, PDO::PARAM_STR, strlen($title));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_path_wc_img_in_bdd($path_wc_img) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `path_wc_img`= :path_wc_img WHERE `id`= :id');
        $req->bindParam(":path_wc_img", $path_wc_img, PDO::PARAM_STR, strlen($path_wc_img));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_path_dc_img_in_bdd($path_dc_img) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `path_dc_img`= :path_dc_img WHERE `id`= :id');
        $req->bindParam(":path_dc_img", $path_dc_img, PDO::PARAM_STR, strlen($path_dc_img));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_wc_name_in_bdd($wc_name) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `wc_name`= :wc_name WHERE `id`= :id');
        $req->bindParam(":wc_name", $wc_name, PDO::PARAM_STR, strlen($wc_name));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_dc_name_in_bdd($dc_name) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `dc_name`= :dc_name WHERE `id`= :id');
        $req->bindParam(":dc_name", $dc_name, PDO::PARAM_STR, strlen($dc_name));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_wc_content_in_bdd($wc_content) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `wc_content`= :wc_content WHERE `id`= :id');
        $req->bindParam(":wc_content", $wc_content, PDO::PARAM_STR, strlen($wc_content));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_dc_content_in_bdd($dc_content) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `dc_content`= :dc_content WHERE `id`= :id');
        $req->bindParam(":dc_content", $dc_content, PDO::PARAM_STR, strlen($dc_content));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_wc_address_in_bdd($wc_address) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `wc_address`= :wc_address WHERE `id`= :id');
        $req->bindParam(":wc_address", $wc_address, PDO::PARAM_STR, strlen($wc_address));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_dc_address_in_bdd($dc_address) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `dc_address`= :dc_address WHERE `id`= :id');
        $req->bindParam(":dc_address", $dc_address, PDO::PARAM_STR, strlen($dc_address));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

	public function set_wc_link_in_bdd($wc_link)
	{
		$bdd = self::connect();
		$req = $bdd->prepare('UPDATE article SET `wc_link`= :wc_link WHERE `id`= :id');
		$req->bindParam(":wc_link", $wc_link, PDO::PARAM_STR, strlen($wc_link));
		$req->bindParam(":id", $this->_id, PDO::PARAM_INT);
		$req->execute();
	}

	public function set_dc_link_in_bdd($dc_link)
	{
		$bdd = self::connect();
		$req = $bdd->prepare('UPDATE article SET `dc_link`= :dc_link WHERE `id`= :id');
		$req->bindParam(":dc_link", $dc_link, PDO::PARAM_STR, strlen($dc_link));
		$req->bindParam(":id", $this->_id, PDO::PARAM_INT);
		$req->execute();
	}

    public function set_wc_city_in_bdd($wc_city) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `wc_city`= :wc_city WHERE `id`= :id');
        $req->bindParam(":wc_city", $wc_city, PDO::PARAM_STR, strlen($wc_city));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_dc_city_in_bdd($dc_city) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `dc_city`= :dc_city WHERE `id`= :id');
        $req->bindParam(":dc_city", $dc_city, PDO::PARAM_STR, strlen($dc_city));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_categorie_in_bdd($categorie) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `categorie`= :categorie WHERE `id`= :id');
        $req->bindParam(":categorie", $categorie, PDO::PARAM_STR, strlen($categorie));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_categorie2_in_bdd($categorie2) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `categorie2`= :categorie2 WHERE `id`= :id');
        $req->bindParam(":categorie2", $categorie2, PDO::PARAM_STR, strlen($categorie2));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_categorie3_in_bdd($categorie3) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `categorie3`= :categorie3 WHERE `id`= :id');
        $req->bindParam(":categorie3", $categorie3, PDO::PARAM_STR, strlen($categorie3));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_date_published_in_bdd($date_published) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `date_published`= :date_published WHERE `id`= :id');
        $req->bindParam(":date_published", $date_published, PDO::PARAM_STR, strlen($date_published));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_last_modified_in_bdd($last_modified) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `last_modified`= :last_modified WHERE `id`= :id');
        $req->bindParam(":last_modified", $last_modified, PDO::PARAM_STR, strlen($last_modified));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_id_users_of_author_in_bdd($id_users_of_author) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `id_users_of_author`= :id_users_of_author WHERE `id`= :id');
        $req->bindParam(":id_users_of_author", $id_users_of_author, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_pseudo_author_in_bdd($pseudo_author) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `pseudo_author`= :pseudo_author WHERE `id`= :id');
        $req->bindParam(":pseudo_author", $pseudo_author, PDO::PARAM_STR, strlen($pseudo_author));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_id_authorized_mod_in_bdd($id_authorized_mod) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `id_authorized_mod`= :id_authorized_mod WHERE `id`= :id');
        $req->bindParam(":id_authorized_mod", $id_authorized_mod, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_id_comment_in_bdd($id_comment) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `id_comment`= :id_comment WHERE `id`= :id');
        $req->bindParam(":id_comment", $id_comment, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function set_authorized_in_bdd($authorized) {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE article SET `authorized`= :authorized WHERE `id`= :id');
        $req->bindParam(":authorized", $authorized, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();



        $req = $bdd->prepare('SELECT `pseudo_suiveur` FROM `follow` WHERE `pseudo_suivi`= :pseudo_suivi');
        $req->bindParam(":pseudo_suivi", $this->_pseudo_author, PDO::PARAM_STR, strlen($this->_pseudo_author));
        $req->execute();
        $followers = $req->fetchAll();
        $req->closeCursor();


        $req = $bdd->prepare('SELECT `id` FROM `article` WHERE `date_published`= :date_published AND `pseudo_author`= :pseudo_author');
        $req->bindParam(":date_published", $this->_date_published, PDO::PARAM_STR, strlen($this->_date_published));
        $req->bindParam(":pseudo_author", $this->_pseudo_author, PDO::PARAM_STR, strlen($this->_pseudo_author));
        $req->execute();
        $art_id = $req->fetchAll();
        $art = new Article($art_id[0]['id']);


        //print_r($followers);
        $i = 0;
        while ($followers[$i]['pseudo_suiveur'])
        {
            $follower = new User($followers[$i++]['pseudo_suiveur']);
            $suivi = new User($this->_pseudo_author);
            $this->follow_notif($suivi, $follower);
        }
    }

    public function add_likes_in_bdd() {
        $bdd = self::connect();
        $likes = $this->_likes + 1;
        $req = $bdd->prepare('UPDATE article SET `likes`= :likes WHERE `id`= :id');
        $req->bindParam(":likes", $likes, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function add_dislikes_in_bdd() {
        $bdd = self::connect();
        $dislikes = $this->_dislikes + 1;
        $req = $bdd->prepare('UPDATE article SET `dislikes`= :dislikes WHERE `id`= :id');
        $req->bindParam(":dislikes", $dislikes, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function del_likes_in_bdd() {
        $bdd = self::connect();
        $likes = $this->_likes - 1;
        $req = $bdd->prepare('UPDATE article SET `likes`= :likes WHERE `id`= :id');
        $req->bindParam(":likes", $likes, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }

    public function del_dislikes_in_bdd() {
        $bdd = self::connect();
        $dislikes = $this->_dislikes - 1;
        $req = $bdd->prepare('UPDATE article SET `dislikes`= :dislikes WHERE `id`= :id');
        $req->bindParam(":dislikes", $dislikes, PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
    }


    public function set_title($title) {
        $this->_title = $title;
    }

    public function set_path_wc_img($path_wc_img) {
        $this->_path_wc_img = $path_wc_img;
    }

    public function set_path_dc_img($path_dc_img) {
        $this->_path_dc_img = $path_dc_img;
    }

    public function set_wc_name($wc_name) {
        $this->_wc_name = $wc_name;
    }

    public function set_dc_name($dc_name) {
        $this->_dc_name = $dc_name;
    }

    public function set_wc_content($wc_content) {
        $this->_wc_content = $wc_content;
    }

    public function set_dc_content($dc_content) {
        $this->_dc_content = $dc_content;
    }

    public function set_wc_address($wc_address) {
        $this->_wc_address = $wc_address;
    }

    public function set_dc_address($dc_address) {
        $this->_dc_address = $dc_address;
    }

	public function set_wc_link($wc_link)
	{
		$this->_wc_link = $wc_link;
	}

	public function set_dc_link($dc_link)
	{
		$this->_dc_link = $dc_link;
	}

    public function set_wc_city($wc_city) {
        $this->_wc_city = $wc_city;
    }

    public function set_dc_city($dc_city) {
        $this->_dc_city = $dc_city;
    }

    public function set_categorie($categorie) {
        $this->_categorie = $categorie;
    }

    public function set_date_published($date_published) {
        $this->_date_published = $date_published;
    }

    public function set_last_modified($last_modified) {
        $this->_last_modified = $last_modified;
    }

    public function set_id_users_of_author($id_users_of_author) {
        $this->_id_users_of_author = $id_users_of_author;
    }

    public function set_pseudo_author($pseudo_author) {
        $this->_pseudo_author = $pseudo_author;
    }

    public function set_id_authorized_mod($id_authorized_mod) {
        $this->_id_authorized_mod = $id_authorized_mod;
    }

    public function set_id_comment($id_comment) {
        $this->_id_comment = $id_comment;
    }

    public function set_authorized($authorized) {
        $this->_authorized = $authorized;
    }

    public function add_likes() {
        $this->_likes += 1;
    }

    public function add_dislikes() {
        $this->_dislikes += 1;
    }

    public function del_likes() {
        $this->_likes -= 1;
    }

    public function del_dislikes() {
        $this->_dislikes -= 1;
    }



    ##########################################################################
                                    //AFFICHAGES
    ##########################################################################


    public function vignette($edit) {

        $str1 = $this->_path_wc_img;
        $str2 = $this->_path_dc_img;
        if($edit == 1)
            $link = "../../Controleur/Article/article.php?id=" . $this->_id . "&d=1";
        else if ($edit == 2)
            $link = "../../Controleur/Article/article.php?id=" . $this->_id . "&v=i";
        else if ($edit == 3)
            $link = "../../Controleur/Article/article.php?id=" . $this->_id . "&i=1";
        else
            $link = "../../Controleur/Article/article.php?id=" . $this->_id;
        if ($edit < 4) {
            printf('
                    <div class="column" style="">
                        <div class="">
                            <a class="vig" href="%s">
                                <div class="row">
                                    <div style="background: url(data:image/.;base64,%s); background-size: 130px 130px; margin-left: 11.5px; width: 130px;" class="small-6  medium-6 large-6 columns vigimg"><p class="tvig" style="">%s</p></div>
                                    <div style="background: url(data:image/.;base64,%s); background-size: 130px 130px; margin-right: 11.5px; width: 130px;" class="small-6  medium-6 large-6 columns vigimg"><p class="tvig" style="">%s</p></div>
                                </div>
                                <p class="contour-text">%s - %s</p>
                            </a>
                        </div>
                    </div>', $link, $str1, $this->_wc_city, $str2, $this->_dc_city, $this->_date_published, $this->_categorie);
        }
        else if ($edit == 4)
        {
            $str = sprintf('
                    <div class="column" style="">
                        <div class="">
                            <a class="vig" href="%s">
                                <div class="row">
                                    <div style="background: url(data:image/.;base64,%s); background-size: 130px 130px; margin-left: 11.5px; width: 130px;" class="small-6  medium-6 large-6 columns vigimg"><p class="tvig" style="">%s</p></div>
                                    <div style="background: url(data:image/.;base64,%s); background-size: 130px 130px; margin-right: 11.5px; width: 130px;" class="small-6  medium-6 large-6 columns vigimg"><p class="tvig" style="">%s</p></div>
                                </div>
                                <p class="contour-text">%s - %s</p>
                            </a>
                        </div>
                    </div>', $link, $str1, $this->_wc_city, $str2, $this->_dc_city, $this->_date_published, $this->_categorie);
            return $str;
        }
    }

    public function slide_down() {

        $str1 = $this->_path_wc_img;
        $str2 = $this->_path_dc_img;
        printf('
                <li class="is-active orbit-slide">
                    <a class="vig1" href="../../Controleur/Article/article.php?id=%d">
                        <div class="row">
                            <div style="background: url(data:image/.;base64,%s); background-size: 425px 425px; width: 425px; height: 425px;" class="small-6  medium-6 large-6 columns vigimg1"><p class="tvig" style="padding-top: 200px">%s</p></div>
                            <div style="background: url(data:image/.;base64,%s); background-size: 425px 425px; width: 425px; height: 425px;" class="small-6  medium-6 large-6 columns vigimg1"><p class="tvig" style="padding-top: 200px">%s</p></div>
                        </div>
                    </a>
                    <figcaption class="orbit-caption txth">%s - %s</figcaption>
                </li>', $this->_id, $str1, $this->_wc_city, $str2, $this->_dc_city, $this->_date_published, $this->_categorie);
    }

    public function slide_top() {

        $str1 = $this->_path_wc_img;
        $str2 = $this->_path_dc_img;
        printf('
                <li class="is-active orbit-slide">
                    <a href="../../Controleur/Article/article.php?id=%d">
                        <div class="menu-centered">
                                <img src="data:image/.;base64,%s" class="cofd" alt="">
                                <img src="data:image/.;base64,%s" class="cofd" alt="">
                        </div>
                    </a>
                    <figcaption class="orbit-caption txth" style="text-align: center; padding: inherit;"><p style="font-size: 34px; font-weight: lighter; margin-bottom: inherit;">WORKING CONCEPT DYING CONCEPT OF THE <strong style="font-weight: bold; font-size: 38px;">DAY</strong></p></figcaption>
                </li>',$this->_id, $str1, $str2);
    }

    public function article() {

        $str1 = $this->_path_wc_img;
        $str2 = $this->_path_dc_img;

        if ($this->_wc_city === $this->_dc_city)
            $city = $this->_wc_city;
        else
            $city = $this->_wc_city . " | " . $this->_dc_city;

        $user = new User($_SESSION['logged_on_user']);

        if(!$_GET['v'] && !$_GET['i'] && !$_GET['d'])
        {
            printf('
                        <div class="menu-centered contact-title1" style="padding-top: 20px">%s</div>
                        <div class="article-bar"></div>
                        <div class="rond2 menu-centered">
                        
                        <div class="row">
                            <div class="small-6 medium-6 large-6 column medium-centered">
                                <img src="../../img/Working_Logo.png" alt="Working Concept" style="position: absolute; left: -250px; top: 10px;">
                            </div>
                            <div class="small-6 medium-6 large-6 column medium-centered">
                                <img src="../../img/Dying_Logo.png" alt="Dying Concept" style="position: absolute; right: -250px;">                            
                            </div>
                        </div>
                        
                            <div class="circle">
                                <p class="art-title2 town" style="font-size: 12px;">%s</p>
                                <hr/>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                            </div>
                        </div>
                
                        <div class="row">
                            <div class="small-6 medium-6 large-6 column workingc">
                                <img src="data:image/.;base64,%s" class="artimg wcartimg">
                                <hr size="1" align="center" style="padding-top: 30px; color: black;">
                                <div class="menu-centered contact-title2">%s</div>
                                <p class="art">%s</p>
                                <a href="%s">%s</a>
                            </div>
                            <div class="small-6 medium-6 large-6 column dyingc">
                                <img src="data:image/.;base64,%s" class="artimg dcartimg">
                                <hr size="1" align="center" style="padding-top: 30px; color: white;">
                                <div class="menu-centered contact-title2">%s</div>
                                <p class="art">%s</p>
                                <a href="%s">%s</a>
                            </div>
                        </div>
                        <hr size="1" align="center" style="position: relative; top: -35px;">
                        <div style="margin-left: 30px;"><a href="../../Controleur/User_interface/user.php?pseudo=%s" style="text-decoration: none; color: black;">Submit by %s</a></div>
                        
                        <br/>
                       
                        <div class="fb-share-button" data-href="http://workingconceptdyingconcept.com/WCDC/Controleur/Article/article.php?id=%d" data-layout="button" data-size="small" data-mobile-iframe="false" style="margin-left: 30px; margin-bottom: 10px;">
                            <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%%3A%%2F%%2Fworkingconceptdyingconcept.com%%2FWCDC%%2FControleur%%2FArticle%%2Farticle.php%%3Fid%%3D%d&amp;src=sdkpreparse">Share<img style="height: 16px; width: 16px;" src="../../img/fb.png"/></a>
                        </div>
                        <a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical" data-via="WCDC4YOU">Tweet</a>
                        <script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>
                        <script type="text/javascript" src="https://platform.linkedin.com/in.js"></script>
                        <script type="in/share" data-counter="right"></script>
                        <br/>
                        <br/>
                        
                        <div class="youtube row">
                            <div class="small-4 medium-4 large-4 columns">  
                                <p class="comm-title" style="margin: inherit; margin-top: 30px;">Comments (<span id="nb_comms">%d</span>)</p>
                            </div>
                            <div class="like-dislike-button">
                                <div class="small-2 medium-2 large-2 columns like-dislike" style="padding: inherit;">
                                    <p id="likes" style="text-align: center;">%d</p>
                                </div>
                                <div class="small-2 medium-2 large-2 columns like-dislike" style="padding: inherit;">
                                    <a href="../../Modele/Article/likes_dislikes.php?id=%d" class="likes" id="lik"><img class="like_dislike_img" src="../../img/likes_img.png"></a>
                                </div>
                                <div class="small-2 medium-2 large-2 columns like-dislike" style="padding: inherit;">
                                    <a href="../../Modele/Article/likes_dislikes.php?id=%d" class="dislikes" id="dis"><img class="like_dislike_img" src="../../img/dislikes_img.png"></a>
                                </div>
                                <div class="small-2 medium-2 large-2 columns like-dislike" style="padding: inherit;">
                                    <p id="dislikes" style="text-align: center">%d</p>
                                </div>
                            </div>
                        </div>
                        
                        <form id="add-comm">
                            <div class="row">
                                <div class="large-6 small-6 medium-6 column large-centered medium-centered small-centered input-group">
                                    <input id="input-add-comm" type="text" name="commentaire" placeholder="Add a commentary" minlength="2" maxlength="2000">
                                    <div class="input-group-button">
                                        <input type="hidden" name="id_article" value="%d">
                                      <input id="add-comm-btn" type="submit" class="button" value="Submit" style="margin-right: 250px;margin-bottom: 15px;">
                                    </div>
                                </div>
                            </div>
                        </form>',   $this->_title, $city, $this->_categorie1, $this->_categorie2, $this->_categorie3,
                                    $this->_path_wc_img, $this->_wc_name, $this->_wc_content, $this->_wc_link, $this->_wc_link, $this->_path_dc_img,
                                    $this->_dc_name, $this->_dc_content, $this->_dc_link, $this->_dc_link, $this->_pseudo_author, $this->_pseudo_author, $this->_id, $this->_id, $this->_count,
                                    $this->_likes, $this->_id, $this->_id, $this->_dislikes, $this->_id);
        }
        else if ($_GET['v'] == 'i' && $user->get_right_access() >= 2)
        {
            printf('
                    <div>
                        <a href="../../Controleur/User_interface/articles_gestion.php"><button id="back-button" class="button">Back</button></a>
                        <div class="menu-centered contact-title1" style="padding-top: 20px">%s</div>
                        <div class="article-bar"></div>
                        <div class="rond2 menu-centered">
                            <div class="circle">
                                <p class="art-title2 town" style="font-size: 12px;">%s</p>
                                <hr/>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                            </div>
                        </div>
                
                        <div class="row">
                            <div class="small-6 medium-6 large-6 column workingc">
                                <img src="data:image/.;base64,%s" class="artimg wcartimg">
                                <hr size="1" align="center" style="padding-top: 30px; color: black;">
                                <div class="menu-centered contact-title2">%s</div>
                                <p class="art">%s</p>
                            </div>
                            <div class="small-6 medium-6 large-6 column dyingc">
                                <img src="data:image/.;base64,%s" class="artimg dcartimg">
                                <hr size="1" align="center" style="padding-top: 30px; color: white;">
                                <div class="menu-centered contact-title2">%s</div>
                                <p class="art">%s</p>
                            </div>
                        </div>
                        <hr size="1" align="center" style="position: relative; top: -70px;">
                        <div style="margin-left: 10px;"><a href="../../Controleur/User_interface/user.php?pseudo=%s" style="text-decoration: none; color: black;">Submit by %s</a></div>
                        <br/>
                        <br/>
                       <!-- <a href="#"><div class="comm art-comm">Commenter</div></a> -->
                       
                         <div class="row menu-centered">
                            <div class="small-12 medium-12 large-12 columns" >
                                <div class="small-6 medium-6 large-6 columns">
                                    <a href="../../Controleur/User_interface/articles_gestion.php?id=%d&val=1"><button class="button success" style="position: relative; top: -50px; float: right;">Validate</button></a>
                                </div>
                                <div class="small-6 medium-6 large-6 columns">
                                    <a href="../../Controleur/User_interface/articles_gestion.php?id=%d&val=-1"><button class="button alert" style="position: relative; top: -50px; float: left;">Ban</button></a>
                                </div>
                            </div>
                         </div>                
                    </div>',    $this->_title, $city, $this->_categorie1, $this->_categorie2, $this->_categorie3, $str1, $this->_wc_name,
                                $this->_wc_content, $str2, $this->_dc_name, $this->_dc_content, $this->_pseudo_author,
                                $this->_pseudo_author, $this->_id, $this->_id);
        }
        else if ($_GET['i'] == 1)
        {
            if ($user->get_right_access() == 2)
            {
                printf('
                        <div>
                            <a href="../../Controleur/User_interface/info.php"><button id="back-button" class="button">Back</button></a>
                            <div class="menu-centered contact-title1" style="padding-top: 20px">%s</div>
                            <div class="article-bar"></div>
                            <div class="rond2 menu-centered">
                                <div class="circle">
                                    <p class="art-title2 town" style="font-size: 12px;">%s</p>
                                    <hr/>
                                    <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                    <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                    <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                </div>
                            </div>
                    
                            <div class="row">
                                <div class="small-6 medium-6 large-6 column workingc">
                                    <img src="data:image/.;base64,%s" class="artimg wcartimg">
                                    <hr size="1" align="center" style="padding-top: 30px; color: black;">
                                    <div class="menu-centered contact-title2">%s</div>
                                    <p class="art">%s</p>
                                </div>
                                <div class="small-6 medium-6 large-6 column dyingc">
                                    <img src="data:image/.;base64,%s" class="artimg dcartimg">
                                    <hr size="1" align="center" style="padding-top: 30px; color: white;">
                                    <div class="menu-centered contact-title2">%s</div>
                                    <p class="art">%s</p>
                                </div>
                            </div>
                            <hr size="1" align="center" style="position: relative; top: -70px;">
                            <div style="margin-left: 10px;"><a href="../../Controleur/User_interface/user.php?pseudo=%s" style="text-decoration: none; color: black;">Submit by %s</a></div>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                           <!-- <a href="#"><div class="comm art-comm">Commenter</div></a> -->
                           
                             <div class="row menu-centered">
                                <div class="small-12 medium-12 large-12 columns" >
                                    <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&st=1"><button class="button success" style="position: relative; top: -50px;float: left;">Slide top add</button></a>
                                    </div>
                                    <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&st=-1"><button class="button alert" style="position: relative; top: -50px;float: left;">Slide top del</button></a>
                                    </div>
                                     <!-- <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&sd=1"><button class="button success" style="position: relative; top: -50px; float: right;">Slide down add</button></a>
                                    </div>
                                     <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&sd=-1"><button class="button alert" style="position: relative; top: -50px; float: right;">Slide down del</button></a>
                                    </div> -->
                                </div>
                             </div>
                        </div>', $this->_title, $city, $this->_categorie1, $this->_categorie2, $this->_categorie3, $str1,
                    $this->_wc_name, $this->_wc_content, $str2, $this->_dc_name, $this->_dc_content,
                    $this->_pseudo_author, $this->_pseudo_author, $this->_id, $this->_id, $this->_id, $this->_id);
            }
            else if ($user->get_right_access() >= 3)
            {
                printf('
                        <div>
                            <a href="../../Controleur/User_interface/info.php"><button id="back-button" class="button">Back</button></a>
                            <a href="../../Modele/User_interface/delete_your_concept.php?id=%d&backup=1" style="float: right; margin-top: 20px; margin-left: 10px; margin-right: 10px;"><button class="button alert">Delete</button></a>
                            <div class="menu-centered contact-title1" style="padding-top: 20px">%s</div>
                            <div class="article-bar"></div>
                            <div class="rond2 menu-centered">
                                <div class="circle">
                                    <p class="art-title2 town" style="font-size: 12px;">%s</p>
                                    <hr/>
                                    <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                    <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                    <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                </div>
                            </div>
                    
                            <div class="row">
                                <div class="small-6 medium-6 large-6 column workingc">
                                    <img src="data:image/.;base64,%s" class="artimg wcartimg">
                                    <hr size="1" align="center" style="padding-top: 30px; color: black;">
                                    <div class="menu-centered contact-title2">%s</div>
                                    <p class="art">%s</p>
                                </div>
                                <div class="small-6 medium-6 large-6 column dyingc">
                                    <img src="data:image/.;base64,%s" class="artimg dcartimg">
                                    <hr size="1" align="center" style="padding-top: 30px; color: white;">
                                    <div class="menu-centered contact-title2">%s</div>
                                    <p class="art">%s</p>
                                </div>
                            </div>
                            <hr size="1" align="center" style="position: relative; top: -70px;">
                            <div style="margin-left: 10px;"><a href="../../Controleur/User_interface/user.php?pseudo=%s" style="text-decoration: none; color: black;">Submit by %s</a></div>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                           <!-- <a href="#"><div class="comm art-comm">Commenter</div></a> -->
                           
                             <div class="row menu-centered">
                                <div class="small-12 medium-12 large-12 columns" >
                                    <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&st=1"><button class="button success" style="position: relative; top: -50px;float: left;">Slide top add</button></a>
                                    </div>
                                    <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&st=-1"><button class="button alert" style="position: relative; top: -50px;float: left;">Slide top del</button></a>
                                    </div>
                                     <!-- <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&sd=1"><button class="button success" style="position: relative; top: -50px; float: right;">Slide down add</button></a>
                                    </div>
                                     <div class="small-3 medium-3 large-3 columns">
                                        <a href="../../Controleur/User_interface/info.php?id=%d&sd=-1"><button class="button alert" style="position: relative; top: -50px; float: right;">Slide down del</button></a>
                                    </div> -->
                                </div>
                             </div>
                        </div>', $this->_id, $this->_title, $city, $this->_categorie1, $this->_categorie2, $this->_categorie3, $str1,
                    $this->_wc_name, $this->_wc_content, $str2, $this->_dc_name, $this->_dc_content,
                    $this->_pseudo_author, $this->_pseudo_author, $this->_id, $this->_id, $this->_id, $this->_id);
            }
        }
        else if ($_GET['d'] == 1)
        {
            printf('
                    <div>
                    <a href="../../Controleur/User_interface/edit.php"><button id="back-button" class="button">Back</button></a>
                    <a href="../../Modele/User_interface/delete_your_concept.php?id=%d" style="float: right; margin-top: 20px; margin-left: 10px; margin-right: 10px;"><button class="button alert">Delete</button></a>
                    <a href="../../Controleur/User_interface/edit_your_concept.php?id=%d" style="float: right; margin-top: 20px;"><button class="button success">Edit</button></a>
                        <div class="menu-centered contact-title1" style="padding-top: 20px">%s</div>
                        <div class="article-bar"></div>
                        <div class="rond2 menu-centered">
                           <div class="circle">
                                <p class="art-title2 town" style="font-size: 12px;">%s</p>
                                <hr/>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                                <p class="" style="font-size: 12px; margin-top: -15px;">%s</p>
                            </div>
                        </div>
                
                        <div class="row">
                            <div class="small-6 medium-6 large-6 column workingc">
                                <img src="data:image/.;base64,%s" class="artimg wcartimg">
                                <hr size="1" align="center" style="padding-top: 30px; color: black;">
                                <div class="menu-centered contact-title2">%s</div>
                                <p class="art">%s</p>
                            </div>
                            <div class="small-6 medium-6 large-6 column dyingc">
                                <img src="data:image/.;base64,%s" class="artimg dcartimg">
                                <hr size="1" align="center" style="padding-top: 30px; color: white;">
                                <div class="menu-centered contact-title2">%s</div>
                                <p class="art">%s</p>
                            </div>
                        </div>
                        <hr size="1" align="center" style="position: relative; top: -70px;">
                        <div style="margin-left: 10px;"><a href="../../Controleur/User_interface/user.php?pseudo=%s" style="text-decoration: none; color: black;">Submit by %s</a></div>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                       <!-- <a href="#"><div class="comm art-comm">Commenter</div></a> -->
                    </div>',    $this->_id, $this->_id, $this->_title, $city, $this->_categorie1, $this->_categorie2, $this->_categorie3, $str1, $this->_wc_name,
                $this->_wc_content, $str2, $this->_dc_name, $this->_dc_content, $this->_pseudo_author,
                $this->_pseudo_author, $this->_id, $this->_id, $this->_id, $this->_id);
        }
    }

    
    

    ##########################################################################
                                    //TO_STRING
    ##########################################################################
    
    public function __toString()
    {
        $str = sprintf("<br/><br/>id:       %d<br/><br/>
                        title:              %s<br/><br/>
                        path_wc_img:        %s<br/><br/>
                        path_dc_img:        %s<br/><br/>
                        wc_name:            %s<br/><br/>
                        dc_name:            %s<br/><br/>
                        wc_content:         %s<br/><br/>
                        dc_content:         %s<br/><br/>
                        wc_address:         %s<br/><br/>
                        dc_address:         %s<br/><br/>
                        wc_city:            %s<br/><br/>
                        dc_city:            %s<br/><br/>
                        category:           %s<br/><br/>
                        date_published:     %s<br/><br/>
                        last_modified:      %s<br/><br/>
                        id_users_of_author: %d<br/><br/>
                        id_authorized_mod:  %d<br/><br/>
                        id_comment:         %d<br/><br/>
                        authorized:         %d<br/><br/>
                        likes:              %d<br/><br/>
                        dislikes:           %d<br/><br/>", $this->_id, $this->_title, $this->_path_wc_img,
            $this->_path_dc_img, $this->_wc_name, $this->_dc_name, $this->_wc_content, $this->_dc_content,
            $this->_wc_address, $this->_dc_address, $this->_wc_city, $this->_dc_city, $this->_categorie,
            $this->_date_published, $this->_last_modified, $this->_id_users_of_author, $this->_id_authorized_mod, $this->_authorized, $this->_likes, $this->_dislikes);
        return $str;
    }
    
}

//$article = new Article(27);
//print($article->__toString());
//$article->vignette();
//$article->slide_down();