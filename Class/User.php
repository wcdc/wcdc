<?php

/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/7/16
 * Time: 12:32 PM
 */

require_once ("../../Class/Article.php");
require_once ("../../Class/Commentaires.php");
require_once ("../../Class/Brand.php");

class User
{
    private $_id;
    private $_nom;
    private $_prenom;
    private $_pseudo;
    private $_password;
    private $_mail;
    private $_ville;
    private $_date_de_naissance;
    private $_sexe;
    private $_token_conf;
    private $_mail_confirmed;
    private $_authorized;
    private $_credibility;
    private $_path_profil_photo;
    private $_right_access;
    private $_user_articles;
    private $_likes;
    private $_dislikes;
    private $_commentaires;
    private $_follow_users;
    private $_followers;
    private $_brand_tab;

    public function connect()
    {
        try
        {
	        $bdd = new PDO('mysql:host=ba677080-001.privatesql;dbname=workingcecwcdc;port=35319;charset=utf8', 'user', 'Userpassword1', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }
        return $bdd;
    }

    public function send_confirmation_mail($user)
    {
        $objet = "Confirmation d'inscription WCDC";

        // message
        $message = '
         <html>
          <head>
               <title>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</title>
          </head>
          <body>
                <h1>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</h1>
                <p>In order to confirm your mail address please follow this link.</p>
          <p><a href="https://workingconceptdyingconcept.com/WCDC/Controleur/Accueil/accueil.php?t=' . $user->get_token_conf() . '">Confirmed mail.</a></p>
          <br/>
          <p>Best Regards,</p>
          <p>The WCDC Team</p> 
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'To: ' . $user->get_mail() . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($user->_mail, $objet, $message, $headers);
        
    }

    public function send_confirmation_submit_concept_successful_mail($user)
    {
        $objet = "Votre WCDC a été reçus avec succès";

        // message
        $message = '
         <html>
          <head>
               <title>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</title>
          </head>
          <body>
                <h1>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</h1>
                <p>Thanks you ! Your WCDC has been submitted. You will received a notification once it is been reviewed by our team.</p>
          <br/>
          <p>Best Regards,</p>
          <p>The WCDC Team</p>
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'To: ' . $user->get_mail() . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($user->_mail, $objet, $message, $headers);

    }

    public function send_validate_concept_mail($user)
    {
        $objet = "Votre WCDC a été validé";

        // message
        $message = '
         <html>
          <head>
               <title>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</title>
          </head>
          <body>
                <h1>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</h1>
                <p>Your WCDC has been approuved. It is now visible on the site. Feel free to share it with your friends.</p>
          <br/>
          <p>Best Regards,</p>
          <p>The WCDC Team</p>
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'To: ' . $user->get_mail() . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($user->_mail, $objet, $message, $headers);

    }

    public function send_ban_concept_mail($user)
    {
        $objet = "Votre WCDC n'a pas été accepté";

        // message
        $message = '
         <html>
          <head>
               <title>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</title>
          </head>
          <body>
                <h1>Hello ' . $user->get_prenom() . ' ' . $user->get_nom() . '</h1>
                <p>Your are sorry to inform you that your WCDC has been denied. Feel free to share other concepts with us.</p>
          <br/>
          <p>Best Regards,</p>
          <p>The WCDC Team</p>
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'To: ' . $user->get_mail() . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($user->_mail, $objet, $message, $headers);

    }

    public function send_reset_password_mail()
    {
        $objet = "Demande de réinitialisation de password WCDC";

        // message
        $message = '
         <html>
          <head>
               <title>Hello ' . $this->_prenom . ' ' . $this->_nom . '</title>
          </head>
          <body>
                <h1>Hello ' . $this->_prenom . ' ' . $this->_nom . '</h1>
                <p>Please follow this link to reset your password.</p>
          <p><a href="https://workingconceptdyingconcept.com/WCDC/Controleur/User_interface/reset_pass.php?t=' . $this->_token_conf . '">Reset your password</a></p>
          <br/>
          <p>Best Regards,</p>
          <p>The WCDC Team</p>
          </body>
         </html>
         ';

        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'To: ' . $this->_mail . "\r\n";
        $headers .= 'From: WorkingConceptDyingConcept<webmaster@workingconceptdyingconcept.com>' . "\r\n";
        mail($this->_mail, $objet, $message, $headers);
    }


    

    public function __construct($pseudo)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('SELECT * FROM users WHERE `pseudo`= :pseudo');
        $req->bindParam(":pseudo", $pseudo, PDO::PARAM_STR, strlen($pseudo));
        $req->execute();
        $user = $req->fetch();
        $req->closeCursor();
        $req = NULL;

        $this->_id = $user['id'];
        $this->_nom = $user['nom'];
        $this->_prenom = $user['prenom'];
        $this->_pseudo = $user['pseudo'];
        $this->_password = $user['password'];
        $this->_mail = $user['mail'];
        $this->_ville = $user['ville'];
        $this->_date_de_naissance = $user['date_de_naissance'];
        $this->_sexe = $user['sexe'];
        $this->_token_conf = $user['token_conf'];
        $this->_mail_confirmed = $user['mail_confirmed'];
        $this->_authorized = $user['authorized'];
        $this->_credibility = $user['credibility'];
        $this->_path_profil_photo = $user['profil_photo'];
        $this->_right_access = $user['right_access'];

        $req = $bdd->prepare('SELECT `id` FROM article WHERE `id_users_of_author`= :id');
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $articles = $req->fetchAll();
        $req->closeCursor();
        //print_r($articles);

        $tab = array();
        $i = 0;

        while ($articles[$i]['id'])
        {
            $article = new Article($articles[$i++]['id']);
            array_push($tab, $article);
        }
        $this->_user_articles = $tab;

        $req = $bdd->prepare('SELECT `id_article` FROM `likes` WHERE `id_user`= :id');
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $likes = $req->fetchAll();
        $req->closeCursor();

        $tab = array();
        $i = 0;

        while ($likes[$i]['id_article'])
        {
            $like = $likes[$i++]['id_article'];
            array_push($tab, $like);
        }
        $this->_likes = $tab;

        $req = $bdd->prepare('SELECT `id_article` FROM `dislikes` WHERE `id_user`= :id');
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $dislikes = $req->fetchAll();
        $req->closeCursor();

        $tab = array();
        $i = 0;

        while ($dislikes[$i]['id_article'])
        {
            $dislike = $dislikes[$i++]['id_article'];
            array_push($tab, $dislike);
        }
        $this->_dislikes = $tab;


        $req = $bdd->prepare('SELECT `id` FROM `commentaires` WHERE `id_author`= :id_author');
        $req->bindParam(":id_author", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $comms = $req->fetchAll();
        $req->closeCursor();

        $tab = array();
        $i = 0;

        while ($comms[$i]['id'])
        {
            $tmp = new Commentaires($comms[$i++]['id']);
            array_push($tab, $tmp);
        }
        $this->_commentaires = $tab;

        $req = $bdd->prepare('SELECT `pseudo_suivi` FROM `follow` WHERE `pseudo_suiveur`= :pseudo');
        $req->bindParam(":pseudo", $this->_pseudo, PDO::PARAM_STR, strlen($this->_pseudo));
        $req->execute();
        $follows = $req->fetchAll();
        $req->closeCursor();

        $tab = array();
        $i = 0;

        while ($follows[$i]['pseudo_suivi'])
        {
            $tmp = $follows[$i++]['pseudo_suivi'];
            array_push($tab, $tmp);
        }
        $this->_follow_users = $tab;



        $req = $bdd->prepare('SELECT `pseudo_suiveur` FROM `follow` WHERE `pseudo_suivi`= :pseudo');
        $req->bindParam(":pseudo", $this->_pseudo, PDO::PARAM_STR, strlen($this->_pseudo));
        $req->execute();
        $followers = $req->fetchAll();
        $req->closeCursor();

        $tab = array();
        $i = 0;

        while ($followers[$i]['pseudo_suiveur'])
        {
            $tmp = $followers[$i++]['pseudo_suiveur'];
            array_push($tab, $tmp);
        }
        $this->_followers = $tab;

        $req = $bdd->prepare('SELECT * FROM `brand` WHERE 1');
        $req->execute();
        $brands = $req->fetchAll();
        $req->closeCursor();

        $tab = array();
        $tab2 = array();
        $i = 0;

        while ($brands[$i]['name'])
        {
            $tmp = new Brand($brands[$i]['name']);
            array_push($tab, $tmp);
            $i++;
        }
        foreach ($tab as $brand)
        {
            $user = array("id" => $this->_id, "pseudo" => $this->_pseudo);
            if (in_array($user, $brand->getTabPseudoAssoc()))
            {
                $tmp_name = $brand->getName();
                $tmp_obj = new Brand($tmp_name);
                array_push($tab2, $tmp_obj);
            }
        }
        $this->_brand_tab = $tab2;
    }

    public function __destruct()
    {
    }



    ##########################################################################
                                    //GETTERS
    ##########################################################################
    public function get_id()
    {
        return $this->_id;
    }

    public function get_nom()
    {
        return $this->_nom;
    }

    public function get_prenom()
    {
        return $this->_prenom;
    }

    public function get_pseudo()
    {
        return $this->_pseudo;
    }

    public function get_password()
    {
        return $this->_password;
    }
    public function get_mail()
    {
        return $this->_mail;
    }

    public function get_ville()
    {
        return $this->_ville;
    }

    public function get_date_de_naissance()
    {
        return $this->_date_de_naissance;
    }

    public function get_sexe()
    {
        return $this->_sexe;
    }

    public function get_token_conf()
    {
        return $this->_token_conf;
    }
    
    public function get_mail_confirmed()
    {
        return $this->_mail_confirmed;
    }

    public function get_authorized()
    {
        return $this->_authorized;
    }

    public function get_credibility()
    {
        return $this->_credibility;
    }

    public function get_path_profil_photo()
    {
        return $this->_path_profil_photo;
    }

    public function get_right_access()
    {
        return $this->_right_access;
    }

    public function get_user_articles()
    {
        return $this->_user_articles;
    }

    public function get_user_likes()
    {
        return $this->_likes;
    }

    public function get_user_dislikes()
    {
        return $this->_dislikes;
    }

    public function get_follow_users()
    {
        return $this->_follow_users;
    }

    public function get_followers()
    {
        return $this->_followers;
    }


    ##########################################################################
                                    //SETTERS
    ##########################################################################

    public function set_nom($nom)
    {
        $this->_nom = $nom;
    }

    public function set_prenom($prenom)
    {
        $this->_prenom = $prenom;
    }

    public function set_pseudo($pseudo)
    {
        $this->_pseudo = $pseudo;
    }

    public function set_password($password)
    {
        $this->_password = $password;
    }
    public function set_mail($mail)
    {
        $this->_mail = $mail;
    }
    public function set_ville($ville)
    {
        $this->_ville = $ville;
    }

    public function set_date_de_naissance($date_de_naissance)
    {
        $this->_date_de_naissance = $date_de_naissance;
    }

    public function set_sexe($sexe)
    {
        $this->_sexe = $sexe;
    }
    
    public function set_token_conf($token)
    {
        $this->_token_conf = $token;
    }
    
    public function set_mail_confirmed($mail_confirmed)
    {
        $this->_mail_confirmed = $mail_confirmed;
    }

    public function set_authorized($authorized)
    {
        $this->_authorized = $authorized;
    }

    public function set_credibility($credibility)
    {
        $this->_credibility = $credibility;
    }

    public function set_path_profil_photo($path_profil_photo)
    {
        $this->_path_profil_photo = $path_profil_photo;
    }

    public function set_right_access($right_access)
    {
        $this->_right_access = $right_access;
    }

    public function set_add_article($article)
    {
        array_push($this->_user_articles, $article);
    }

    public function set_user_articles($user_articles)
    {
        $this->_user_articles = $user_articles;
    }

    public function add_user_likes($id_article)
    {
        array_push($this->_likes, $id_article);
    }

    public function add_user_dislikes($id_article)
    {
        array_push($this->_dislikes, $id_article);
    }


    public function del_user_likes($id_article)
    {
        $key = array_search($id_article, $this->_likes);
        if ($key != FALSE)
        {
            $this->_likes[$key] = "";
            array_filter($this->_likes);
        }
    }

    public function del_user_dislikes($id_article)
    {
        $key = array_search($id_article, $this->_dislikes);
        if ($key != FALSE)
        {
            $this->_dislikes[$key] = "";
            array_filter($this->_dislikes);
        }
    }




    public function set_nom_in_bdd($nom)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `nom`= :nom WHERE `id`= :id');
        $req->bindParam(":nom", $nom ,PDO::PARAM_STR, strlen($nom));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_prenom_in_bdd($prenom)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `prenom`= :prenom WHERE `id`= :id');
        $req->bindParam(":prenom", $prenom ,PDO::PARAM_STR, strlen($prenom));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_pseudo_in_bdd($new_pseudo)
    {
        $bdd = self::connect();

        if ($pseudo = filter_var($new_pseudo, FILTER_SANITIZE_STRING))
        {
            $pseudo = strtolower($pseudo);
            $req = $bdd->prepare('SELECT `pseudo` FROM users WHERE `pseudo`= :pseudo');
            $req->bindParam(":pseudo", $pseudo, PDO::PARAM_STR, strlen($pseudo));
            $req->execute();
            $req->closeCursor();

            if ($req->rowCount() > 0)
            {
                $req->closeCursor();
                header('Location:../../Controleur/User_interface/modif.php?e=3');
                exit(0);
            }
            else
            {
                $oldPseudo = $this->_pseudo;

                foreach ($this->_user_articles as $article)
                {
                    $article->set_pseudo_author_in_bdd($pseudo);
                }
                foreach ($this->_commentaires as $commentaire)
                {
                    $commentaire->set_pseudo_author_in_bdd($new_pseudo);
                }

                $req = $bdd->prepare('UPDATE `likes` SET `pseudo`= :new_pseudo WHERE `id_user`= :id');
                $req->bindParam(":new_pseudo", $new_pseudo, PDO::PARAM_STR,strlen($new_pseudo));
                $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
                $req->execute();
                $req->closeCursor();

                $req = $bdd->prepare('UPDATE `dislikes` SET `pseudo`= :new_pseudo WHERE `id_user`= :id');
                $req->bindParam(":new_pseudo", $new_pseudo, PDO::PARAM_STR,strlen($new_pseudo));
                $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
                $req->execute();
                $req->closeCursor();

                $req = $bdd->prepare('UPDATE `follow` SET `pseudo_suivi`= :new_pseudo WHERE `id_suivi`= :id');
                $req->bindParam(":new_pseudo", $new_pseudo, PDO::PARAM_STR,strlen($new_pseudo));
                $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
                $req->execute();
                $req->closeCursor();

                $req = $bdd->prepare('UPDATE `follow` SET `pseudo_suiveur`= :new_pseudo WHERE `id_suiveur`= :id');
                $req->bindParam(":new_pseudo", $new_pseudo, PDO::PARAM_STR,strlen($new_pseudo));
                $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
                $req->execute();
                $req->closeCursor();

                foreach ($this->_brand_tab as $brand)
                {
                    $brand->updatePseudoToTabPseusoAssocInBdd($this->_id, $new_pseudo, $oldPseudo);
                }
                
                $token = hash('whirlpool', $this->_pseudo) . $this->_password;
                $req = $bdd->prepare('UPDATE `users` SET `pseudo`= :pseudo, `token_conf`= :token WHERE `id`= :id');
                $req->bindParam(":pseudo", $pseudo ,PDO::PARAM_STR, strlen($pseudo));
                $req->bindParam(":token", $token, PDO::PARAM_STR, strlen($token));
                $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
                $req->execute();
                $req->closeCursor();
                $_SESSION['logged_on_user'] = $pseudo;
                header('Location:../../Controleur/User_interface/user.php?pseudo=' . $_SESSION['logged_on_user']);
                exit(0);
            }
        }
    }

    public function set_password_in_bdd($old_password, $new_password)
    {
        $bdd = self::connect();
        $old_password = hash('whirlpool', $old_password);
        if ($old_password === $this->_password)
        {
            $password = hash('whirlpool', $new_password);
            $token = hash('whirlpool', $this->_pseudo) . $password;
            $req = $bdd->prepare('UPDATE `users` SET `password`= :password, `token_conf`= :token WHERE `id`= :id');
            $req->bindParam(":password", $password, PDO::PARAM_STR, strlen($password));
            $req->bindParam(":token", $token, PDO::PARAM_STR, strlen($token));
            $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
            $req->execute();
            $req->closeCursor();
            header('Location:../../Controleur/User_interface/user.php?pseudo=' . $_SESSION['logged_on_user']);
            exit(0);
        }
        else
        {
            header('Location:../../Controleur/User_interface/modif.php?e=5');
            exit(0);
        }
    }
    
    public function reset_password_in_bdd ($new_password)
    {
        $bdd = self::connect();

        $password = hash('whirlpool', $new_password);
        $token = hash('whirlpool', $this->_pseudo) . $password;
        $req = $bdd->prepare('UPDATE `users` SET `password`= :password, `token_conf`= :token WHERE `id`= :id');
        $req->bindParam(":password", $password, PDO::PARAM_STR, strlen($password));
        $req->bindParam(":token", $token, PDO::PARAM_STR, strlen($token));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
        header('Location:../../Controleur/User_interface/user.php?pseudo=' . $_SESSION['logged_on_user']);
        exit(0);
    }
    
    public function set_mail_in_bdd($new_mail)
    {
        $bdd = self::connect();

        if (($mail = filter_var($new_mail, FILTER_SANITIZE_EMAIL)) &&
            $mail = filter_var($mail, FILTER_VALIDATE_EMAIL))
        {
            $mail = (String)$mail;
            $req = $bdd->prepare('SELECT `mail` FROM users WHERE `mail`= :mail');
            $req->bindParam(":mail", $mail, PDO::PARAM_STR, strlen($mail));
            $req->execute();

            if ($req->rowCount() > 0)
            {
                $req->closeCursor();
                header('Location:../../Controleur/User_interface/modif.php?e=4');
                exit(0);
            }
            else
            {
                $req->closeCursor();
                
                $req = $bdd->prepare('UPDATE `users` SET `mail`= :mail WHERE `id`= :id');
                $req->bindParam(":mail", $mail ,PDO::PARAM_STR, strlen($mail));
                $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
                $req->execute();
                $req->closeCursor();
                
                $this->set_mail_confirmed_in_bdd(0);
                $this->set_mail($mail);
                $this->set_authorized_in_bdd(0);
                $this->set_token_conf(hash('whirlpool', $this->_pseudo) . hash('whirlpool', $this->_password));
                $this->set_token_conf_in_bdd($this->_token_conf);
                self::send_confirmation_mail($this);
                header('Location:../../Controleur/User_interface/modif.php?m=1');
                exit(0);
            }
        }
        else
        {
            header('Location:../../Controleur/User_interface/modif.php?e=5');
            exit(0);
        }
    }
    public function set_ville_in_bdd($ville)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `ville`= :ville WHERE `id`= :id');
        $req->bindParam(":ville", $ville ,PDO::PARAM_STR, strlen($ville));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_date_de_naissance_in_bdd($date_de_naissance)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `date_de_naissance`= :date_de_naissance WHERE `id`= :id');
        $req->bindParam(":date_de_naissance", $date_de_naissance ,PDO::PARAM_STR, strlen($date_de_naissance));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_sexe_in_bdd($sexe)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `sexe`= :sexe WHERE `id`= :id');
        $req->bindParam(":sexe", $sexe ,PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_token_conf_in_bdd($token)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `token_conf`= :token WHERE `id`= :id');
        $req->bindParam(":token", $token ,PDO::PARAM_STR, strlen($token));
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }
    
    public function set_mail_confirmed_in_bdd($mail_confirmed)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `mail_confirmed`= :mail_confirmed WHERE `id`= :id');
        $req->bindParam(":mail_confirmed", $mail_confirmed ,PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_authorized_in_bdd($authorized)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `authorized`= :authorized WHERE `id`= :id');
        $req->bindParam(":authorized", $authorized ,PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_credibility_in_bdd($credibility)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `credibility`= :credibility WHERE `id`= :id');
        $req->bindParam(":credibility", $credibility ,PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_path_profil_photo_in_bdd($profil_photo)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `profil_photo`= :profil_photo WHERE `id`= :id');
        $req->bindParam(":profil_photo", $profil_photo ,PDO::PARAM_LOB);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function set_right_access_in_bdd($right_access)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('UPDATE `users` SET `right_access`= :right_access WHERE `id`= :id');
        $req->bindParam(":right_access", $right_access ,PDO::PARAM_INT);
        $req->bindParam(":id", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function add_likes_article_in_bdd($id_article)
    {
        $bdd = self::connect();
        $id_article = (int)$id_article;
        $date_likes = date("Y-m-d H:i:s");

        $req = $bdd->prepare('INSERT INTO likes (`id_article`, `id_user`, `pseudo`, `date_likes`) VALUES (:id_article, :id_user, :pseudo, :date_likes)');
        $req->bindParam(":id_article", $id_article ,PDO::PARAM_INT);
        $req->bindParam(":id_user", $this->_id, PDO::PARAM_INT);
        $req->bindParam(":pseudo", $this->_pseudo, PDO::PARAM_STR, strlen($this->_pseudo));
        $req->bindParam(":date_likes", $date_likes, PDO::PARAM_STR, strlen($date_likes));
        $req->execute();
        $req->closeCursor();
    }

    public function del_likes_article_in_bdd($id_article)
    {
        $bdd = self::connect();
        $id_article = (int)$id_article;

        $req = $bdd->prepare('DELETE FROM `likes` WHERE `id_article`= :id_article AND `id_user`= :id_user');
        $req->bindParam(":id_article", $id_article, PDO::PARAM_INT);
        $req->bindParam(":id_user", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }

    public function add_dislikes_article_in_bdd($id_article)
    {
        $bdd = self::connect();
        $id_article = (int)$id_article;
        $date_dislikes = date("Y-m-d H:i:s");

        $req = $bdd->prepare('INSERT INTO `dislikes` (`id_article`, `id_user`, `pseudo`, `date_dislikes`) VALUES (:id_article, :id_user, :pseudo, :date_dislikes)');
        $req->bindParam(":id_article", $id_article ,PDO::PARAM_INT);
        $req->bindParam(":id_user", $this->_id, PDO::PARAM_INT);
        $req->bindParam(":pseudo", $this->_pseudo, PDO::PARAM_STR, strlen($this->_pseudo));
        $req->bindParam(":date_dislikes", $date_dislikes, PDO::PARAM_STR, strlen($date_dislikes));
        $req->execute();
        $req->closeCursor();
    }

    public function del_dislikes_article_in_bdd($id_article)
    {
        $bdd = self::connect();
        $id_article = (int)$id_article;
        $req = $bdd->prepare('DELETE FROM `dislikes` WHERE `id_article`= :id_article AND `id_user`= :id_user');
        $req->bindParam(":id_article", $id_article, PDO::PARAM_INT);
        $req->bindParam(":id_user", $this->_id, PDO::PARAM_INT);
        $req->execute();
        $req->closeCursor();
    }


    ##########################################################################
                                    //AFFICHAGE
    ##########################################################################

    public function vignette($opt)
    {
        if ($opt == 0) 
        {
            printf('
                <div class="column">
                    <div class="vign_user">
                        <div class="rond3">
                            <a href="../../Controleur/User_interface/user.php?pseudo=%s">
                                <img class="img_user" src = "data:image/.;base64,%s" alt = "">
                            </a>                            
                            <p class="contour-text2" style="margin-bottom: ">%s</p>
                        </div>
                    </div >
                </div>', urlencode($this->_pseudo), $this->_path_profil_photo, $this->_pseudo);
        }
        else if ($opt == 1)
        {
            printf('
                <div class="column">
                    <div class="vign_user">
                        <div class="rond3">
                            <a href="../../Controleur/User_interface/user.php?pseudo=%s&v=i">
                                <img class="img_user" src = "data:image/.;base64,%s" alt = "">
                            </a>                            
                            <p class="contour-text2">%s</p>
                        </div>
                    </div >
                </div>', urlencode($this->_pseudo), $this->_path_profil_photo, $this->_pseudo);
        }
    }

    public function profil() {
        printf ('
                        <div class="menu-centered contact-title2 " style="margin-bottom: 20px;">%s</div>
                        <div class="menu-centered" style="margin-bottom: 40px;">Credibility</div>
                        <div class="barre" data-length="%d"><span style="position: relative; left: 300px; top: -10px;;">%d%%</span></div>
                        <div class="contact-bar"></div>
                        <div id="droite" class="rond2 about"><img src="data:image/.;base64,%s" class="aboutimg"></div>', $this->_pseudo, $this->_credibility, $this->_credibility, $this->_path_profil_photo);
    }



    ##########################################################################
                                    //TO_STRING
    ##########################################################################


    public function __toString()
    {
        $str = sprintf("<br/><br/> id:                  %d<br/>
                            nom:                %s<br/>
                            prenom:             %s<br/>
                            pseudo:             %s<br/>
                            password:           %s<br/>
                            mail:               %s<br/>
                            ville:              %s<br/>
                            date_de_naissance:  %s<br/>
                            sexe:               %d<br/>
                            token:              %s<br/>
                            mail_confirmed:     %d<br/>
                            authorized:         %d<br/>
                            credibility:        %s<br/>
                            path_profil_photo:  %s<br/>
                            right_access:       %s<br/>
                            likes:              %s<br/>
                            dislikes:           %s<br/>
                            commentaires:       %s<br/>
                            followers:          %s<br/>
                            brand_tab:          %s<br/>",   $this->_id, $this->_nom, $this->_prenom, $this->_pseudo,
                                                            $this->_password, $this->_mail, $this->_ville,
                                                            $this->_date_de_naissance, $this->_sexe, $this->_token_conf,
                                                            $this->_mail_confirmed, $this->_authorized,
                                                            $this->_credibility, $this->_path_profil_photo,
                                                            $this->_right_access, $this->_likes, $this->_dislikes,
                                                            $this->_commentaires, $this->_follow_users, $this->_brand_tab);
        $str .= "<br/><br/>";
        foreach ($this->_user_articles as $art) {
            $str .= $art->__toString();
            $str .= "<br/><br/>";
        }
        return $str;
    }
}

//$user = new User("Alexis");
//print ($user->__toString());