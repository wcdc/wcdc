<?php

/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 6/10/16
 * Time: 10:57 AM
 */

class Slide_down
{
    private $_id;
    private $_type;
    private $_articles;

    public function connect()
    {
        try
        {
	        $bdd = new PDO('mysql:host=ba677080-001.privatesql;dbname=workingcecwcdc;port=35319;charset=utf8', 'user', 'Userpassword1', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }
        return $bdd;
    }

    public function __construct()
    {

        $bdd = self::connect();
        $req = $bdd->prepare('SELECT `id_article` FROM `slide_down` WHERE 1 LIMIT 5');
        $req->execute();
        $slide = $req->fetchAll();
        $req->closeCursor();
        
        $i = 0;
        $tab = array();
        while ($slide[$i]['id_article'])
        {
            $tmp = new Article($slide[$i++]['id_article']);
            array_push($tab, $tmp);
        }
        $this->_articles = $tab;
    }

    ##########################################################################
                                    //GETTERS
    ##########################################################################

    public function get_articles()
    {
        return $this->_articles;
    }



    ##########################################################################
                                    //SETTERS
    ##########################################################################

    public function add_article($id)
    {
        if ($this->_articles.count() < 5)
            array_push($this->_articles, $id);
    }

    public function add_article_in_bdd($id)
    {
        $bdd = self::connect();
        $req = $bdd->prepare('SELECT COUNT(`id_article`) FROM `slide_down`');
        $req->execute();
        $nb_art_in_slide = $req->fetchAll();
        $req->closeCursor();

        $req = $bdd->prepare('SELECT `id_article` FROM slide_down WHERE `id_article`= :id');
        $req->bindParam(":id", $id, PDO::PARAM_INT);
        $req->execute();
        if ($req->rowCount() > 0)
        {
            $req->closeCursor();
            header('Location:../../Controleur/User_interface/info.php?e=4');
            exit(0);
        }
        $req->closeCursor();

        $art = new Article($id);
        
        if ($nb_art_in_slide[0][0] < 5 && $art->get_authorized() == 1)
        {
            $req = $bdd->prepare('INSERT INTO `slide_down` (`id_article`) VALUE (:id)');
            $req->bindParam(":id", $id, PDO::PARAM_INT);
            $req->execute();
        }


    }


    ##########################################################################
                                    //UNSETTERS
    ##########################################################################


    public function delete_article($id)
    {
        $i = 0;
        while ($this->_articles[$i] != $id)
            $i++;
        array_splice($this->_articles, $i, 1);
    }

    public function delete_article_in_bdd($id)
    {
        $bdd = self::connect();
        $req = $bdd->prepare("DELETE FROM `slide_down` WHERE `id_article`= :id");
        $req->bindParam(":id", $id, PDO::PARAM_INT);
        $req->execute();
    }
}

