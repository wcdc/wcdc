/**
 * Created by aaverty on 5/20/16.
 */
$(function(){
    $('#form').on('submit', function (e) {
        var form = document.getElementById("form");
        var i = document.createElement("input");
        form.appendChild(i);
        var pass = document.getElementById("passwd2");
        var conf_pass = document.getElementById("confirm_passwd");
        i.name = "i";
        i.type = "hidden";

        var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
        if (!re.test(pass.value))
        {
            alert("Your password must contain at least one lowercase letter, one uppercase letter, one number and have a minimum size of 8 characters");
            e.preventDefault();
        }
        if (pass.value == conf_pass.value) {
            i.value = sha512(pass.value);
            pass.value = "";
            conf_pass.value = "";
        }
        else
        {
            alert("Password and Confirmation of Password are not same");
            e.preventDefault();
        }
    });
});

$(function(){
    $('#signin').on('submit', function (e) {
        var form = document.getElementById("signin");
        var i = document.createElement("input");
        form.appendChild(i);
        var pass = document.getElementById("passwd");
        i.name = "i";
        i.type = "hidden";
        i.value = sha512(pass.value);
        pass.value = "";
    });
});


$(function () {
    $('#wc_form').on('submit', function (e) {
        // On empêche le navigateur de soumettre le formulaire
        e.preventDefault();

        var $form = $(this);
        var formdata = (window.FormData) ? new FormData($form[0]) : null;
        var data = (formdata !== null) ? formdata : $form.serialize();

        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            contentType: false, // obligatoire pour de l'upload
            processData: false, // obligatoire pour de l'upload
            dataType: 'json', // selon le retour attendu
            data: data,
            success: function (response) {
                // La réponse du serveur
            }
        });
    });
});

$(function () {
    // A chaque sélection de fichier
    $('#wc_form').find('input[name="image"]').on('change', function (e) {
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#image_preview');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('.thumbnail').removeClass('hidden');
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
            $image_preview.find('h4').html(file.name);
            $image_preview.find('.caption p:first').html(file.size +' bytes');
        }
    });

    // Bouton "Annuler" pour vider le champ d'upload
    $('#image_preview').find('button[type="button"]').on('click', function (e) {
        e.preventDefault();

        $('#wc_form').find('input[name="image"]').val('');
        $('#image_preview').find('.thumbnail').addClass('hidden');
    });
});



