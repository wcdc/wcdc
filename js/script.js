/*
* Author : Ali Aboussebaba
* Email : bewebdeveloper@gmail.com
* Website : http://www.bewebdeveloper.com
* Subject : Crop photo using PHP and jQuery
*/

//
// $(function () {
// 	$('#t').find('input[name="wc_image"]').on('change'), function () {
// 		$(this).form.submit();
// 	}
// })

$(function () {
	$('#my_form').on('submit', function (e) {
		// On empêche le navigateur de soumettre le formulaire
		//e.preventDefault();

		var $form = $(this);
		var formdata = (window.FormData) ? new FormData($form[0]) : null;
		var data = (formdata !== null) ? formdata : $form.serialize();

		$.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			contentType: false, // obligatoire pour de l'upload
			processData: false, // obligatoire pour de l'upload
			data: data,
			success: function (response) {
				$('#result > pre').html(JSON.stringify(response, undefined, 4));
			}
		});
	});

	// A change sélection de fichier
	$('#my_form').find('input[name="wc_image"]').on('change', function (e) {
		var form = $(this);
		var files = $(this)[0].files;

		if (files.length > 0) {
			// On part du principe qu'il n'y qu'un seul fichier
			// étant donné que l'on a pas renseigné l'attribut "multiple"
			var file = files[0],
				$image_preview = $('#image_preview');

			// Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
			$image_preview.find('.thumbnail').removeClass('hidden');
			$image_preview.find('img').attr('src', window.URL.createObjectURL(file));
			$image_preview.find('h4').html(file.name);
			$image_preview.find('.caption p:first').html(file.size +' bytes');
		}
	});

	// Bouton "Annuler"
	$('#image_preview').find('button[type="button"]').on('click', function (e) {
		e.preventDefault();

		$('#my_form').find('input[name="wc_image"]').val('');
		$('#image_preview').find('.thumbnail').addClass('hidden');
	});
});

//
// $(function () {
// 	$('#my_form2').on('submit', function (e) {
// 		// On empêche le navigateur de soumettre le formulaire
// 		//e.preventDefault();
//
// 		var $form = $(this);
// 		var formdata = (window.FormData) ? new FormData($form[0]) : null;
// 		var data = (formdata !== null) ? formdata : $form.serialize();
//
// 		$.ajax({
// 			url: $form.attr('action'),
// 			type: $form.attr('method'),
// 			contentType: false, // obligatoire pour de l'upload
// 			processData: false, // obligatoire pour de l'upload
// 			dataType: 'json', // selon le retour attendu
// 			data: data,
// 			success: function (response) {
// 				$('#result > pre').html(JSON.stringify(response, undefined, 4));
// 			}
// 		});
// 	});
//
// 	// A change sélection de fichier
// 	$('#my_form2').find('input[name="dc_image"]').on('change', function (e) {
// 		var files = $(this)[0].files;
//
// 		if (files.length > 0) {
// 			// On part du principe qu'il n'y qu'un seul fichier
// 			// étant donné que l'on a pas renseigné l'attribut "multiple"
// 			var file = files[0],
// 				$image_preview = $('#image_preview2');
//
// 			// Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
// 			$image_preview.find('.thumbnail').removeClass('hidden');
// 			$image_preview.find('img').attr('src', window.URL.createObjectURL(file));
// 			$image_preview.find('h4').html(file.name);
// 			$image_preview.find('.caption p:first').html(file.size +' bytes');
// 		}
// 	});
//
// 	// Bouton "Annuler"
// 	$('#image_preview2').find('button[type="button"]').on('click', function (e) {
// 		e.preventDefault();
//
// 		$('#my_form2').find('input[name="dc_image"]').val('');
// 		$('#image_preview2').find('.thumbnail').addClass('hidden');
// 	});
// });


function preview(img, selection) {
	var scaleX = 100 / selection.width;
	var scaleY = 100 / selection.height;

	$('#thumbnail + div > img').css({
		width: Math.round(scaleX * 500) + 'px',
		height: Math.round(scaleY * 282) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
	});
	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
}

$(document).ready(function () {
	$('#save_thumb').click(function() {
		var x1 = $('#x1').val();
		var y1 = $('#y1').val();
		var x2 = $('#x2').val();
		var y2 = $('#y2').val();
		var w = $('#w').val();
		var h = $('#h').val();
		if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
			alert("You must make a selection first");
			return false;
		}else{
			return true;
		}
	});
});

$(window).load(function () {
	$('#thumbnail').imgAreaSelect({ aspectRatio: '1:1', onSelectChange: preview });
});

